﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using ITC.Library.Classes;
using PersonnelExtended.Entity;

namespace Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.DAL
{

    //-----------------------------------------------------

    #region Class "ProcItemValueTypeDB"

    public class ProcItemValueTypeDB
    {


        #region Methods :

        public void AddProcItemValueTypeDB(ProcItemValueTypeEntity procItemValueTypeEntityParam, out int ItemValueTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@ItemValueTypeId", SqlDbType.Int, 4),
                new SqlParameter("@ItemValueTypeTitle", SqlDbType.NVarChar, 200),               
                new SqlParameter("@IsViewed", SqlDbType.Bit),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(procItemValueTypeEntityParam.ItemValueTypeTitle.Trim());
            parameters[2].Value = procItemValueTypeEntityParam.IsViewed;
            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Personnel.p_ProcItemValueTypeAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            ItemValueTypeId = int.Parse(parameters[0].Value.ToString());
        }

        public void UpdateProcItemValueTypeDB(ProcItemValueTypeEntity procItemValueTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@ItemValueTypeId", SqlDbType.Int, 4),
                new SqlParameter("@ItemValueTypeTitle", SqlDbType.NVarChar, 200),               
                new SqlParameter("@IsViewed", SqlDbType.Bit),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };

            parameters[0].Value = procItemValueTypeEntityParam.ItemValueTypeId;
            parameters[1].Value = FarsiToArabic.ToArabic(procItemValueTypeEntityParam.ItemValueTypeTitle.Trim());
            parameters[2].Value = procItemValueTypeEntityParam.IsViewed;
            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Personnel.p_ProcItemValueTypeUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteProcItemValueTypeDB(ProcItemValueTypeEntity ProcItemValueTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@ItemValueTypeId", SqlDbType.Int, 4),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Value = ProcItemValueTypeEntityParam.ItemValueTypeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Personnel.p_ProcItemValueTypeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public ProcItemValueTypeEntity GetSingleProcItemValueTypeDB(ProcItemValueTypeEntity ProcItemValueTypeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                {
                    _intranetDB.GetParameter("@ItemValueTypeId", DataTypes.integer, 4, ProcItemValueTypeEntityParam.ItemValueTypeId)
                };

                reader = _intranetDB.RunProcedureReader("Personnel.p_ProcItemValueTypeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetProcItemValueTypeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ProcItemValueTypeEntity> GetAllProcItemValueTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetProcItemValueTypeDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Personnel.p_ProcItemValueTypeGetAll", new IDataParameter[] {}));
        }

        public List<ProcItemValueTypeEntity> GetAllIsActiveProcItemValueTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetProcItemValueTypeDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Personnel.p_ProcItemValueTypeGetAllIsActive", new IDataParameter[] { }));
        }

        public List<ProcItemValueTypeEntity> GetPageProcItemValueTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
            {
                new SqlParameter("@PageSize", SqlDbType.Int),
                new SqlParameter("@CurrentPage", SqlDbType.Int),
                new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
            };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_ProcItemValueType";
            parameters[5].Value = "ItemValueTypeId";
            DataSet ds = _intranetDB.RunProcedureDS("Personnel.p_TablesGetPage", parameters);
            return GetProcItemValueTypeDBCollectionFromDataSet(ds, out count);
        }

        public ProcItemValueTypeEntity GetProcItemValueTypeDBFromDataReader(IDataReader reader)
        {
            return new ProcItemValueTypeEntity(int.Parse(reader["ItemValueTypeId"].ToString()),
                reader["ItemValueTypeTitle"].ToString(),
                reader["CreationDate"].ToString(),
                reader["ModificationDate"].ToString(),
                bool.Parse(reader["IsViewed"].ToString()));
        }

        public List<ProcItemValueTypeEntity> GetProcItemValueTypeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<ProcItemValueTypeEntity> lst = new List<ProcItemValueTypeEntity>();
                while (reader.Read())
                    lst.Add(GetProcItemValueTypeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ProcItemValueTypeEntity> GetProcItemValueTypeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetProcItemValueTypeDBCollectionFromDataReader(ds.CreateDataReader());
        }

        #endregion

    }

    #endregion
}