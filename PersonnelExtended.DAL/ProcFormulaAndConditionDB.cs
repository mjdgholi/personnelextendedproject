﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using PersonnelExtended.Entity;

namespace Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.DAL
{

    #region Class "ProcFormulaAndConditionDB"

    public class ProcFormulaAndConditionDB
    {
        #region Methods :

        public void AddProcFormulaAndConditionDB(ProcFormulaAndConditionEntity procFormulaAndConditionEntityParam, out int FormulaAndConditionId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@FormulaAndConditionId", SqlDbType.Int, 4),
                new SqlParameter("@ItemDetailProcessId", SqlDbType.Int),
                new SqlParameter("@TypeId", SqlDbType.Bit),
                new SqlParameter("@ConditionalExpressionForUser", SqlDbType.NVarChar, -1),             
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = procFormulaAndConditionEntityParam.ItemDetailProcessId;
            parameters[2].Value = procFormulaAndConditionEntityParam.TypeId;
            parameters[3].Value = procFormulaAndConditionEntityParam.ConditionalExpressionForUser;           
            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Personnel.p_ProcFormulaAndConditionAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            FormulaAndConditionId = int.Parse(parameters[0].Value.ToString());
        }

        public void UpdateProcFormulaAndConditionDB(ProcFormulaAndConditionEntity procFormulaAndConditionEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@FormulaAndConditionId", SqlDbType.Int, 4),
                new SqlParameter("@ItemDetailProcessId", SqlDbType.Int),
                new SqlParameter("@TypeId", SqlDbType.Bit),
                new SqlParameter("@ConditionalExpressionForUser", SqlDbType.NVarChar, -1),             
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };

            parameters[0].Value = procFormulaAndConditionEntityParam.FormulaAndConditionId;
            parameters[1].Value = procFormulaAndConditionEntityParam.ItemDetailProcessId;
            parameters[2].Value = procFormulaAndConditionEntityParam.TypeId;
            parameters[3].Value = procFormulaAndConditionEntityParam.ConditionalExpressionForUser;
            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Personnel.p_ProcFormulaAndConditionUpdate", parameters);
            var messageError = parameters[100].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteProcFormulaAndConditionDB(ProcFormulaAndConditionEntity ProcFormulaAndConditionEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@FormulaAndConditionId", SqlDbType.Int, 4),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Value = ProcFormulaAndConditionEntityParam.FormulaAndConditionId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Personnel.p_ProcFormulaAndConditionDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public ProcFormulaAndConditionEntity GetSingleProcFormulaAndConditionDB(ProcFormulaAndConditionEntity ProcFormulaAndConditionEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                {
                    _intranetDB.GetParameter("@FormulaAndConditionId", DataTypes.integer, 4, ProcFormulaAndConditionEntityParam.FormulaAndConditionId)
                };

                reader = _intranetDB.RunProcedureReader("Personnel.p_ProcFormulaAndConditionGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetProcFormulaAndConditionDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ProcFormulaAndConditionEntity> GetAllProcFormulaAndConditionDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetProcFormulaAndConditionDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Personnel.p_ProcFormulaAndConditionGetAll", new IDataParameter[] {}));
        }

        public List<ProcFormulaAndConditionEntity> GetPageProcFormulaAndConditionDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
            {
                new SqlParameter("@PageSize", SqlDbType.Int),
                new SqlParameter("@CurrentPage", SqlDbType.Int),
                new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
            };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_ProcFormulaAndCondition";
            parameters[5].Value = "FormulaAndConditionId";
            DataSet ds = _intranetDB.RunProcedureDS("Personnel.p_TablesGetPage", parameters);
            return GetProcFormulaAndConditionDBCollectionFromDataSet(ds, out count);
        }

        public ProcFormulaAndConditionEntity GetProcFormulaAndConditionDBFromDataReader(IDataReader reader)
        {
            return new ProcFormulaAndConditionEntity(int.Parse(reader["FormulaAndConditionId"].ToString()),
                int.Parse(reader["ItemDetailProcessId"].ToString()),
                bool.Parse(reader["TypeId"].ToString()),
                reader["ConditionalExpressionForUser"].ToString(),
                reader["CreationDate"].ToString(),
                reader["ModificationDate"].ToString());
        }

        public List<ProcFormulaAndConditionEntity> GetProcFormulaAndConditionDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<ProcFormulaAndConditionEntity> lst = new List<ProcFormulaAndConditionEntity>();
                while (reader.Read())
                    lst.Add(GetProcFormulaAndConditionDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ProcFormulaAndConditionEntity> GetProcFormulaAndConditionDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetProcFormulaAndConditionDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<ProcFormulaAndConditionEntity> GetProcFormulaAndConditionDBCollectionByProcItemDetailProcessDB(int ItemDetailProcessId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ItemDetailProcessId", SqlDbType.Int);
            parameter.Value = ItemDetailProcessId;
            return GetProcFormulaAndConditionDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Personnel.p_ProcFormulaAndConditionGetByProcItemDetailProcess", new[] {parameter}));
        }

        #endregion

        public ProcFormulaAndConditionEntity GetSingleByDetailProcessByType(int itemDetailProcessId, int type)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                {
                    _intranetDB.GetParameter("@itemDetailProcessId", DataTypes.integer, 4, itemDetailProcessId),
                    _intranetDB.GetParameter("@type", DataTypes.integer, 4, type)
                };

                reader = _intranetDB.RunProcedureReader("Personnel.p_ProcFormulaAndConditionGetSingleByDetailProcessAndType", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetProcFormulaAndConditionDBFromDataReader(reader);
                return null;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }
    }

    #endregion
}