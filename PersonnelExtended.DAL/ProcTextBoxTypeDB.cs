﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using ITC.Library.Classes;
using PersonnelExtended.Entity;

namespace Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.DAL
{

    //-----------------------------------------------------

    #region Class "ProcTextBoxTypeDB"

    public class ProcTextBoxTypeDB
    {

        #region Methods :

        public void AddProcTextBoxTypeDB(ProcTextBoxTypeEntity procTextBoxTypeEntityParam, out int TextBoxTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@TextBoxTypeId", SqlDbType.Int, 4),
                new SqlParameter("@TextBoxTypeTitle", SqlDbType.NVarChar, 100),
                new SqlParameter("@CSSName", SqlDbType.NVarChar, 200),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(procTextBoxTypeEntityParam.TextBoxTypeTitle.Trim());
            parameters[2].Value = procTextBoxTypeEntityParam.CSSName;            
            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Personnel.p_ProcTextBoxTypeAdd", parameters);
            var messageError = parameters[100].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            TextBoxTypeId = int.Parse(parameters[0].Value.ToString());
        }

        public void UpdateProcTextBoxTypeDB(ProcTextBoxTypeEntity procTextBoxTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@TextBoxTypeId", SqlDbType.Int, 4),
                new SqlParameter("@TextBoxTypeTitle", SqlDbType.NVarChar, 100),
                new SqlParameter("@CSSName", SqlDbType.NVarChar, 200),             
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };

            parameters[0].Value = procTextBoxTypeEntityParam.TextBoxTypeId;
            parameters[1].Value = FarsiToArabic.ToArabic(procTextBoxTypeEntityParam.TextBoxTypeTitle.Trim());
            parameters[2].Value = procTextBoxTypeEntityParam.CSSName;
            parameters[3].Direction = ParameterDirection.Output; 

            _intranetDB.RunProcedure("Personnel.p_ProcTextBoxTypeUpdate", parameters);
            var messageError = parameters[100].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteProcTextBoxTypeDB(ProcTextBoxTypeEntity ProcTextBoxTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@TextBoxTypeId", SqlDbType.Int, 4),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Value = ProcTextBoxTypeEntityParam.TextBoxTypeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Personnel.p_ProcTextBoxTypeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public ProcTextBoxTypeEntity GetSingleProcTextBoxTypeDB(ProcTextBoxTypeEntity ProcTextBoxTypeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                {
                    _intranetDB.GetParameter("@TextBoxTypeId", DataTypes.integer, 4, ProcTextBoxTypeEntityParam.TextBoxTypeId)
                };

                reader = _intranetDB.RunProcedureReader("Personnel.p_ProcTextBoxTypeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetProcTextBoxTypeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ProcTextBoxTypeEntity> GetAllProcTextBoxTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetProcTextBoxTypeDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Personnel.p_ProcTextBoxTypeGetAll", new IDataParameter[] {}));
        }

        public List<ProcTextBoxTypeEntity> GetPageProcTextBoxTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
            {
                new SqlParameter("@PageSize", SqlDbType.Int),
                new SqlParameter("@CurrentPage", SqlDbType.Int),
                new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
            };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_ProcTextBoxType";
            parameters[5].Value = "TextBoxTypeId";
            DataSet ds = _intranetDB.RunProcedureDS("Personnel.p_TablesGetPage", parameters);
            return GetProcTextBoxTypeDBCollectionFromDataSet(ds, out count);
        }

        public ProcTextBoxTypeEntity GetProcTextBoxTypeDBFromDataReader(IDataReader reader)
        {
            return new ProcTextBoxTypeEntity(int.Parse(reader["TextBoxTypeId"].ToString()),
                reader["TextBoxTypeTitle"].ToString(),
                reader["CSSName"].ToString(),
                reader["CreationDate"].ToString(),
                reader["ModificationDate"].ToString());
        }

        public List<ProcTextBoxTypeEntity> GetProcTextBoxTypeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<ProcTextBoxTypeEntity> lst = new List<ProcTextBoxTypeEntity>();
                while (reader.Read())
                    lst.Add(GetProcTextBoxTypeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ProcTextBoxTypeEntity> GetProcTextBoxTypeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetProcTextBoxTypeDBCollectionFromDataReader(ds.CreateDataReader());
        }

        #endregion

    }

    #endregion
}