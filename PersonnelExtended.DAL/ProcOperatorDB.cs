﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using ITC.Library.Classes;
using PersonnelExtended.Entity;

namespace Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.DAL
{
    #region Class "ProcOperatorDB"

    public class ProcOperatorDB
    {
        #region Methods :

        public void AddProcOperatorDB(ProcOperatorEntity procOperatorEntityParam, out int OperatorId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@OperatorId", SqlDbType.Int, 4),
                new SqlParameter("@OperatorTitle", SqlDbType.NVarChar, 200),
                new SqlParameter("@SqlOperator", SqlDbType.NVarChar, 200),
                new SqlParameter("@SampleTextForToolTip", SqlDbType.NVarChar, -1),
                new SqlParameter("@IsActive", SqlDbType.Bit),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(procOperatorEntityParam.OperatorTitle.Trim());
            parameters[2].Value = procOperatorEntityParam.SqlOperator;
            parameters[3].Value = FarsiToArabic.ToArabic(procOperatorEntityParam.SampleTextForToolTip.Trim());
            parameters[4].Value = procOperatorEntityParam.IsActive;
            parameters[5].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Personnel.p_ProcOperatorAdd", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            OperatorId = int.Parse(parameters[0].Value.ToString());
        }

        public void UpdateProcOperatorDB(ProcOperatorEntity procOperatorEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@OperatorId", SqlDbType.Int, 4),
                new SqlParameter("@OperatorTitle", SqlDbType.NVarChar, 200),
                new SqlParameter("@SqlOperator", SqlDbType.NVarChar, 200),
                new SqlParameter("@SampleTextForToolTip", SqlDbType.NVarChar, -1),
                new SqlParameter("@IsActive", SqlDbType.Bit),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };

            parameters[0].Value = procOperatorEntityParam.OperatorId;
            parameters[1].Value = FarsiToArabic.ToArabic(procOperatorEntityParam.OperatorTitle.Trim());
            parameters[2].Value = procOperatorEntityParam.SqlOperator;
            parameters[3].Value = FarsiToArabic.ToArabic(procOperatorEntityParam.SampleTextForToolTip.Trim());
            parameters[4].Value = procOperatorEntityParam.IsActive;
            parameters[5].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Personnel.p_ProcOperatorUpdate", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteProcOperatorDB(ProcOperatorEntity ProcOperatorEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@OperatorId", SqlDbType.Int, 4),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Value = ProcOperatorEntityParam.OperatorId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Personnel.p_ProcOperatorDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public ProcOperatorEntity GetSingleProcOperatorDB(ProcOperatorEntity ProcOperatorEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                {
                    _intranetDB.GetParameter("@OperatorId", DataTypes.integer, 4, ProcOperatorEntityParam.OperatorId)
                };

                reader = _intranetDB.RunProcedureReader("Personnel.p_ProcOperatorGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetProcOperatorDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ProcOperatorEntity> GetAllProcOperatorDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetProcOperatorDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Personnel.p_ProcOperatorGetAll", new IDataParameter[] {}));
        }

        public List<ProcOperatorEntity> GetAllIsActiveProcOperatorDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetProcOperatorDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Personnel.p_ProcOperatorGetAllIsActive", new IDataParameter[] { }));
        }

        public List<ProcOperatorEntity> GetPageProcOperatorDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
            {
                new SqlParameter("@PageSize", SqlDbType.Int),
                new SqlParameter("@CurrentPage", SqlDbType.Int),
                new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
            };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_ProcOperator";
            parameters[5].Value = "OperatorId";
            DataSet ds = _intranetDB.RunProcedureDS("Personnel.p_TablesGetPage", parameters);
            return GetProcOperatorDBCollectionFromDataSet(ds, out count);
        }

        public ProcOperatorEntity GetProcOperatorDBFromDataReader(IDataReader reader)
        {
            return new ProcOperatorEntity(int.Parse(reader["OperatorId"].ToString()),
                reader["OperatorTitle"].ToString(),
                reader["SqlOperator"].ToString(),
                reader["SampleTextForToolTip"].ToString(),
                bool.Parse(reader["IsActive"].ToString()),
                reader["CreationDate"].ToString(),
                reader["ModificationDate"].ToString());
        }

        public List<ProcOperatorEntity> GetProcOperatorDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<ProcOperatorEntity> lst = new List<ProcOperatorEntity>();
                while (reader.Read())
                    lst.Add(GetProcOperatorDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ProcOperatorEntity> GetProcOperatorDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetProcOperatorDBCollectionFromDataReader(ds.CreateDataReader());
        }

        #endregion
    }

    #endregion
}