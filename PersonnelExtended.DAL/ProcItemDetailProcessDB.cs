﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using PersonnelExtended.Entity;

namespace Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.DAL
{

    //-----------------------------------------------------

    #region Class "ProcItemDetailProcessDB"

    public class ProcItemDetailProcessDB
    {


        #region Methods :

        public void AddProcItemDetailProcessDB(ProcItemDetailProcessEntity procItemDetailProcessEntityParam, out int ItemDetailProcessId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@ItemDetailProcessId", SqlDbType.Int, 4),
                new SqlParameter("@ItemId", SqlDbType.Int),
                new SqlParameter("@IsBuiltInFunction", SqlDbType.Bit),
                new SqlParameter("@Formula", SqlDbType.NVarChar, 1024),
                new SqlParameter("@FunctionId", SqlDbType.Int),
                new SqlParameter("@Priority", SqlDbType.Int),
                new SqlParameter("@FromDate", SqlDbType.SmallDateTime),
                new SqlParameter("@ToDate", SqlDbType.SmallDateTime),
                new SqlParameter("@IsValid", SqlDbType.Bit),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = procItemDetailProcessEntityParam.ItemId;
            parameters[2].Value = procItemDetailProcessEntityParam.IsBuiltInFunction;
            parameters[3].Value = (procItemDetailProcessEntityParam.Formula) == null ? DBNull.Value : (object) procItemDetailProcessEntityParam.Formula;
            parameters[4].Value = (procItemDetailProcessEntityParam.FunctionId == null) ? DBNull.Value : (object) procItemDetailProcessEntityParam.FunctionId;
            parameters[5].Value = (procItemDetailProcessEntityParam.Priority);
            parameters[6].Value = procItemDetailProcessEntityParam.FromDate;
            parameters[7].Value = procItemDetailProcessEntityParam.ToDate;
            parameters[8].Value = procItemDetailProcessEntityParam.IsValid;
            parameters[9].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Personnel.p_ProcItemDetailProcessAdd", parameters);
            var messageError = parameters[9].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            ItemDetailProcessId = int.Parse(parameters[0].Value.ToString());
        }

        public void UpdateProcItemDetailProcessDB(ProcItemDetailProcessEntity procItemDetailProcessEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@ItemDetailProcessId", SqlDbType.Int, 4),
                new SqlParameter("@ItemId", SqlDbType.Int),               
                new SqlParameter("@IsBuiltInFunction", SqlDbType.Bit),
                new SqlParameter("@Formula", SqlDbType.NVarChar, 1024),
                new SqlParameter("@FunctionId", SqlDbType.Int),
                new SqlParameter("@Priority", SqlDbType.Int),         
                 new SqlParameter("@FromDate", SqlDbType.SmallDateTime),
                new SqlParameter("@ToDate", SqlDbType.SmallDateTime),
                new SqlParameter("@IsValid", SqlDbType.Bit),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };

            parameters[0].Value = procItemDetailProcessEntityParam.ItemDetailProcessId;
            parameters[1].Value = procItemDetailProcessEntityParam.ItemId;
            parameters[2].Value = procItemDetailProcessEntityParam.IsBuiltInFunction;
            parameters[3].Value = (procItemDetailProcessEntityParam.Formula) == null ? DBNull.Value : (object)procItemDetailProcessEntityParam.Formula;
            parameters[4].Value = (procItemDetailProcessEntityParam.FunctionId == null) ? DBNull.Value : (object)procItemDetailProcessEntityParam.FunctionId;
            parameters[5].Value = (procItemDetailProcessEntityParam.Priority);
            parameters[6].Value = procItemDetailProcessEntityParam.FromDate;
            parameters[7].Value = procItemDetailProcessEntityParam.ToDate;
            parameters[8].Value = procItemDetailProcessEntityParam.IsValid;
            parameters[9].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Personnel.p_ProcItemDetailProcessUpdate", parameters);
            var messageError = parameters[9].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteProcItemDetailProcessDB(ProcItemDetailProcessEntity ProcItemDetailProcessEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@ItemDetailProcessId", SqlDbType.Int, 4),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Value = ProcItemDetailProcessEntityParam.ItemDetailProcessId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Personnel.p_ProcItemDetailProcessDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public ProcItemDetailProcessEntity GetSingleProcItemDetailProcessDB(ProcItemDetailProcessEntity ProcItemDetailProcessEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                {
                    _intranetDB.GetParameter("@ItemDetailProcessId", DataTypes.integer, 4, ProcItemDetailProcessEntityParam.ItemDetailProcessId)
                };

                reader = _intranetDB.RunProcedureReader("Personnel.p_ProcItemDetailProcessGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetProcItemDetailProcessDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ProcItemDetailProcessEntity> GetAllProcItemDetailProcessDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetProcItemDetailProcessDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Personnel.p_ProcItemDetailProcessGetAll", new IDataParameter[] {}));
        }

        public List<ProcItemDetailProcessEntity> GetPageProcItemDetailProcessDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
            {
                new SqlParameter("@PageSize", SqlDbType.Int),
                new SqlParameter("@CurrentPage", SqlDbType.Int),
                new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
            };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_ProcItemDetailProcess";
            parameters[5].Value = "ItemDetailProcessId";
            DataSet ds = _intranetDB.RunProcedureDS("Personnel.p_TablesGetPage", parameters);
            return GetProcItemDetailProcessDBCollectionFromDataSet(ds, out count);
        }

        public ProcItemDetailProcessEntity GetProcItemDetailProcessDBFromDataReader(IDataReader reader)
        {
            return new ProcItemDetailProcessEntity(int.Parse(reader["ItemDetailProcessId"].ToString()),
                int.Parse(reader["ItemId"].ToString()),
                reader["Condition"].ToString(),
                bool.Parse(reader["IsBuiltInFunction"].ToString()),
                reader["Formula"].ToString(),
                Convert.IsDBNull(reader["FunctionId"]) ? null : (int?) reader["FunctionId"],
                int.Parse(reader["Priority"].ToString()),
                reader["FromDate"].ToString(),
                reader["ToDate"].ToString(),
                bool.Parse(reader["IsValid"].ToString()),
                reader["CreationDate"].ToString(),
                reader["ModificationDate"].ToString());
        }

        public List<ProcItemDetailProcessEntity> GetProcItemDetailProcessDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<ProcItemDetailProcessEntity> lst = new List<ProcItemDetailProcessEntity>();
                while (reader.Read())
                    lst.Add(GetProcItemDetailProcessDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ProcItemDetailProcessEntity> GetProcItemDetailProcessDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetProcItemDetailProcessDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<ProcItemDetailProcessEntity> GetProcItemDetailProcessDBCollectionByProcItemDB(int ItemId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ItemId", SqlDbType.Int);
            parameter.Value = ItemId;
            return GetProcItemDetailProcessDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Personnel.p_ProcItemDetailProcessGetByProcItem", new[] {parameter}));
        }

        #endregion

        public void CloseDateProcItemDetailProcessDB(ProcItemDetailProcessEntity procItemDetailProcessEntity)
        {
            var intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@ItemDetailProcessId", SqlDbType.Int, 4),
                                              new SqlParameter("@ToDate", SqlDbType.SmallDateTime),
                                              new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                          };
            parameters[0].Value = procItemDetailProcessEntity.ItemDetailProcessId;
            parameters[1].Value = (procItemDetailProcessEntity.ToDate.Trim() == "") ? (object)DBNull.Value : procItemDetailProcessEntity.ToDate;
            parameters[2].Direction = ParameterDirection.Output;
            intranetDB.RunProcedure("Personnel.p_ProcItemDetailProcessCloseDate", parameters);
            var messageError = parameters[2].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }
    }

    #endregion
}