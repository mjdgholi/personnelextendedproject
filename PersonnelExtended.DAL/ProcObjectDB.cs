﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using ITC.Library.Classes;
using PersonnelExtended.Entity;

namespace Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.DAL
{

    #region Class "ProcObjectDB"

    public class ProcObjectDB
    {
        #region Methods :

        public void AddProcObjectDB(ProcObjectEntity procObjectEntityParam, out int ObjectId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@ObjectId", SqlDbType.Int, 4),
                new SqlParameter("@ObjectTitle", SqlDbType.NVarChar, 350),
                new SqlParameter("@ObjectTitlePersian",SqlDbType.NVarChar,350), 
                new SqlParameter("@ObjectTypeId", SqlDbType.Int),
                new SqlParameter("@SystemId", SqlDbType.Int),
                new SqlParameter("@IsHierarchy", SqlDbType.Bit),
                new SqlParameter("@IsActive", SqlDbType.Bit),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(procObjectEntityParam.ObjectTitle.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(procObjectEntityParam.ObjectTitlePersian.Trim());
            parameters[3].Value = procObjectEntityParam.ObjectTypeId;
            parameters[4].Value = (procObjectEntityParam.SystemId == null ? System.DBNull.Value : (object)procObjectEntityParam.SystemId);
            parameters[5].Value = procObjectEntityParam.IsHierarchy;
            parameters[6].Value = procObjectEntityParam.IsActive;
            parameters[7].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Personnel.p_ProcObjectAdd", parameters);
            var messageError = parameters[7].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            ObjectId = int.Parse(parameters[0].Value.ToString());
        }

        public void UpdateProcObjectDB(ProcObjectEntity procObjectEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@ObjectId", SqlDbType.Int, 4),
                new SqlParameter("@ObjectTitle", SqlDbType.NVarChar, 350),
                new SqlParameter("@ObjectTitlePersian",SqlDbType.NVarChar,350), 
                new SqlParameter("@ObjectTypeId", SqlDbType.Int),
                new SqlParameter("@SystemId", SqlDbType.Int),
                new SqlParameter("@IsHierarchy", SqlDbType.Bit),
                new SqlParameter("@IsActive", SqlDbType.Bit),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };

            parameters[0].Value = procObjectEntityParam.ObjectId;
            parameters[1].Value = FarsiToArabic.ToArabic(procObjectEntityParam.ObjectTitle.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(procObjectEntityParam.ObjectTitlePersian.Trim());
            parameters[3].Value = procObjectEntityParam.ObjectTypeId;
            parameters[4].Value = (procObjectEntityParam.SystemId == null ? System.DBNull.Value : (object)procObjectEntityParam.SystemId);
            parameters[5].Value = procObjectEntityParam.IsHierarchy;
            parameters[6].Value = procObjectEntityParam.IsActive;
            parameters[7].Direction = ParameterDirection.Output;           

            _intranetDB.RunProcedure("Personnel.p_ProcObjectUpdate", parameters);
            var messageError = parameters[7].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteProcObjectDB(ProcObjectEntity ProcObjectEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@ObjectId", SqlDbType.Int, 4),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Value = ProcObjectEntityParam.ObjectId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Personnel.p_ProcObjectDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public ProcObjectEntity GetSingleProcObjectDB(ProcObjectEntity ProcObjectEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                {
                    _intranetDB.GetParameter("@ObjectId", DataTypes.integer, 4, ProcObjectEntityParam.ObjectId)
                };

                reader = _intranetDB.RunProcedureReader("Personnel.p_ProcObjectGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetProcObjectDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ProcObjectEntity> GetAllProcObjectDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetProcObjectDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Personnel.p_ProcObjectGetAll", new IDataParameter[] {}));
        }

        public List<ProcObjectEntity> GetAllIsActiveProcObjectDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetProcObjectDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader("Personnel.p_ProcObjectGetAllIsActive", new IDataParameter[] { }));
        }

        public List<ProcObjectEntity> GetPageProcObjectDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
            {
                new SqlParameter("@PageSize", SqlDbType.Int),
                new SqlParameter("@CurrentPage", SqlDbType.Int),
                new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
            };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_ProcObject";
            parameters[5].Value = "ObjectId";
            DataSet ds = _intranetDB.RunProcedureDS("Personnel.p_TablesGetPage", parameters);
            return GetProcObjectDBCollectionFromDataSet(ds, out count);
        }

        public ProcObjectEntity GetProcObjectDBFromDataReader(IDataReader reader)
        {
            return new ProcObjectEntity(int.Parse(reader["ObjectId"].ToString()),
                reader["ObjectTitle"].ToString(),
                reader["objectTitlePersian"].ToString(),
                reader["CreationDate"].ToString(),
                reader["ModificationDate"].ToString(),
                int.Parse(reader["ObjectTypeId"].ToString()),
                Convert.IsDBNull(reader["SystemId"]) ? null : (int?)reader["SystemId"],                     
                bool.Parse(reader["IsHierarchy"].ToString()),
                bool.Parse(reader["IsActive"].ToString()));
        }

        public List<ProcObjectEntity> GetProcObjectDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<ProcObjectEntity> lst = new List<ProcObjectEntity>();
                while (reader.Read())
                    lst.Add(GetProcObjectDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ProcObjectEntity> GetProcObjectDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetProcObjectDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<ProcObjectEntity> GetProcObjectDBCollectionByProcObjectTypeDB(int ObjectTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ObjectTypeId", SqlDbType.Int);
            parameter.Value = ObjectTypeId;
            return GetProcObjectDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Personnel.p_ProcObjectGetByProcObjectType", new[] {parameter}));
        }

        //---------------- start own function -------------
        public DataSet GetAllSqlObject(string objectTitle, string schemaName)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("@ObjectType",SqlDbType.NVarChar,50), 
                    new SqlParameter("@SchemaName",SqlDbType.NVarChar,300), 
                };
            parameters[0].Value = objectTitle;
            parameters[1].Value = schemaName;
            return _intranetDB.RunProcedureDS("Personnel.p_GelAllSqlObject", parameters);
        }


        #endregion

    }

    #endregion
}