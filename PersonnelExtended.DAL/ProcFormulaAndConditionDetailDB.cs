﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using ITC.Library.Classes;
using PersonnelExtended.Entity;

namespace Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.DAL
{

    //-----------------------------------------------------

    #region Class "ProcFormulaAndConditionDetailDB"

    public class ProcFormulaAndConditionDetailDB
    {
        #region Methods :

        public void AddProcFormulaAndConditionDetailDB(ProcFormulaAndConditionDetailEntity procFormulaAndConditionDetailEntityParam, out int ProcFormulaAndConditionDetailId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@ProcFormulaAndConditionDetailId", SqlDbType.Int, 4),
                new SqlParameter("@FormulaAndConditionId", SqlDbType.Int),
                new SqlParameter("@PreLink", SqlDbType.Int),
                new SqlParameter("@PostLink", SqlDbType.Int),
                new SqlParameter("@UserAlterLink", SqlDbType.Int),
                new SqlParameter("@TextBoxTypeId", SqlDbType.Int),
                new SqlParameter("@OperatorId", SqlDbType.Int),
                new SqlParameter("@ItemId", SqlDbType.Int),
                new SqlParameter("@Value", SqlDbType.NVarChar, -1),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = procFormulaAndConditionDetailEntityParam.FormulaAndConditionId;
            parameters[2].Value = procFormulaAndConditionDetailEntityParam.PreLink;
            parameters[3].Value = procFormulaAndConditionDetailEntityParam.PostLink;
            parameters[4].Value = procFormulaAndConditionDetailEntityParam.UserAlterLink;
            parameters[5].Value = procFormulaAndConditionDetailEntityParam.TextBoxTypeId;
            parameters[6].Value = procFormulaAndConditionDetailEntityParam.OperatorId;
            parameters[7].Value = procFormulaAndConditionDetailEntityParam.ItemId;
            parameters[8].Value = procFormulaAndConditionDetailEntityParam.Value.Trim();
            parameters[9].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Personnel.p_ProcFormulaAndConditionDetailAdd", parameters);
            var messageError = parameters[9].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            ProcFormulaAndConditionDetailId = int.Parse(parameters[0].Value.ToString());
        }

        public void UpdateProcFormulaAndConditionDetailDB(ProcFormulaAndConditionDetailEntity procFormulaAndConditionDetailEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@ProcFormulaAndConditionDetailId", SqlDbType.Int, 4),
                new SqlParameter("@FormulaAndConditionId", SqlDbType.Int),
                new SqlParameter("@PreLink", SqlDbType.Int),
                new SqlParameter("@PostLink", SqlDbType.Int),
                new SqlParameter("@UserAlterLink", SqlDbType.Int),
                new SqlParameter("@TextBoxTypeId", SqlDbType.Int),
                new SqlParameter("@OperatorId", SqlDbType.Int),
                new SqlParameter("@ItemId", SqlDbType.Int),
                new SqlParameter("@Value", SqlDbType.NVarChar, -1),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };

            parameters[0].Value = procFormulaAndConditionDetailEntityParam.ProcFormulaAndConditionDetailId;
            parameters[1].Value = procFormulaAndConditionDetailEntityParam.FormulaAndConditionId;
            parameters[2].Value = procFormulaAndConditionDetailEntityParam.PreLink;
            parameters[3].Value = procFormulaAndConditionDetailEntityParam.PostLink;
            parameters[4].Value = procFormulaAndConditionDetailEntityParam.UserAlterLink;
            parameters[5].Value = procFormulaAndConditionDetailEntityParam.TextBoxTypeId;
            parameters[6].Value = procFormulaAndConditionDetailEntityParam.OperatorId;
            parameters[7].Value = procFormulaAndConditionDetailEntityParam.ItemId;
            parameters[8].Value = procFormulaAndConditionDetailEntityParam.Value.Trim();
            parameters[9].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Personnel.p_ProcFormulaAndConditionDetailUpdate", parameters);
            var messageError = parameters[9].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteProcFormulaAndConditionDetailDB(ProcFormulaAndConditionDetailEntity ProcFormulaAndConditionDetailEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@ProcFormulaAndConditionDetailId", SqlDbType.Int, 4),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Value = ProcFormulaAndConditionDetailEntityParam.ProcFormulaAndConditionDetailId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Personnel.p_ProcFormulaAndConditionDetailDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public ProcFormulaAndConditionDetailEntity GetSingleProcFormulaAndConditionDetailDB(ProcFormulaAndConditionDetailEntity ProcFormulaAndConditionDetailEntityParam)
        {
            IDataReader reader = null;
            try
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                {
                    _intranetDB.GetParameter("@ProcFormulaAndConditionDetailId", DataTypes.integer, 4, ProcFormulaAndConditionDetailEntityParam.ProcFormulaAndConditionDetailId)
                };

                reader = _intranetDB.RunProcedureReader("Personnel.p_ProcFormulaAndConditionDetailGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetProcFormulaAndConditionDetailDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ProcFormulaAndConditionDetailEntity> GetAllProcFormulaAndConditionDetailDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetProcFormulaAndConditionDetailDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Personnel.p_ProcFormulaAndConditionDetailGetAll", new IDataParameter[] {}));
        }

        public List<ProcFormulaAndConditionDetailEntity> GetPageProcFormulaAndConditionDetailDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
            {
                new SqlParameter("@PageSize", SqlDbType.Int),
                new SqlParameter("@CurrentPage", SqlDbType.Int),
                new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
            };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_ProcFormulaAndConditionDetail";
            parameters[5].Value = "ProcFormulaAndConditionDetailId";
            DataSet ds = _intranetDB.RunProcedureDS("Personnel.p_TablesGetPage", parameters);
            return GetProcFormulaAndConditionDetailDBCollectionFromDataSet(ds, out count);
        }

        public ProcFormulaAndConditionDetailEntity GetProcFormulaAndConditionDetailDBFromDataReader(IDataReader reader)
        {
            return new ProcFormulaAndConditionDetailEntity(int.Parse(reader["ProcFormulaAndConditionDetailId"].ToString()),
                int.Parse(reader["FormulaAndConditionId"].ToString()),
                int.Parse(reader["PreLink"].ToString()),
                int.Parse(reader["PostLink"].ToString()),
                int.Parse(reader["UserAlterLink"].ToString()),
                int.Parse(reader["TextBoxTypeId"].ToString()),
                int.Parse(reader["OperatorId"].ToString()),
                int.Parse(reader["ItemId"].ToString()),
                reader["Value"].ToString(),
                reader["CreationDate"].ToString(),
                reader["ModificationDate"].ToString());
        }

        public List<ProcFormulaAndConditionDetailEntity> GetProcFormulaAndConditionDetailDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<ProcFormulaAndConditionDetailEntity> lst = new List<ProcFormulaAndConditionDetailEntity>();
                while (reader.Read())
                    lst.Add(GetProcFormulaAndConditionDetailDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ProcFormulaAndConditionDetailEntity> GetProcFormulaAndConditionDetailDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetProcFormulaAndConditionDetailDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<ProcFormulaAndConditionDetailEntity> GetProcFormulaAndConditionDetailDBCollectionByProcFormulaAndConditionDetailDB(int PreLink)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@PreLink", SqlDbType.Int);
            parameter.Value = PreLink;
            return GetProcFormulaAndConditionDetailDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Personnel.p_ProcFormulaAndConditionDetailGetByProcFormulaAndConditionDetail", new[] {parameter}));
        }

        public List<ProcFormulaAndConditionDetailEntity> GetProcFormulaAndConditionDetailDBCollectionByProcFormulaAndConditionDB(int FormulaAndConditionId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@FormulaAndConditionId", SqlDbType.Int);
            parameter.Value = FormulaAndConditionId;
            return GetProcFormulaAndConditionDetailDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Personnel.p_ProcFormulaAndConditionDetailGetByProcFormulaAndCondition", new[] {parameter}));
        }

        public List<ProcFormulaAndConditionDetailEntity> GetProcFormulaAndConditionDetailDBCollectionByProcItemDB(int ItemId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ItemId", SqlDbType.Int);
            parameter.Value = ItemId;
            return GetProcFormulaAndConditionDetailDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Personnel.p_ProcFormulaAndConditionDetailGetByProcItem", new[] {parameter}));
        }

        public List<ProcFormulaAndConditionDetailEntity> GetProcFormulaAndConditionDetailDBCollectionByProcOperatorDB(int OperatorId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@OperatorId", SqlDbType.Int);
            parameter.Value = OperatorId;
            return GetProcFormulaAndConditionDetailDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Personnel.p_ProcFormulaAndConditionDetailGetByProcOperator", new[] {parameter}));
        }

        public List<ProcFormulaAndConditionDetailEntity> GetProcFormulaAndConditionDetailDBCollectionByProcTextBoxTypeDB(int TextBoxTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@TextBoxTypeId", SqlDbType.Int);
            parameter.Value = TextBoxTypeId;
            return GetProcFormulaAndConditionDetailDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Personnel.p_ProcFormulaAndConditionDetailGetByProcTextBoxType", new[] {parameter}));
        }



        // own 
        public DataTable GetProcFormulaAndConditionDetailCollectionByProcFormulaAndConditionAndType(int itemDetailProcessId, int typeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@ItemDetailProcessId", SqlDbType.Int, 4),
                new SqlParameter("@TypeId", SqlDbType.Int, 4)
            };
            parameters[0].Value = itemDetailProcessId;
            parameters[1].Value = typeId;
            return _intranetDB.RunProcedureDS("Personnel.p_ProcFormulaAndConditionDetailCollectionGetAllByProcFormulaAndConditionAndType", parameters).Tables[0];

        }


        // ownFunction
        public void AddAndUpdateLinkListDB(int? currentBoxId, ProcFormulaAndConditionDetailEntity newBoxEntity, out int procFormulaAndConditionDetailId)
        {
            var intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@ProcFormulaAndConditionDetailId", SqlDbType.Int, 4),
                new SqlParameter("@FormulaAndConditionId", SqlDbType.Int),
                new SqlParameter("@PreLink", SqlDbType.Int),
                new SqlParameter("@PostLink", SqlDbType.Int),
                new SqlParameter("@UserAlterLink", SqlDbType.Int),
                new SqlParameter("@TextBoxTypeId", SqlDbType.Int),
                new SqlParameter("@OperatorId", SqlDbType.Int),
                new SqlParameter("@ItemId", SqlDbType.Int),
                new SqlParameter("@Value", SqlDbType.NVarChar, -1),
                new SqlParameter("@ValueUserAlter", SqlDbType.NVarChar, -1),
                new SqlParameter("@currentBoxId", SqlDbType.Int, -1),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = newBoxEntity.FormulaAndConditionId;
            parameters[2].Value = (newBoxEntity.PreLink == null) ? DBNull.Value : (object) newBoxEntity.PreLink;
            parameters[3].Value = (newBoxEntity.PostLink == null) ? DBNull.Value : (object) newBoxEntity.PostLink;
            parameters[4].Value = (newBoxEntity.UserAlterLink == null) ? DBNull.Value : (object) newBoxEntity.UserAlterLink;
            parameters[5].Value = newBoxEntity.TextBoxTypeId;
            parameters[6].Value = (newBoxEntity.OperatorId == null) ? DBNull.Value : (object) newBoxEntity.OperatorId;
            parameters[7].Value = (newBoxEntity.ItemId == null) ? DBNull.Value : (object) newBoxEntity.ItemId;
            parameters[8].Value = (newBoxEntity.Value == null) ? DBNull.Value : (object) FarsiToArabic.ToArabic(newBoxEntity.Value);
            parameters[9].Value = (newBoxEntity.ValueUserAlter == null) ? DBNull.Value : (object)FarsiToArabic.ToArabic(newBoxEntity.ValueUserAlter);
            parameters[10].Value = (currentBoxId == null) ? DBNull.Value : (object) currentBoxId;
            parameters[11].Direction = ParameterDirection.Output;

            intranetDB.RunProcedure("Personnel.p_ProcFormulaAndConditionDetailAddAndUpdateLinkList", parameters);
            var messageError = parameters[11].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            procFormulaAndConditionDetailId = int.Parse(parameters[0].Value.ToString());
        }

        #endregion

        // ownFunction
        public void DeleteAndUpdateLinkList(int procFormulaAndConditionDetailId)
        {
            var intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@ProcFormulaAndConditionDetailId", SqlDbType.Int, 4),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };

            parameters[0].Value = procFormulaAndConditionDetailId;
            parameters[1].Direction = ParameterDirection.Output;

            intranetDB.RunProcedure("Personnel.p_ProcFormulaAndConditionDetailDeleteAndUpdateLinkList", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void UpdateFinalExpression(int formulaAndConditionId, string finalFormula, string finalFormulaForUser)
        {
            var intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@FormulaAndConditionId", SqlDbType.Int, 4),
                new SqlParameter("@FinalExpression", SqlDbType.NVarChar,-1),
                new SqlParameter("@finalFormulaForUser", SqlDbType.NVarChar,-1),
                };
            parameters[0].Value = formulaAndConditionId;
            parameters[1].Value = finalFormula;
            parameters[2].Value = finalFormulaForUser;


            intranetDB.RunProcedure("Personnel.p_ProcFormulaAndConditionUpdateFinalExpression", parameters);                      
        }
    }

    #endregion
}