﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using ITC.Library.Classes;
using PersonnelExtended.Entity;

namespace Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.DAL
{

    #region Class "ProcItemDB"

    public class ProcItemDB
    {
        #region Methods :

        public void AddProcItemDB(ProcItemEntity procItemEntityParam, out int ItemId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@ItemId", SqlDbType.Int, 4),
                new SqlParameter("@ItemTitle", SqlDbType.NVarChar, 200),
                new SqlParameter("@ItemValueTypeId", SqlDbType.Int),
                new SqlParameter("@ShowOrder", SqlDbType.Int),
                new SqlParameter("@ObjectId", SqlDbType.Int),
                new SqlParameter("@SourceTableId", SqlDbType.Int),
                new SqlParameter("@SourceFieldName", SqlDbType.VarChar, 100),
                new SqlParameter("@Icon", SqlDbType.VarChar, 1024),
                new SqlParameter("@HasTitle", SqlDbType.Bit),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(procItemEntityParam.ItemTitle.Trim());
            parameters[2].Value = (procItemEntityParam.ItemValueTypeId == null) ? DBNull.Value : (object) procItemEntityParam.ItemValueTypeId;
            parameters[3].Value = (procItemEntityParam.ShowOrder == null) ? DBNull.Value : (object) procItemEntityParam.ShowOrder;
            parameters[4].Value = (procItemEntityParam.ObjectId == null) ? DBNull.Value : (object) procItemEntityParam.ObjectId;
            parameters[5].Value = (procItemEntityParam.SourceTableId == null) ? DBNull.Value : (object) procItemEntityParam.SourceTableId;
            parameters[6].Value = (procItemEntityParam.SourceFieldName == null) ? DBNull.Value : (object) procItemEntityParam.SourceFieldName;
            parameters[7].Value = (procItemEntityParam.Icon == null) ? DBNull.Value : (object) procItemEntityParam.Icon;
            parameters[8].Value = (procItemEntityParam.HasTitle == null) ? DBNull.Value : (object) procItemEntityParam.HasTitle;
            parameters[9].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Personnel.p_ProcItemAdd", parameters);
            var messageError = parameters[9].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            ItemId = int.Parse(parameters[0].Value.ToString());
        }

        public void UpdateProcItemDB(ProcItemEntity procItemEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@ItemId", SqlDbType.Int, 4),
                new SqlParameter("@ItemTitle", SqlDbType.NVarChar, 200),
                new SqlParameter("@ItemValueTypeId", SqlDbType.Int),
                new SqlParameter("@ShowOrder", SqlDbType.Int),
                new SqlParameter("@ObjectId", SqlDbType.Int),
                new SqlParameter("@SourceTableId", SqlDbType.Int),
                new SqlParameter("@SourceFieldName", SqlDbType.VarChar, 100),
                new SqlParameter("@Icon", SqlDbType.VarChar, 1024),
                new SqlParameter("@HasTitle", SqlDbType.Bit),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };

            parameters[0].Value = procItemEntityParam.ItemId;
            parameters[1].Value = FarsiToArabic.ToArabic(procItemEntityParam.ItemTitle.Trim());
            parameters[2].Value = (procItemEntityParam.ItemValueTypeId == null) ? DBNull.Value : (object) procItemEntityParam.ItemValueTypeId;
            parameters[3].Value = (procItemEntityParam.ShowOrder == null) ? DBNull.Value : (object) procItemEntityParam.ShowOrder;
            parameters[4].Value = (procItemEntityParam.ObjectId == null) ? DBNull.Value : (object) procItemEntityParam.ObjectId;
            parameters[5].Value = (procItemEntityParam.SourceTableId == null) ? DBNull.Value : (object) procItemEntityParam.SourceTableId;
            parameters[6].Value = (procItemEntityParam.SourceFieldName == null) ? DBNull.Value : (object) procItemEntityParam.SourceFieldName;
            parameters[7].Value = (procItemEntityParam.Icon == null) ? DBNull.Value : (object) procItemEntityParam.Icon;
            parameters[8].Value = (procItemEntityParam.HasTitle == null) ? DBNull.Value : (object) procItemEntityParam.HasTitle;
            parameters[9].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Personnel.p_ProcItemUpdate", parameters);
            var messageError = parameters[9].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteProcItemDB(ProcItemEntity ProcItemEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@ItemId", SqlDbType.Int, 4),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Value = ProcItemEntityParam.ItemId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Personnel.p_ProcItemDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public ProcItemEntity GetSingleProcItemDB(ProcItemEntity ProcItemEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                {
                    _intranetDB.GetParameter("@ItemId", DataTypes.integer, 4, ProcItemEntityParam.ItemId)
                };

                reader = _intranetDB.RunProcedureReader("Personnel.p_ProcItemGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetProcItemDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ProcItemEntity> GetAllProcItemDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetProcItemDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Personnel.p_ProcItemGetAll", new IDataParameter[] {}));
        }

        public List<ProcItemEntity> GetPageProcItemDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
            {
                new SqlParameter("@PageSize", SqlDbType.Int),
                new SqlParameter("@CurrentPage", SqlDbType.Int),
                new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
            };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_ProcItem";
            parameters[5].Value = "ItemId";
            DataSet ds = _intranetDB.RunProcedureDS("Personnel.p_TablesGetPage", parameters);
            return GetProcItemDBCollectionFromDataSet(ds, out count);
        }

        public ProcItemEntity GetProcItemDBFromDataReader(IDataReader reader)
        {
            return new ProcItemEntity(int.Parse(reader["ItemId"].ToString()),
                reader["ItemTitle"].ToString(),
                (Convert.IsDBNull(reader["ItemValueTypeId"])) ? null : (int?) reader["ItemValueTypeId"],
                (Convert.IsDBNull(reader["ShowOrder"])) ? null : (int?) reader["ShowOrder"],
                (Convert.IsDBNull(reader["ObjectId"])) ? null : (int?) reader["ObjectId"],
                (Convert.IsDBNull(reader["SourceTableId"])) ? null : (int?) reader["SourceTableId"],
                reader["SourceFieldName"].ToString(),
                reader["Icon"].ToString(),
                reader["CreationDate"].ToString(),
                reader["ModificationDate"].ToString(),
                (Convert.IsDBNull(reader["HasTitle"])) ? null : (bool?) (reader["HasTitle"]));
        }

        public List<ProcItemEntity> GetProcItemDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<ProcItemEntity> lst = new List<ProcItemEntity>();
                while (reader.Read())
                    lst.Add(GetProcItemDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ProcItemEntity> GetProcItemDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetProcItemDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<ProcItemEntity> GetProcItemDBCollectionByProcObjectDB(int ObjectId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ObjectId", SqlDbType.Int);
            parameter.Value = ObjectId;
            return GetProcItemDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Personnel.p_ProcItemGetByProcObject", new[] {parameter}));
        }

        // own Function
        public bool CheckItemObjectIsTree(ProcItemEntity procItemEntity)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@ItemId", SqlDbType.Int, 4),
                new SqlParameter("@IsTree", SqlDbType.Bit)
            };
            parameters[0].Value = procItemEntity.ItemId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Personnel.p_ProcItemCheckObjectIsTree", parameters);
            return bool.Parse(parameters[1].Value.ToString());
        }

        // own Function
        public DataTable ProcItemGetAllData(int itemId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@ItemId", SqlDbType.Int, 4),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, -1),
            };
            parameters[0].Value = itemId;
            parameters[1].Direction = ParameterDirection.Output;
            var ds = _intranetDB.RunProcedureDS("Personnel.p_ProcItemGetAllData", parameters);
            if (parameters[1].Value.ToString() != string.Empty)
            {
                throw new Exception(parameters[1].Value.ToString());
            }
            else
            {
                return ds.Tables[0];
            }
        }

        // own Function
        public DataTable ProcItemGroupGetAll()
        {
            var intranetDB = IntranetDB.GetIntranetDB();
            var ds = intranetDB.RunProcedureDS("Personnel.p_ProcItemGroupGetAll", new IDataParameter[] {});
            return ds.Tables[0];
        }

        // own Function
        public DataTable GetAllByItemGroup(int itemGroupId)
        {
            var intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@ItemGroupId", SqlDbType.Int, 4),
            };
            parameters[0].Value = itemGroupId;
            var ds = intranetDB.RunProcedureDS("Personnel.p_ProcItemGetAllByItemGroup", parameters);
            return ds.Tables[0];
        }

        #endregion
    }

    #endregion
}