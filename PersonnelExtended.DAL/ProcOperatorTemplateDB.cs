﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using PersonnelExtended.Entity;

namespace Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.DAL
{

    //-----------------------------------------------------

    #region Class "ProcOperatorTemplateDB"

    public class ProcOperatorTemplateDB
    {       

        #region Methods :

        public void AddProcOperatorTemplateDB(ProcOperatorTemplateEntity procOperatorTemplateEntityParam, out int OperatorTemplateId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@OperatorTemplateId", SqlDbType.Int, 4),
                new SqlParameter("@OperatorId", SqlDbType.Int),
                new SqlParameter("@TemplateOperatorId", SqlDbType.Int),
                new SqlParameter("@Priority", SqlDbType.Int),
                new SqlParameter("@IsActive", SqlDbType.Bit),              
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = procOperatorTemplateEntityParam.OperatorId;
            parameters[2].Value = procOperatorTemplateEntityParam.TemplateOperatorId;
            parameters[3].Value = procOperatorTemplateEntityParam.Priority;
            parameters[4].Value = procOperatorTemplateEntityParam.IsActive;
            parameters[5].Direction = ParameterDirection.Output;
            
            _intranetDB.RunProcedure("Personnel.p_ProcOperatorTemplateAdd", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            OperatorTemplateId = int.Parse(parameters[0].Value.ToString());
        }

        public void UpdateProcOperatorTemplateDB(ProcOperatorTemplateEntity procOperatorTemplateEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@OperatorTemplateId", SqlDbType.Int, 4),
                new SqlParameter("@OperatorId", SqlDbType.Int),
                new SqlParameter("@TemplateOperatorId", SqlDbType.Int),
                new SqlParameter("@Priority", SqlDbType.Int),
                new SqlParameter("@IsActive", SqlDbType.Bit),                
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };

            parameters[0].Value = procOperatorTemplateEntityParam.OperatorTemplateId;
            parameters[1].Value = procOperatorTemplateEntityParam.OperatorId;
            parameters[2].Value = procOperatorTemplateEntityParam.TemplateOperatorId;
            parameters[3].Value = procOperatorTemplateEntityParam.Priority;
            parameters[4].Value = procOperatorTemplateEntityParam.IsActive;
       parameters[5].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Personnel.p_ProcOperatorTemplateUpdate", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteProcOperatorTemplateDB(ProcOperatorTemplateEntity ProcOperatorTemplateEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@OperatorTemplateId", SqlDbType.Int, 4),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Value = ProcOperatorTemplateEntityParam.OperatorTemplateId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Personnel.p_ProcOperatorTemplateDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public ProcOperatorTemplateEntity GetSingleProcOperatorTemplateDB(ProcOperatorTemplateEntity ProcOperatorTemplateEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                {
                    _intranetDB.GetParameter("@OperatorTemplateId", DataTypes.integer, 4, ProcOperatorTemplateEntityParam.OperatorTemplateId)
                };

                reader = _intranetDB.RunProcedureReader("Personnel.p_ProcOperatorTemplateGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetProcOperatorTemplateDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ProcOperatorTemplateEntity> GetAllProcOperatorTemplateDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetProcOperatorTemplateDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Personnel.p_ProcOperatorTemplateGetAll", new IDataParameter[] {}));
        }

        public List<ProcOperatorTemplateEntity> GetAllIsActiveProcOperatorTemplateDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetProcOperatorTemplateDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Personnel.p_ProcOperatorTemplateGetAllIsActive", new IDataParameter[] { }));
        }

        public List<ProcOperatorTemplateEntity> GetPageProcOperatorTemplateDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
            {
                new SqlParameter("@PageSize", SqlDbType.Int),
                new SqlParameter("@CurrentPage", SqlDbType.Int),
                new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
            };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_ProcOperatorTemplate";
            parameters[5].Value = "OperatorTemplateId";
            DataSet ds = _intranetDB.RunProcedureDS("Personnel.p_TablesGetPage", parameters);
            return GetProcOperatorTemplateDBCollectionFromDataSet(ds, out count);
        }

        public ProcOperatorTemplateEntity GetProcOperatorTemplateDBFromDataReader(IDataReader reader)
        {
            return new ProcOperatorTemplateEntity(int.Parse(reader["OperatorTemplateId"].ToString()),
                int.Parse(reader["OperatorId"].ToString()),
                int.Parse(reader["TemplateOperatorId"].ToString()),
                int.Parse(reader["Priority"].ToString()),
                bool.Parse(reader["IsActive"].ToString()),
                reader["CreationDate"].ToString(),
                reader["ModificationDate"].ToString());
        }

        public List<ProcOperatorTemplateEntity> GetProcOperatorTemplateDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<ProcOperatorTemplateEntity> lst = new List<ProcOperatorTemplateEntity>();
                while (reader.Read())
                    lst.Add(GetProcOperatorTemplateDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ProcOperatorTemplateEntity> GetProcOperatorTemplateDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetProcOperatorTemplateDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<ProcOperatorTemplateEntity> GetProcOperatorTemplateDBCollectionByProcOperatorDB(int OperatorId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@OperatorId", SqlDbType.Int);
            parameter.Value = OperatorId;
            return GetProcOperatorTemplateDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Personnel.p_ProcOperatorTemplateGetByProcOperator", new[] {parameter}));
        }

        #endregion

    }

    #endregion
}