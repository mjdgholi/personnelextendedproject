﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using ITC.Library.Classes;
using PersonnelExtended.Entity;

namespace Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.DAL
{
    #region Class "ProcObjectTypeDB"

    public class ProcObjectTypeDB
    {
        #region Methods :

        public void AddProcObjectTypeDB(ProcObjectTypeEntity procObjectTypeEntityParam, out int ObjectTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@ObjectTypeId", SqlDbType.Int, 4),
                new SqlParameter("@ObjectTypeTitle", SqlDbType.NVarChar, 100),
                new SqlParameter("@ObjectTypeEnglieshTitle", SqlDbType.NVarChar, 100),
                new SqlParameter("@IsActive", SqlDbType.Bit),               
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(procObjectTypeEntityParam.ObjectTypeTitle.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(procObjectTypeEntityParam.ObjectTypeEnglieshTitle.Trim());
            parameters[3].Value = procObjectTypeEntityParam.IsActive;            
            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Personnel.p_ProcObjectTypeAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            ObjectTypeId = int.Parse(parameters[0].Value.ToString());
        }

        public void UpdateProcObjectTypeDB(ProcObjectTypeEntity procObjectTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@ObjectTypeId", SqlDbType.Int, 4),
                new SqlParameter("@ObjectTypeTitle", SqlDbType.NVarChar, 100),
                new SqlParameter("@ObjectTypeEnglieshTitle", SqlDbType.NVarChar, 100),
                new SqlParameter("@IsActive", SqlDbType.Bit),              
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };

            parameters[0].Value = procObjectTypeEntityParam.ObjectTypeId;
            parameters[1].Value = FarsiToArabic.ToArabic(procObjectTypeEntityParam.ObjectTypeTitle.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(procObjectTypeEntityParam.ObjectTypeEnglieshTitle.Trim());
            parameters[3].Value = procObjectTypeEntityParam.IsActive;
            parameters[4].Direction = ParameterDirection.Output;      

            _intranetDB.RunProcedure("Personnel.p_ProcObjectTypeUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteProcObjectTypeDB(ProcObjectTypeEntity ProcObjectTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@ObjectTypeId", SqlDbType.Int, 4),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Value = ProcObjectTypeEntityParam.ObjectTypeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Personnel.p_ProcObjectTypeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public ProcObjectTypeEntity GetSingleProcObjectTypeDB(ProcObjectTypeEntity ProcObjectTypeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                {
                    _intranetDB.GetParameter("@ObjectTypeId", DataTypes.integer, 4, ProcObjectTypeEntityParam.ObjectTypeId)
                };

                reader = _intranetDB.RunProcedureReader("Personnel.p_ProcObjectTypeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetProcObjectTypeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ProcObjectTypeEntity> GetAllProcObjectTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetProcObjectTypeDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Personnel.p_ProcObjectTypeGetAll", new IDataParameter[] {}));
        }

        public List<ProcObjectTypeEntity> GetAllIsActiveProcObjectTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetProcObjectTypeDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Personnel.p_ProcObjectTypeGetAllIsActive", new IDataParameter[] { }));
        }

        public List<ProcObjectTypeEntity> GetPageProcObjectTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
            {
                new SqlParameter("@PageSize", SqlDbType.Int),
                new SqlParameter("@CurrentPage", SqlDbType.Int),
                new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
            };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_ProcObjectType";
            parameters[5].Value = "ObjectTypeId";
            DataSet ds = _intranetDB.RunProcedureDS("Personnel.p_TablesGetPage", parameters);
            return GetProcObjectTypeDBCollectionFromDataSet(ds, out count);
        }

        public ProcObjectTypeEntity GetProcObjectTypeDBFromDataReader(IDataReader reader)
        {
            return new ProcObjectTypeEntity(int.Parse(reader["ObjectTypeId"].ToString()),
                reader["ObjectTypeTitle"].ToString(),
                reader["ObjectTypeEnglieshTitle"].ToString(),
                bool.Parse(reader["IsActive"].ToString()),
                reader["CreationDate"].ToString(),
                reader["ModificationDate"].ToString());
        }

        public List<ProcObjectTypeEntity> GetProcObjectTypeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<ProcObjectTypeEntity> lst = new List<ProcObjectTypeEntity>();
                while (reader.Read())
                    lst.Add(GetProcObjectTypeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ProcObjectTypeEntity> GetProcObjectTypeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetProcObjectTypeDBCollectionFromDataReader(ds.CreateDataReader());
        }

        #endregion
    }

    #endregion
}