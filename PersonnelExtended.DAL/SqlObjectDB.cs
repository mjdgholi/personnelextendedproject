﻿using System;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.DAL
{
    public class SqlObjectDB
    {
        public DataTable GetAllFunction(string schemaName)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@SchemaName", SqlDbType.NVarChar,100),                
            };
            parameters[0].Value = schemaName;
           return _intranetDB.RunProcedureDS("Personnel.p_ProcGetAllFunction", parameters).Tables[0];
            
        }

        public string SqlQueryValidation(string sqlQuery)
        {
            var intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@SqlQuery", SqlDbType.NVarChar,-1),                
                new SqlParameter("@MessageError", SqlDbType.NVarChar,-1),                
            };
            parameters[0].Value = sqlQuery;
            parameters[1].Direction =ParameterDirection.Output;
             intranetDB.RunProcedure("Personnel.p_SqlQueryValidation", parameters);
             return parameters[1].Value.ToString();
        }
    }
}