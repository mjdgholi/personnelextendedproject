﻿namespace PersonnelExtended.Entity
{
    public class ProcOperatorTemplateEntity
    {
         #region Properties :

        public int OperatorTemplateId { get; set; }
        public int OperatorId { get; set; }

        public int TemplateOperatorId { get; set; }

        public int Priority { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public ProcOperatorTemplateEntity()
        {
        }

        public ProcOperatorTemplateEntity(int _OperatorTemplateId, int OperatorId, int TemplateOperatorId, int Priority, bool IsActive, string CreationDate, string ModificationDate)
        {
            OperatorTemplateId = _OperatorTemplateId;
            this.OperatorId = OperatorId;
            this.TemplateOperatorId = TemplateOperatorId;
            this.Priority = Priority;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion 
    }
}