﻿namespace PersonnelExtended.Entity
{
    public class ProcFormulaAndConditionDetailEntity
    {
         #region Properties :

        public int ProcFormulaAndConditionDetailId { get; set; }
        public int FormulaAndConditionId { get; set; }

        public int? PreLink { get; set; }

        public int? PostLink { get; set; }

        public int? UserAlterLink { get; set; }

        public int TextBoxTypeId { get; set; }

        public int? OperatorId { get; set; }

        public int? ItemId { get; set; }

        public string Value { get; set; }

        public string ValueUserAlter { get; set; } // در هنگام افزودن مقدار نمایش کاربری از طریق این فیلد ارسال میگردد

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public ProcFormulaAndConditionDetailEntity()
        {
        }

        public ProcFormulaAndConditionDetailEntity(int _ProcFormulaAndConditionDetailId, int FormulaAndConditionId, int? PreLink, int? PostLink, int? UserAlterLink, int TextBoxTypeId, int? OperatorId, int? ItemId, string Value, string CreationDate, string ModificationDate)
        {
            ProcFormulaAndConditionDetailId = _ProcFormulaAndConditionDetailId;
            this.FormulaAndConditionId = FormulaAndConditionId;
            this.PreLink = PreLink;
            this.PostLink = PostLink;
            this.UserAlterLink = UserAlterLink;
            this.TextBoxTypeId = TextBoxTypeId;
            this.OperatorId = OperatorId;
            this.ItemId = ItemId;
            this.Value = Value;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion 
    }
}