﻿namespace PersonnelExtended.Entity
{
    public class ProcTextBoxTypeEntity
    {
        #region Properties :

        public int TextBoxTypeId { get; set; }
        public string TextBoxTypeTitle { get; set; }

        public string CSSName { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public ProcTextBoxTypeEntity()
        {
        }

        public ProcTextBoxTypeEntity(int _TextBoxTypeId, string TextBoxTypeTitle, string CSSName, string CreationDate, string ModificationDate)
        {
            TextBoxTypeId = _TextBoxTypeId;
            this.TextBoxTypeTitle = TextBoxTypeTitle;
            this.CSSName = CSSName;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
    }
}