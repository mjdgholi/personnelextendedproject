﻿namespace PersonnelExtended.Entity
{
    public class ProcObjectEntity
    {
         
        #region Properties :

        public int ObjectId { get; set; }
        public string ObjectTitle { get; set; }

        public string ObjectTitlePersian { get; set; }
        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public int ObjectTypeId { get; set; }

        public int? SystemId { get; set; }

        public bool IsHierarchy { get; set; }

        public bool IsActive { get; set; }

        #endregion

        #region Constrauctors :

        public ProcObjectEntity()
        {
        }

        public ProcObjectEntity(int _ObjectId, string ObjectTitle,string objectTitlePersian, string CreationDate, string ModificationDate, int ObjectTypeId, int? SystemId, bool IsHierarchy, bool IsActive)
        {
            ObjectId = _ObjectId;
            this.ObjectTitle = ObjectTitle;
            this.ObjectTitlePersian = objectTitlePersian;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.ObjectTypeId = ObjectTypeId;
            this.SystemId = SystemId;
            this.IsHierarchy = IsHierarchy;
            this.IsActive = IsActive;

        }

        #endregion
    }
}