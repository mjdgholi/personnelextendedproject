﻿namespace PersonnelExtended.Entity
{
    public class ProcItemValueTypeEntity
    {
         
        #region Properties :

        public int ItemValueTypeId { get; set; }
        public string ItemValueTypeTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsViewed { get; set; }



        #endregion

        #region Constrauctors :

        public ProcItemValueTypeEntity()
        {
        }

        public ProcItemValueTypeEntity(int _ItemValueTypeId, string ItemValueTypeTitle, string CreationDate, string ModificationDate, bool IsViewed)
        {
            ItemValueTypeId = _ItemValueTypeId;
            this.ItemValueTypeTitle = ItemValueTypeTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsViewed = IsViewed;

        }

        #endregion
    }
}