﻿namespace PersonnelExtended.Entity
{
    public class ProcOperatorEntity
    {
        #region Properties :

        public int OperatorId { get; set; }
        public string OperatorTitle { get; set; }

        public string SqlOperator { get; set; }

        public string SampleTextForToolTip { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public ProcOperatorEntity()
        {
        }

        public ProcOperatorEntity(int _OperatorId, string OperatorTitle, string SqlOperator, string SampleTextForToolTip, bool IsActive, string CreationDate, string ModificationDate)
        {
            OperatorId = _OperatorId;
            this.OperatorTitle = OperatorTitle;
            this.SqlOperator = SqlOperator;
            this.SampleTextForToolTip = SampleTextForToolTip;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
    }
}