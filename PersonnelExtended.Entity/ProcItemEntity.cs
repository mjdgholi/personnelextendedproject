﻿namespace PersonnelExtended.Entity
{
    public class ProcItemEntity
    {
         #region Properties :

        public int ItemId { get; set; }
        public string ItemTitle { get; set; }

        public int? ItemValueTypeId { get; set; }

        public int? ShowOrder { get; set; }

        public int? ObjectId { get; set; }

        public int? SourceTableId { get; set; }

        public string SourceFieldName { get; set; }

        public string Icon { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool? HasTitle { get; set; }



        #endregion

        #region Constrauctors :

        public ProcItemEntity()
        {
        }

        public ProcItemEntity(int _ItemId, string ItemTitle, int? ItemValueTypeId, int? ShowOrder, int? ObjectId, int? SourceTableId, string SourceFieldName, string Icon, string CreationDate, string ModificationDate, bool? HasTitle)
        {
            ItemId = _ItemId;
            this.ItemTitle = ItemTitle;
            this.ItemValueTypeId = ItemValueTypeId;
            this.ShowOrder = ShowOrder;
            this.ObjectId = ObjectId;
            this.SourceTableId = SourceTableId;
            this.SourceFieldName = SourceFieldName;
            this.Icon = Icon;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.HasTitle = HasTitle;

        }

        #endregion
 
    }
}