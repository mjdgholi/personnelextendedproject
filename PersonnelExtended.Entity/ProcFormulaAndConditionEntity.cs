﻿namespace PersonnelExtended.Entity
{
    public class ProcFormulaAndConditionEntity
    {
        
        #region Properties :

        public int FormulaAndConditionId { get; set; }
        public int ItemDetailProcessId { get; set; }

        public bool TypeId { get; set; }

        public string ConditionalExpressionForUser { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public ProcFormulaAndConditionEntity()
        {
        }

        public ProcFormulaAndConditionEntity(int _FormulaAndConditionId, int ItemDetailProcessId, bool TypeId, string ConditionalExpressionForUser, string CreationDate, string ModificationDate)
        {
            FormulaAndConditionId = _FormulaAndConditionId;
            this.ItemDetailProcessId = ItemDetailProcessId;
            this.TypeId = TypeId;
            this.ConditionalExpressionForUser = ConditionalExpressionForUser;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion 
    }
}