﻿namespace PersonnelExtended.Entity
{
    public class ProcObjectTypeEntity
    {
        #region Properties :

        public int ObjectTypeId { get; set; }
        public string ObjectTypeTitle { get; set; }

        public string ObjectTypeEnglieshTitle { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }


        #endregion

        #region Constrauctors :

        public ProcObjectTypeEntity()
        {
        }

        public ProcObjectTypeEntity(int _ObjectTypeId, string ObjectTypeTitle, string ObjectTypeEnglieshTitle, bool IsActive, string CreationDate, string ModificationDate)
        {
            ObjectTypeId = _ObjectTypeId;
            this.ObjectTypeTitle = ObjectTypeTitle;
            this.ObjectTypeEnglieshTitle = ObjectTypeEnglieshTitle;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
    }
}