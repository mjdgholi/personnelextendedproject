﻿namespace PersonnelExtended.Entity
{
    public class ProcItemDetailProcessEntity
    {
           #region Properties :

        public int ItemDetailProcessId { get; set; }
        public int ItemId { get; set; }

        public string Condition { get; set; }

        public bool IsBuiltInFunction { get; set; }

        public string Formula { get; set; }

        public int? FunctionId { get; set; }

        public int Priority { get; set; }

        public string FromDate { get; set; }

        public string ToDate { get; set; }

        public bool IsValid { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public ProcItemDetailProcessEntity()
        {
        }

        public ProcItemDetailProcessEntity(int _ItemDetailProcessId, int ItemId, string Condition, bool IsBuiltInFunction, string Formula, int? FunctionId, int Priority, string FromDate, string ToDate, bool IsValid, string CreationDate, string ModificationDate)
        {
            ItemDetailProcessId = _ItemDetailProcessId;
            this.ItemId = ItemId;
            this.Condition = Condition;
            this.IsBuiltInFunction = IsBuiltInFunction;
            this.Formula = Formula;
            this.FunctionId = FunctionId;
            this.Priority = Priority;
            this.FromDate = FromDate;
            this.ToDate = ToDate;
            this.IsValid = IsValid;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
 
    }
}