﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ITC.Library.Classes;
using PersonnelExtended.BL;
using PersonnelExtended.Entity;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.FormulaEditor.Operational
{
    public partial class ProcItemDetailProcessPage : ItcBaseControl
    {//
        #region PublicParam:

        private readonly ProcItemDetailProcessBL _procItemDetailProcessBL = new ProcItemDetailProcessBL();
        private readonly ProcObjectBL _procObject = new ProcObjectBL();
        private readonly SqlObjectBL _sqlObjectBL=new SqlObjectBL();
        private readonly ProcObjectBL _procObjectBL = new ProcObjectBL();        
        private const string TableName = "[Personnel].[v_ProcItemDetailProcess]";
        private const string PrimaryKey = "ItemDetailProcessId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
            CmbIsBuiltInFunction.SelectedIndex = 2;
            LblFormulaTitle.Visible = false;
            CmbFormula.Visible = false;
            CmbIsValid.SelectedIndex = 1;
            //TxtFromDate.Enabled = true;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
          //  TxtFromDate.Enabled = false;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {           
            TxtPriority.Text = string.Empty;
            TxtFromDate.Text = string.Empty;
            TxtToDate.Text = string.Empty;
        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var procItemDetailProcessEntity = new ProcItemDetailProcessEntity()
            {
                ItemDetailProcessId = Int32.Parse(ViewState["ItemDetailProcessId"].ToString())
            };
            var myItemDetailProcess = _procItemDetailProcessBL.GetSingleById(procItemDetailProcessEntity);
            TxtPriority.Text = myItemDetailProcess.Priority.ToString();
            TxtFromDate.Text = myItemDetailProcess.FromDate;
            TxtToDate.Text = myItemDetailProcess.ToDate;
            CmbIsBuiltInFunction.SelectedValue = myItemDetailProcess.IsBuiltInFunction.ToString();
            if (myItemDetailProcess.IsBuiltInFunction)
            {
                SetCmbFunction();
                CmbFormula.SelectedValue = myItemDetailProcess.Formula;
                CmbFormula.Visible = true;
                LblFormulaTitle.Visible = true;
            }
            else
            {
                CmbFormula.Visible = false;
                LblFormulaTitle.Visible = false;
            }
            CmbIsValid.SelectedValue = myItemDetailProcess.IsValid.ToString();
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }              
       
        private GridParamEntity SetGridParam(int pageSize, int currentpPage, int rowSelectId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdItemDetailProcess,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectId = rowSelectId
            };
            return gridParamEntity;
        }

        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " ItemId=" + Int32.Parse(Session["ItemId"].ToString());
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";                          
                var gridParamEntity = SetGridParam(grdItemDetailProcess.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
            }
            catch (Exception ex)
            {
                 
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }       

        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int itemId;
                ViewState["WhereClause"] = " ItemId=" + Int32.Parse(Session["ItemId"].ToString());

                var procIteDetailProcessmEntity = new ProcItemDetailProcessEntity()
                {
                    ItemId = Int32.Parse(Session["ItemId"].ToString()),
                    FromDate = ItcToDate.ShamsiToMiladi(TxtFromDate.TextWithLiterals),
                    ToDate = ItcToDate.ShamsiToMiladi(TxtToDate.TextWithLiterals), 
                    Condition = "",
                    IsBuiltInFunction = bool.Parse(CmbIsBuiltInFunction.SelectedValue),
                    Formula = (CmbIsBuiltInFunction.SelectedIndex==1)?CmbFormula.SelectedItem.Text:null,
                    IsValid = bool.Parse(CmbIsValid.SelectedValue),
                    Priority = Int32.Parse(TxtPriority.Text)
                    
                };

                _procItemDetailProcessBL.Add(procIteDetailProcessmEntity, out itemId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdItemDetailProcess.MasterTableView.PageSize, 0, itemId);
                DatabaseManager.ItcGetPage(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " ItemId=" + Int32.Parse(Session["ItemId"].ToString());
                if (TxtPriority.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Priority =" + TxtPriority.Text;
                if (TxtFromDate.Text.Trim() !="")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And FromDate=" + ItcToDate.ShamsiToMiladi(TxtFromDate.TextWithLiterals);              
                if (CmbIsBuiltInFunction.SelectedIndex>0)
                {
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And IsBuiltInFunction='" + CmbIsBuiltInFunction.SelectedValue + "'";   
                }
                if (CmbFormula.SelectedIndex > 0)
                {
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And Formula='" + CmbFormula.SelectedItem.Text + "'";
                }
                if (CmbIsValid.SelectedIndex>0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And isvalid='" + CmbIsValid.SelectedValue +"'";   
                grdItemDetailProcess.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdItemDetailProcess.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
                if (grdItemDetailProcess.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " ItemId=" + Int32.Parse(Session["ItemId"].ToString());
                var gridParamEntity = SetGridParam(grdItemDetailProcess.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var procItemEntity = new ProcItemDetailProcessEntity()
                {
                    ItemDetailProcessId = Int32.Parse(ViewState["ItemDetailProcessId"].ToString()),
                    ItemId = Int32.Parse(Session["ItemId"].ToString()),
                    FromDate = ItcToDate.ShamsiToMiladi(TxtFromDate.TextWithLiterals),
                    ToDate = ItcToDate.ShamsiToMiladi(TxtToDate.TextWithLiterals),      
                    Condition = "",
                    IsBuiltInFunction = bool.Parse(CmbIsBuiltInFunction.SelectedValue),
                    Formula = (CmbIsBuiltInFunction.SelectedIndex == 1) ? CmbFormula.SelectedItem.Text : null,
                    IsValid = bool.Parse(CmbIsValid.SelectedValue),
                    Priority = Int32.Parse(TxtPriority.Text)
                };
                _procItemDetailProcessBL.Update(procItemEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdItemDetailProcess.MasterTableView.PageSize, 0, Int32.Parse(ViewState["ItemDetailProcessId"].ToString()));
                DatabaseManager.ItcGetPage(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdItemDetailProcess_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["ItemDetailProcessId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = " ItemId=" + Int32.Parse(Session["ItemId"].ToString());

                    var procItemEntity = new ProcItemDetailProcessEntity()
                    {
                        ItemDetailProcessId = Int32.Parse(e.CommandArgument.ToString()),
                    };
                    _procItemDetailProcessBL.Delete(procItemEntity);
                    var gridParamEntity = SetGridParam(grdItemDetailProcess.MasterTableView.PageSize, 0, -1);
                    DatabaseManager.ItcGetPage(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }

                if (e.CommandName == "_MyCloseDate")
                {                 
                    var procItemDetailProcessId = Int32.Parse(e.CommandArgument.ToString());
                    var item = e.Item as GridDataItem;
                    var txtEndDate = (ITC.Library.Controls.CustomeRadMaskedTextBox) item.FindControl("TxtEndDate");
                    var endDate = (txtEndDate.Text.Trim() == "") ? "" : ITC.Library.Classes.ItcToDate.ShamsiToMiladi(txtEndDate.TextWithLiterals);
                    var procItemDetailProcessEntity = new ProcItemDetailProcessEntity
                                                              {
                                                                 ItemDetailProcessId = procItemDetailProcessId,
                                                                 ToDate = endDate
                                                              };
                    _procItemDetailProcessBL.CloseDateProcItemDetailProcessDB(procItemDetailProcessEntity);
                    var gridParamEntity = SetGridParam(grdItemDetailProcess.MasterTableView.PageSize, 0, -1);
                    DatabaseManager.ItcGetPage(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>
        protected void grdItemDetailProcess_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdItemDetailProcess.MasterTableView.PageSize, e.NewPageIndex, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>
        protected void grdItemDetailProcess_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdItemDetailProcess.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>
        protected void grdItemDetailProcess_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdItemDetailProcess.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        private void SetCmbFunction()
        {
            CmbFormula.Items.Clear();
            CmbFormula.DataTextField = "name";
            CmbFormula.DataValueField = "name";
            CmbFormula.DataSource = _sqlObjectBL.GetAllFunction("Personnel");
            CmbFormula.DataBind();
        }

        protected void CmbIsBuiltInFunction_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (CmbIsBuiltInFunction.SelectedIndex == 1) // is builtin
            {
               SetCmbFunction();
                CmbFormula.Visible = true;
                LblFormulaTitle.Visible = true;
            }
            else
            {
                CmbFormula.Items.Clear();
                CmbFormula.Visible = false;
                LblFormulaTitle.Visible = false;
            }
        }

        #endregion
    }
}