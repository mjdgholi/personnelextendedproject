﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProcItemDetailProcessPage.ascx.cs"
    Inherits="Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.FormulaEditor.Operational.ProcItemDetailProcessPage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<style type="text/css">
    .TextLtr {
        direction: ltr;
        text-align: left;
    }
</style>
<asp:Panel ID="Panel1" runat="server">
    <table style="width: 100%; background-color: #F0F8FF;" dir="rtl">
        <tr>
            <td dir="rtl" style="width: 100%;" valign="top">
                <asp:Panel ID="pnlButton" runat="server">
                    <table>
                        <tr>
                            <td>
                                <cc1:CustomRadButton ID="btnSave" runat="server" CustomeButtonType="Add" OnClick="btnSave_Click"
                                    ValidationGroup="Detail">
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnSearch" runat="server" CustomeButtonType="Search" CausesValidation="False"
                                    OnClick="btnSearch_Click">
                                    <Icon PrimaryIconCssClass="rbSearch" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnShowAll" runat="server" CustomeButtonType="ShowAll" CausesValidation="False"
                                    OnClick="btnShowAll_Click">
                                    <Icon PrimaryIconCssClass="rbRefresh" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnEdit" runat="server" CustomeButtonType="Edit"
                                    OnClick="btnEdit_Click" ValidationGroup="Detail">
                                    <Icon PrimaryIconCssClass="rbEdit" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnBack" runat="server" CustomeButtonType="Back" OnClick="btnBack_Click">
                                    <Icon PrimaryIconCssClass="rbPrevious" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <asp:Panel ID="pnlDetail" runat="server" Width="100%">
                    <table>
                        <tr>
                            <td nowrap="nowrap">
                                <asp:Label runat="server" ID="lblPriority">اولویت <font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadNumericTextBox ID="TxtPriority" runat="server" ValidationGroup="Detail">
                                    <NumberFormat DecimalDigits="0" ZeroPattern="n" />
                                </telerik:RadNumericTextBox>
                                <asp:RequiredFieldValidator ID="rfvPrioroty" runat="server" ControlToValidate="TxtPriority"
                                    ErrorMessage="*" ValidationGroup="Detail"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">
                                <asp:Label ID="lblPriority0" runat="server">آیا تابع توکار است؟ <font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <cc1:CustomRadComboBox ID="CmbIsBuiltInFunction" runat="server" AppendDataBoundItems="True"
                                                Filter="Contains" MarkFirstMatch="True" ValidationGroup="Detail"
                                                AutoPostBack="True" CausesValidation="False"
                                                OnSelectedIndexChanged="CmbIsBuiltInFunction_SelectedIndexChanged">
                                                <Items>
                                                    <telerik:RadComboBoxItem Owner="CmbIsBuiltInFunction" Text="" Value="" />
                                                    <telerik:RadComboBoxItem Owner="CmbIsBuiltInFunction" Text="بلی" Value="True" />
                                                    <telerik:RadComboBoxItem Owner="CmbIsBuiltInFunction" Text="خیر" Value="False" />
                                                </Items>
                                            </cc1:CustomRadComboBox>
                                            <asp:RequiredFieldValidator ID="rfvIsBuiltInFunction" runat="server" ControlToValidate="CmbIsBuiltInFunction"
                                                ErrorMessage="*" ValidationGroup="Detail"></asp:RequiredFieldValidator>
                                        </td>
                                        <td></td>
                                        <td>
                                            <asp:Label ID="LblFormulaTitle" runat="server">فرمول:</asp:Label>
                                        </td>
                                        <td>
                                            <cc1:CustomRadComboBox ID="CmbFormula" runat="server" AppendDataBoundItems="True"
                                                Filter="Contains" MarkFirstMatch="True" ValidationGroup="Detail"
                                                CheckedItemsTexts="DisplayAllInInput" ExpandDirection="Up" Height="300px"
                                                Width="400px">

                                                <ExpandAnimation Type="InQuad" />

                                            </cc1:CustomRadComboBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">
                                <asp:Label ID="LblFromDate" runat="server">تاریخ آغاز:</asp:Label>
                            </td>
                            <td align="right" dir="ltr">
                                <asp:RequiredFieldValidator ID="rfvFromDate0" runat="server" ControlToValidate="TxtFromDate"
                                    ErrorMessage="*" ValidationGroup="Detail"></asp:RequiredFieldValidator>
                                <cc1:CustomeRadMaskedTextBox ID="TxtFromDate" runat="server" Width="80px" ValidationGroup="Detail">
                                </cc1:CustomeRadMaskedTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">
                                <asp:Label ID="LblToDate" runat="server">تاریخ پایان:</asp:Label>
                                </td>
                            <td align="right" dir="ltr">
                                <cc1:CustomeRadMaskedTextBox ID="TxtToDate" runat="server" ValidationGroup="Detail" Width="80px">
                                </cc1:CustomeRadMaskedTextBox>
                            </td>
                                </tr>
                        <tr>
                            <td nowrap="nowrap">
                                <asp:Label ID="LblIsValid" runat="server">آیا رکورد معتبر است؟</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="CmbIsValid" runat="server" AppendDataBoundItems="True"
                                    Filter="Contains" MarkFirstMatch="True" ValidationGroup="Detail">
                                    <Items>
                                        <telerik:RadComboBoxItem Text="" Value="" />
                                        <telerik:RadComboBoxItem Text="بلی" Value="True" />
                                        <telerik:RadComboBoxItem Text="خیر" Value="False" />
                                    </Items>
                                </cc1:CustomRadComboBox>
                                <asp:RequiredFieldValidator ID="rfvIsValid" runat="server" ControlToValidate="CmbIsValid"
                                    ErrorMessage="*" ValidationGroup="Detail"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top" bgcolor="White">
                <asp:Panel ID="pnlGrid" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <telerik:RadGrid ID="grdItemDetailProcess" runat="server" AllowCustomPaging="True"
                                    AllowPaging="True" AllowSorting="True" CellSpacing="0" GridLines="None" HorizontalAlign="Center"
                                    AutoGenerateColumns="False" OnItemCommand="grdItemDetailProcess_ItemCommand"
                                    OnPageIndexChanged="grdItemDetailProcess_PageIndexChanged" OnPageSizeChanged="grdItemDetailProcess_PageSizeChanged"
                                    OnSortCommand="grdItemDetailProcess_SortCommand">
                                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Office2007">
                                    </HeaderContextMenu>
                                    <MasterTableView DataKeyNames="ItemDetailProcessId" Dir="RTL" GroupsDefaultExpanded="False"
                                        NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="Priority" HeaderText="ترتیب" SortExpression="Priority">
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="FromDateShamsi" HeaderText="تاریخ آغاز" SortExpression="FromDateShamsi">
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ToDateShamsi" HeaderText="تاریخ پایان" SortExpression="ToDateShamsi">
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridTemplateColumn HeaderText="شرط" FilterControlAltText="Filter TemplateColumn1 column">
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TxtConditionForUser" CssClass="TextLtr" runat="server" Text='<%#Eval("ConditionForUser")%>' TextMode="MultiLine" Height="50" Width="350" Font-Names="Tahoma" />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="فرمول" FilterControlAltText="Filter TemplateColumn1 column">
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TxtFormulaForUser" CssClass="TextLtr" runat="server" Text='<%#Eval("FormulaForUser")%>' TextMode="MultiLine" Height="50" Width="350" Font-Names="Tahoma" />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                            <telerik:GridBoundColumn DataField="IsBuiltInFunctionText" HeaderText="آیا توساخته است" SortExpression="IsBuiltInFunctionText">
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridTemplateColumn HeaderText="ثبت شرط" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="_Condition">
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                <ItemTemplate>
                                                    <cc1:SelectControl ID="ConditionControl" runat="server" imageName="Editor" Width="1" RadWindowHeight="700" RadWindowWidth="1000"
                                                        PortalPathUrl="PersonnelExtendedProject/PersonnelExtended/FormulaEditor/Modal/FormulaAndConditionPage.aspx"
                                                        WhereClause='<%#Eval("ItemDetailProcessId")+",0" %>' />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="ثبت فرمول" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="_Formula">
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                <ItemTemplate>
                                                    <cc1:SelectControl ID="FormulaControl" runat="server" imageName="RunQuery" Width="1" RadWindowHeight="700" RadWindowWidth="1000"
                                                        PortalPathUrl="PersonnelExtendedProject/PersonnelExtended/FormulaEditor/Modal/FormulaAndConditionPage.aspx"
                                                        WhereClause='<%#Eval("ItemDetailProcessId")+",1" %>' />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="بستن بازه" HeaderTooltip="بستن بازه" FilterControlAltText="Filter TemplateColumn column"
                                                UniqueName="CloseDateUnq" Visible="False">
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                <ItemTemplate>
                                                    <table dir="rtl" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td dir="ltr" align="left" cellpadding="0" cellspacing="0">
                                                                <cc1:CustomeRadMaskedTextBox runat="server" ID="TxtEndDate" ToolTip="بستن بازه" />
                                                            </td>
                                                            <td align="right" cellpadding="0" cellspacing="0">
                                                                <asp:ImageButton ID="ImgButtonCloseDate" runat="server" CausesValidation="false"
                                                                    ToolTip="بستن بازه" CommandArgument='<%#Eval("ItemDetailProcessId").ToString() %>'
                                                                    CommandName="_MyCloseDate" ForeColor="#000066" ImageUrl="../../Images/CloseDate.png" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="ویرایش" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="TemplateColumn1">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton" runat="server" CausesValidation="false" CommandArgument='<%#Eval("ItemDetailProcessId").ToString() %>'
                                                        CommandName="_MyِEdit" ForeColor="#000066" ImageUrl="../../Images/Edit.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="حذف" FilterControlAltText="Filter TemplateColumn column"
                                                UniqueName="TemplateColumn">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton1" runat="server" CausesValidation="false" OnClientClick="return confirm('آیا اطلاعات رکورد حذف شود؟');"
                                                        CommandArgument='<%#Eval("ItemDetailProcessId").ToString() %>' CommandName="_MyِDelete"
                                                        ForeColor="#000066" ImageUrl="../../Images/Delete.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                        </ExpandCollapseColumn>
                                        <EditFormSettings>
                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                            </EditColumn>
                                        </EditFormSettings>
                                        <PagerStyle FirstPageToolTip="صفحه اول" HorizontalAlign="Center" LastPageToolTip="صفحه آخر"
                                            NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; به &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                                            PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                                            VerticalAlign="Middle" />
                                    </MasterTableView><FilterMenu EnableImageSprites="False">
                                    </FilterMenu>
                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnSave">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnShowAll">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnEdit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnBack">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="CmbIsBuiltInFunction">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="LblFormulaTitle" />
                <telerik:AjaxUpdatedControl ControlID="CmbFormula"
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="grdItemDetailProcess">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
</telerik:RadAjaxLoadingPanel>
