﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;
using ITC.Library.Classes.ItcException;
using PersonnelExtended.BL;
using PersonnelExtended.Entity;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.FormulaEditor.Modal
{
    public partial class FormulaAndConditionPage : System.Web.UI.Page
    {
        private int _itemDetailProcessId;
        private int _type;
        private int _formulaAndConditionId;
        private readonly ProcFormulaAndConditionDetailBL _procFormulaAndConditionDetailBL = new ProcFormulaAndConditionDetailBL();
        private readonly ProcItemDetailProcessBL _procItemDetailProcessBL = new ProcItemDetailProcessBL();
        private readonly ProcFormulaAndConditionBL _procFormulaAndConditionBL = new ProcFormulaAndConditionBL();
        private readonly ProcItemBL _procItemBL = new ProcItemBL();
        private ProcOperatorBL _procOperatorBL = new ProcOperatorBL();
        private SqlObjectBL _sqlObjectBL = new SqlObjectBL();

        protected void Page_Load(object sender, EventArgs e)
        {

            var param = Request.QueryString["WhereClause"];
            var paramArr = param.Split(',');
            if (paramArr.Count() == 2)
            {
                _itemDetailProcessId = Int32.Parse(paramArr[0]);
                _type = Int32.Parse(paramArr[1]);
                _formulaAndConditionId = _procFormulaAndConditionBL.GetSingleByDetailProcessByType(_itemDetailProcessId, _type).FormulaAndConditionId;
            }
            if (!IsPostBack)
            {
                if (_type == 0)
                {
                    LblPageTitle.Text = "ثبت شرط";

                    LblConditionOrFormulaTitleForUser.Text = "فرمول: " + _procFormulaAndConditionBL.GetSingleByDetailProcessByType(_itemDetailProcessId, 1).ConditionalExpressionForUser;
                }
                else
                {
                    LblPageTitle.Text = "ثبت فرمول";

                    LblConditionOrFormulaTitleForUser.Text = "شرط: " + _procFormulaAndConditionBL.GetSingleByDetailProcessByType(_itemDetailProcessId, 0).ConditionalExpressionForUser;
                }
                var myDetailProcess = _procItemDetailProcessBL.GetSingleById(new ProcItemDetailProcessEntity() {ItemDetailProcessId = _itemDetailProcessId});
                var itemId = myDetailProcess.ItemId;
                var myItem = _procItemBL.GetSingleById(new ProcItemEntity() {ItemId = itemId});
                LblItemTitle.Text = "عنوان آیتم: " + myItem.ItemTitle;

                SetCmbItem();
                SetCmbItemGroup();
                SetClearToolBox();

            }
        }


        private void SetClearToolBox()
        {
            CmbItem.ClearSelection();          
            TxtValue.Text = string.Empty;
            txtItemValue.Text = string.Empty;
            HdItemValueModal.Value = string.Empty;
            pnlMain.DefaultButton = "BtnAdd";
            BtnAdd.Focus();
            LblTestSqlExpression.Text = "";
        }

        protected override void OnInit(EventArgs e)
        {
            var param = Request.QueryString["WhereClause"];
            var paramArr = param.Split(',');
            if (paramArr.Count() == 2)
            {
                _itemDetailProcessId = Int32.Parse(paramArr[0]);
                _type = Int32.Parse(paramArr[1]);
                _formulaAndConditionId = _procFormulaAndConditionBL.GetSingleByDetailProcessByType(_itemDetailProcessId, _type).FormulaAndConditionId;
            }
            FillPanelFormulaItem(-1);
            BuidOperatorBox();
        }

        private void FillPanelFormulaItem(int? focusedBox)
        {
            try
            {
                PlchFormulaMain.Controls.Clear();
                PlchFormulaForUser.Controls.Clear();
                var itemList = _procFormulaAndConditionDetailBL.GetProcFormulaAndConditionDetailCollectionByProcFormulaAndConditionAndType(_itemDetailProcessId, _type);
                foreach (DataRow rooItem in itemList.Rows)
                {
                    if (string.IsNullOrEmpty(rooItem["PreLink"].ToString()))
                    {
                        FillFormulaItemForSqlRecursive(rooItem, itemList);
                        FillFormulaItemRecursiveForUser(rooItem, itemList);
                        SetFocusToCurrentBox(focusedBox);
                        break;
                    }
                }
                FillFinalFormula();
            }
            catch (Exception exception)
            {
                CustomMessageErrorControl.ShowErrorMessage(exception.Message);
            }
        }

        private void FillFormulaItemForSqlRecursive(DataRow formulaItem, DataTable itemList)
        {
            var customTextBox = new CustomTextBox
            {
                ProcFormulaAndConditionDetailId = Int32.Parse(formulaItem["ProcFormulaAndConditionDetailId"].ToString()),
                PreLink = string.IsNullOrEmpty(formulaItem["PreLink"].ToString()) ? (int?) null : Int32.Parse(formulaItem["PreLink"].ToString()),
                PostLink = string.IsNullOrEmpty(formulaItem["PostLink"].ToString()) ? (int?) null : Int32.Parse(formulaItem["PostLink"].ToString()),
                TextBoxTypeId = Int32.Parse(formulaItem["TextBoxTypeId"].ToString()),
                CssClass = formulaItem["CSSName"].ToString(),
                ID = formulaItem["ProcFormulaAndConditionDetailId"].ToString(),
                UserAlterLink = string.IsNullOrEmpty(formulaItem["UserAlterLink"].ToString()) ? (int?) null : Int32.Parse(formulaItem["UserAlterLink"].ToString()),
                AlterUserValue = formulaItem["AlterUserValue"].ToString(),

            };
            if (formulaItem["TextBoxTypeId"].ToString() == "1") // آیتم
            {
                customTextBox.Text = "[" + formulaItem["ItemId"] + "]";
                customTextBox.SqlValue = formulaItem["ItemId"].ToString();
            }
            else if (formulaItem["TextBoxTypeId"].ToString() == "2") // عملگر
            {
                customTextBox.Text = formulaItem["SqlOperator"].ToString();
                customTextBox.SqlValue = formulaItem["OperatorId"].ToString();
            }
            else if (formulaItem["TextBoxTypeId"].ToString() == "3" || formulaItem["TextBoxTypeId"].ToString() == "4") // مقدار یا آیتم مقدار
            {
                customTextBox.Text = formulaItem["Value"].ToString();
                customTextBox.SqlValue = formulaItem["Value"].ToString();
            }
            customTextBox.Width = (customTextBox.Text.Length < 20) ? new Unit(customTextBox.Text.Length*10) : new Unit(20*10);
            AddRadToolTip(customTextBox);
            customTextBox.ReadOnly = true;
            customTextBox.BorderWidth = new Unit(1, UnitType.Pixel);
            customTextBox.Attributes.Add("OnFocus", "SetActiveBox(" + customTextBox.ProcFormulaAndConditionDetailId + "," + customTextBox.ClientID + ")");
            //customTextBox.Attributes.Add("OnBlur", "this.style.borderColor='';this.style.borderWidth=2");
            PlchFormulaMain.Controls.Add(customTextBox);

            if (formulaItem["PostLink"] != null)
            {
                foreach (DataRow otherFormulaItem in itemList.Rows)
                {
                    if (otherFormulaItem["PreLink"].ToString() == formulaItem["ProcFormulaAndConditionDetailId"].ToString())
                    {
                        FillFormulaItemForSqlRecursive(otherFormulaItem, itemList);
                        break;
                    }
                }
            }
        }

        private void FillFormulaItemRecursiveForUser(DataRow formulaItem, DataTable itemList)
        {
            var customTextBox = new CustomTextBox
            {
                ProcFormulaAndConditionDetailId = Int32.Parse(formulaItem["ProcFormulaAndConditionDetailId"].ToString()),
                CssClass = formulaItem["CSSName"].ToString(),
                ID = formulaItem["ProcFormulaAndConditionDetailId"] + "alter"
            };
            if (formulaItem["TextBoxTypeId"].ToString() == "1") // آیتم
            {
                customTextBox.Text = formulaItem["ItemTitle"].ToString();
                customTextBox.SqlValue = formulaItem["ItemId"].ToString();
            }
            else if (formulaItem["TextBoxTypeId"].ToString() == "2") // عملگر
            {
                customTextBox.Text = formulaItem["OperatorTitle"].ToString();
                customTextBox.SqlValue = formulaItem["OperatorId"].ToString();
            }
            else if (formulaItem["TextBoxTypeId"].ToString() == "3" || formulaItem["TextBoxTypeId"].ToString() == "4") // مقدار یا آیتم مقدار
            {
                customTextBox.Text = formulaItem["AlterUserValue"].ToString();
                customTextBox.SqlValue = formulaItem["AlterUserValue"].ToString();
            }
            customTextBox.Width = (customTextBox.Text.Length < 20) ? new Unit(customTextBox.Text.Length*10) : new Unit(20*10);
            AddRadToolTip(customTextBox);
            customTextBox.ReadOnly = true;
            customTextBox.BorderWidth = new Unit(1, UnitType.Pixel);
            customTextBox.Attributes.Add("OnFocus", "SetActiveAlterBox(" + customTextBox.ProcFormulaAndConditionDetailId + ",'" + customTextBox.ClientID + "')");
            //customTextBox.Attributes.Add("OnBlur", "this.style.borderColor='';this.style.borderWidth=2");
            PlchFormulaForUser.Controls.Add(customTextBox);
            if (formulaItem["PostLink"] != null)
            {
                foreach (DataRow otherFormulaItem in itemList.Rows)
                {
                    if (otherFormulaItem["PreLink"].ToString() == formulaItem["ProcFormulaAndConditionDetailId"].ToString())
                    {
                        FillFormulaItemRecursiveForUser(otherFormulaItem, itemList);
                        break;
                    }
                }
            }
        }

        private void FillFinalFormula()
        {
            TxtFinalFormula.Text = "";
            foreach (Control control in PlchFormulaMain.Controls)
            {
                if (control is CustomTextBox)
                {
                    var boxControl = (CustomTextBox) control;
                    TxtFinalFormula.Text += " " + boxControl.Text;
                }
            }
        }

        private string FillFinalFormulaForUser()
        {
            var finalFormulaForUser= "";
            foreach (Control control in PlchFormulaForUser.Controls)
            {
                if (control is CustomTextBox)
                {
                    var boxControl = (CustomTextBox)control;
                    finalFormulaForUser += " " + boxControl.Text;
                }
            }
            return finalFormulaForUser;
        }

        protected void BtnItemValueSelect_Click(object sender, EventArgs e)
        {

        }

        protected void CmbItemGroup_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {            
            SetCmbItem();
        }     

        private void SetCmbItemGroup()
        {
            CmbItemGroup.Items.Clear();
            CmbItemGroup.Items.Add(new RadComboBoxItem("گروه آیتم را انتخاب نمایید"));
            CmbItemGroup.DataTextField = "ItemGroupTitle";
            CmbItemGroup.DataValueField = "ItemGroupId";
            CmbItemGroup.DataSource = _procItemBL.ProcItemGroupGetAll();
            CmbItemGroup.DataBind();
        }


        private void SetCmbItem()
        {
            int itemGroupId = (CmbItemGroup.SelectedIndex > 0) ? Int32.Parse(CmbItemGroup.SelectedValue) : -1;
            CmbItem.Items.Clear();
            CmbItem.Items.Add(new RadComboBoxItem("آیتم را انتخاب نمایید"));
            CmbItem.DataTextField = "ItemTitle";
            CmbItem.DataValueField = "ItemId";
            CmbItem.DataSource = _procItemBL.GetAllByItemGroup(itemGroupId);
            CmbItem.DataBind();
        }      

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (TxtValue.Text.Trim() != string.Empty)
                {
                    var activeTextBox = FindActiveTextBox();
                    if (activeTextBox == null && PlchFormulaMain.Controls.Count > 0) throw new Exception("یک گزینه از فرمول را انتخاب نمایید");
                    int? currentBoxId = (activeTextBox == null) ? (int?) null : activeTextBox.ProcFormulaAndConditionDetailId;

                    int procFormulaAndConditionDetailId;
                    var procFormulaAndConditionDetailEntity = new ProcFormulaAndConditionDetailEntity
                    {
                        PreLink = (activeTextBox == null) ? null : (int?) activeTextBox.ProcFormulaAndConditionDetailId,
                        PostLink = (activeTextBox == null) ? null : activeTextBox.PostLink,
                        FormulaAndConditionId = _formulaAndConditionId,
                        UserAlterLink = null
                    };

                    procFormulaAndConditionDetailEntity.TextBoxTypeId = 4;
                    procFormulaAndConditionDetailEntity.Value = TxtValue.Text.Trim();
                    procFormulaAndConditionDetailEntity.ValueUserAlter = TxtValue.Text.Trim();
                    procFormulaAndConditionDetailEntity.ItemId = null;
                    procFormulaAndConditionDetailEntity.OperatorId = null;

                    var newBoxEntity = procFormulaAndConditionDetailEntity;
                    if (activeTextBox != null && activeTextBox.TextBoxTypeId == 3 && newBoxEntity.TextBoxTypeId == 3) // در صورتیکه آیتم جدید  و آیتم جاری هر دو آیتم مقدار باشند  باکس قبلی پاک شده  و باکس جدید جایگزین آن میشود
                    {
                        currentBoxId = activeTextBox.PreLink;
                        _procFormulaAndConditionDetailBL.DeleteAndUpdateLinkList(activeTextBox.ProcFormulaAndConditionDetailId);
                        newBoxEntity.PreLink = activeTextBox.PreLink;
                        newBoxEntity.PostLink = activeTextBox.PostLink;
                    }
                    _procFormulaAndConditionDetailBL.AddAndUpdateLinkList(currentBoxId, newBoxEntity, out procFormulaAndConditionDetailId);
                    if (procFormulaAndConditionDetailId > 0)
                    {
                        FillPanelFormulaItem(procFormulaAndConditionDetailId);
                        _procFormulaAndConditionDetailBL.UpdateFinalExpression(_formulaAndConditionId, TxtFinalFormula.Text,FillFinalFormulaForUser());
                        SetClearToolBox();
                        CustomMessageErrorControl.ShowSuccesMessage("ثبت موفق");
                    }
                }
            }
            catch (Exception exception)
            {
                CustomMessageErrorControl.ShowErrorMessage(exception.Message);
            }
        }

        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                var activeTextBox = FindActiveTextBox();
                if (activeTextBox == null)
                {
                    throw new Exception(" یک آیتم از فرمول را جهت حذف انتخاب نمایید");
                }
                _procFormulaAndConditionDetailBL.DeleteAndUpdateLinkList(activeTextBox.ProcFormulaAndConditionDetailId);
                var focusBoxId = (activeTextBox.PreLink != null) ? activeTextBox.PreLink : activeTextBox.PostLink;
                FillPanelFormulaItem(focusBoxId);
                _procFormulaAndConditionDetailBL.UpdateFinalExpression(_formulaAndConditionId, TxtFinalFormula.Text, FillFinalFormulaForUser());
                SetClearToolBox();
                CustomMessageErrorControl.ShowSuccesMessage("حذف موفق");
            }
            catch (Exception exception)
            {
                CustomMessageErrorControl.ShowErrorMessage(exception.Message);
            }
        }

        private ProcFormulaAndConditionDetailEntity SetNewBoxData(CustomTextBox activeTextBox)
        {
            var procFormulaAndConditionDetailEntity = new ProcFormulaAndConditionDetailEntity
            {
                PreLink = (activeTextBox == null) ? null : (int?) activeTextBox.ProcFormulaAndConditionDetailId,
                PostLink = (activeTextBox == null) ? null : activeTextBox.PostLink,
                FormulaAndConditionId = _formulaAndConditionId,
                UserAlterLink = null
            };

            if (TxtValue.Text.Trim() != string.Empty)
            {
                procFormulaAndConditionDetailEntity.TextBoxTypeId = 4;
                procFormulaAndConditionDetailEntity.Value = TxtValue.Text.Trim();
                procFormulaAndConditionDetailEntity.ValueUserAlter = TxtValue.Text.Trim();
                procFormulaAndConditionDetailEntity.ItemId = null;
                procFormulaAndConditionDetailEntity.OperatorId = null;
                return procFormulaAndConditionDetailEntity; // مقدار
            }
            if (HdItemValueModal.Value.Trim() != string.Empty)
            {
                procFormulaAndConditionDetailEntity.TextBoxTypeId = 3;
                procFormulaAndConditionDetailEntity.Value = HdItemValueModal.Value.Trim();
                procFormulaAndConditionDetailEntity.ValueUserAlter = txtItemValue.Text;
                procFormulaAndConditionDetailEntity.ItemId = null;
                procFormulaAndConditionDetailEntity.OperatorId = null;
                return procFormulaAndConditionDetailEntity; // آیتم مقدار
            }

            if (CmbItem.SelectedIndex > 0)
            {
                procFormulaAndConditionDetailEntity.TextBoxTypeId = 1;
                procFormulaAndConditionDetailEntity.Value = null;
                procFormulaAndConditionDetailEntity.ItemId = Int32.Parse(CmbItem.SelectedValue);
                procFormulaAndConditionDetailEntity.OperatorId = null;
                return procFormulaAndConditionDetailEntity; // آیتم 
            }
            //if (CmbOperator.SelectedIndex > 0)
            //{
            //    procFormulaAndConditionDetailEntity.TextBoxTypeId = 2;
            //    procFormulaAndConditionDetailEntity.Value = null;
            //    procFormulaAndConditionDetailEntity.ItemId = null;
            //    procFormulaAndConditionDetailEntity.OperatorId = Int32.Parse(CmbOperator.SelectedValue);
            //    return procFormulaAndConditionDetailEntity; //عملگر  
            //}
            return null;
        }

        private CustomTextBox FindActiveTextBox()
        {
            var activeBoxId = HiddenField1.Value;
            if (string.IsNullOrEmpty(activeBoxId))
                return null;

            foreach (Control control in PlchFormulaMain.Controls)
            {
                if (control is CustomTextBox)
                {
                    var boxControl = (CustomTextBox) control;
                    if (boxControl.ProcFormulaAndConditionDetailId.ToString() == activeBoxId)
                    {
                        return boxControl;
                    }
                }
            }
            return null;
        }

        private void SetFocusToCurrentBox(int? focusBoxId)
        {
            foreach (Control control in PlchFormulaMain.Controls)
            {
                if (control is CustomTextBox)
                {
                    var boxControl = (CustomTextBox) control;
                    if (boxControl.ProcFormulaAndConditionDetailId == focusBoxId)
                    {
                        boxControl.Focus(); // not work
                        HiddenField1.Value = focusBoxId.ToString();
                        boxControl.BorderColor = Color.Red;
                        boxControl.BorderWidth = new Unit(3, UnitType.Pixel);
                    }
                }
            }

            foreach (Control control in PlchFormulaForUser.Controls)
            {
                if (control is CustomTextBox)
                {
                    var boxControl = (CustomTextBox) control;
                    if (boxControl.ProcFormulaAndConditionDetailId == focusBoxId)
                    {
                        boxControl.Focus(); // not work
                        HiddenField1.Value = focusBoxId.ToString();
                        boxControl.BorderColor = Color.Red;
                        boxControl.BorderWidth = new Unit(3, UnitType.Pixel);
                    }
                }
            }
        }

      private int? FindLastItemBeforeSelectedItem(int? activeBoxId)
        {
            int? finalReturn = null;
            CustomTextBox boxControl = null;
            foreach (Control control in PlchFormulaMain.Controls)
            {
                if (control is CustomTextBox)
                {
                    boxControl = (CustomTextBox) control;
                    if (boxControl.PostLink == activeBoxId)
                    {
                        if (boxControl.TextBoxTypeId == 1) // آیتم باشد
                            return Int32.Parse(boxControl.SqlValue);
                        activeBoxId = boxControl.ProcFormulaAndConditionDetailId;
                        if (boxControl.PreLink != null)
                        {
                            finalReturn = FindLastItemBeforeSelectedItem(activeBoxId);
                            break;
                        }
                    }

                }
            }

            return finalReturn;
        }

        private void AddRadToolTip(CustomTextBox control)
        {
            control.ToolTip = control.Text;
            //RadToolTip tip = new RadToolTip();
            //// tip.ID = control.ProcFormulaAndConditionDetailId + "toolTip";
            //this.form1.Controls.Add(tip);
            //tip.Text = control.Text;
            //tip.IsClientID = true;
            //tip.TargetControlID = control.ID;
            //tip.RelativeTo = ToolTipRelativeDisplay.BrowserWindow;
            ////tip.Width = Unit.Pixel(200);
            ////tip.Height = Unit.Pixel(200);
            //tip.Skin = "Forest";
            //tip.Animation = ToolTipAnimation.FlyIn;
            //tip.Position = ToolTipPosition.Center;
            //tip.ShowDelay = 700;
            //tip.ShowEvent = ToolTipShowEvent.OnMouseOver;
        }

        private void BuidOperatorBox()
        {
            plcOperator.Controls.Clear();
            var tblGeneral = new Table
            {
                //Width = new Unit(90, UnitType.Percentage),
                BorderWidth = new Unit(3),
                CellPadding = 7,
                CellSpacing = 5,
                HorizontalAlign = HorizontalAlign.Center,
                
            };
            plcOperator.Controls.Add(tblGeneral);
            int count;
            string moduleToolTip;
            var operatorData = _procOperatorBL.GetAllIsActive();

            tblGeneral.BackColor = System.Drawing.Color.Pink;

            //tblGeneral.BackImageUrl = "Images/" + backGroundImagePath;

            var isCellMouseOverActive = true;
            var mouseOverColor = "Gold";
            var mouseOutColor = "";
            var rowNo = 4;
            var colNo = 5;

            for (var i = 0; i < rowNo; i++)
            {
                var tableRow = new TableRow();
                tableRow.HorizontalAlign = HorizontalAlign.Center;
                for (var j = 0; j < colNo; j++)
                {
                    if (operatorData.Count > 0)
                    {
                        var operatorButton = new CustomButton
                        {
                            Text = operatorData[0].SqlOperator,
                            ToolTip = operatorData[0].OperatorTitle + "\n" + operatorData[0].SampleTextForToolTip,
                            ID = "Btn_" + operatorData[0].OperatorId,
                            
                        };
                        operatorButton.Click += OperatorClick;

                        var tableCell = new TableCell {};
                        if (isCellMouseOverActive)
                        {
                            tableCell.Attributes.Add("DIR", "LTR");
                            tableCell.Attributes.Add("onmouseover", "this.style.backgroundColor = '" + mouseOverColor + "';");
                            tableCell.Attributes.Add("onmouseout", "this.style.backgroundColor = '" + mouseOutColor + "';");
                        }

                        tableCell.VerticalAlign = VerticalAlign.Middle;
                        tableCell.Controls.Add(operatorButton);
                        tableCell.BorderWidth = 0;
                        tableCell.BorderColor = Color.DarkBlue;
                        operatorData.RemoveAt(0);
                        tableRow.Controls.Add(tableCell);
                    }
                }
                tblGeneral.Controls.Add(tableRow);
            }
        }

        private void OperatorClick(object sender, EventArgs e)
        {
            try
            {
                CustomButton operatorBtn = (CustomButton) sender;
                int procFormulaAndConditionDetailId;

                var activeTextBox = FindActiveTextBox();
                if (activeTextBox == null && PlchFormulaMain.Controls.Count > 0) throw new Exception("یک گزینه از فرمول را انتخاب نمایید");
                int? currentBoxId = (activeTextBox == null) ? (int?) null : activeTextBox.ProcFormulaAndConditionDetailId;

                var procFormulaAndConditionDetailEntity = new ProcFormulaAndConditionDetailEntity
                {
                    PreLink = (activeTextBox == null) ? null : (int?) activeTextBox.ProcFormulaAndConditionDetailId,
                    PostLink = (activeTextBox == null) ? null : activeTextBox.PostLink,
                    FormulaAndConditionId = _formulaAndConditionId,
                    UserAlterLink = null
                };
                procFormulaAndConditionDetailEntity.TextBoxTypeId = 2;
                procFormulaAndConditionDetailEntity.Value = null;
                procFormulaAndConditionDetailEntity.ItemId = null;
                procFormulaAndConditionDetailEntity.OperatorId = Int32.Parse(operatorBtn.ID.Replace("Btn_", ""));

                var newBoxEntity = procFormulaAndConditionDetailEntity;

                _procFormulaAndConditionDetailBL.AddAndUpdateLinkList(currentBoxId, newBoxEntity, out procFormulaAndConditionDetailId);
                if (procFormulaAndConditionDetailId > 0)
                {
                    FillPanelFormulaItem(procFormulaAndConditionDetailId);
                    _procFormulaAndConditionDetailBL.UpdateFinalExpression(_formulaAndConditionId, TxtFinalFormula.Text, FillFinalFormulaForUser());
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage("ثبت موفق");
                }
            }
            catch (Exception exception)
            {
                CustomMessageErrorControl.ShowErrorMessage(exception.Message);
            }
        }

        protected void CmbItem_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                if (CmbItem.SelectedIndex > 0)
                {
                    int procFormulaAndConditionDetailId;
                    var activeTextBox = FindActiveTextBox();
                    if (activeTextBox == null && PlchFormulaMain.Controls.Count > 0) throw new Exception("یک گزینه از فرمول را انتخاب نمایید");
                    int? currentBoxId = (activeTextBox == null) ? (int?)null : activeTextBox.ProcFormulaAndConditionDetailId;

                    var procFormulaAndConditionDetailEntity = new ProcFormulaAndConditionDetailEntity
                    {
                        PreLink = (activeTextBox == null) ? null : (int?) activeTextBox.ProcFormulaAndConditionDetailId,
                        PostLink = (activeTextBox == null) ? null : activeTextBox.PostLink,
                        FormulaAndConditionId = _formulaAndConditionId,
                        UserAlterLink = null
                    };
                    procFormulaAndConditionDetailEntity.TextBoxTypeId = 1;
                    procFormulaAndConditionDetailEntity.Value = null;
                    procFormulaAndConditionDetailEntity.ItemId = Int32.Parse(CmbItem.SelectedValue);
                    procFormulaAndConditionDetailEntity.OperatorId = null;
                    var newBoxEntity = procFormulaAndConditionDetailEntity;

                    _procFormulaAndConditionDetailBL.AddAndUpdateLinkList(currentBoxId, newBoxEntity, out procFormulaAndConditionDetailId);
                    if (procFormulaAndConditionDetailId > 0)
                    {
                        FillPanelFormulaItem(procFormulaAndConditionDetailId);
                        _procFormulaAndConditionDetailBL.UpdateFinalExpression(_formulaAndConditionId, TxtFinalFormula.Text, FillFinalFormulaForUser());
                        SetClearToolBox();
                        CustomMessageErrorControl.ShowSuccesMessage("ثبت موفق");
                    }
                }
            }

            catch (Exception exception)
            {
                CustomMessageErrorControl.ShowErrorMessage(exception.Message);
            }
        }

        protected void BtnItemValueModal_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                var activeBox = FindActiveTextBox();
                if (activeBox == null)
                {
                    throw  new Exception("یک گزینه از فرمول را انتخاب نمایید");
                }
                if (activeBox.TextBoxTypeId == 3) // آیتم مقدار است پس بعد از باز شدن فرم مقادیری که قبلا انتخاب شده است تیک میخورد
                {
                    Session["SellectedItemValue"] = activeBox.SqlValue;
                    Session["SellectedItemAlterUserValue"] = activeBox.AlterUserValue;
                }
                else
                {
                    Session["SellectedItemValue"] = null;
                    Session["SellectedItemAlterUserValue"] = null;
                }

                int? activeBoxId = (HiddenField1.Value == string.Empty) ? -1 : Int32.Parse(HiddenField1.Value);
                var itemId = FindLastItemBeforeSelectedItem(activeBoxId);
                if (itemId == null)
                    throw new Exception("در فرمول تا قبل از گزینه انتخابی آیتمی وجود ندارد");
                var myItem = _procItemBL.GetSingleById(new ProcItemEntity() {ItemId = Int32.Parse(itemId.ToString())});
                if (myItem.ObjectId==null)
                {
                    throw new Exception("آبجکت آيتم انتخابي تعيين نگرديده است");
                }
                var isTree = _procItemBL.CheckItemObjectIsTree(new ProcItemEntity() { ItemId = (int)itemId });
                var navigateUrl = (isTree) ? "FrmItemValueTree.aspx?ItemId=" + itemId : "FrmItemValueGridMultiSelect.aspx?ItemId=" + itemId;
                var newWindow = new RadWindow
                {
                    NavigateUrl = navigateUrl,
                    Top = Unit.Pixel(22),
                    VisibleOnPageLoad = true,
                    Left = Unit.Pixel(0),
                    DestroyOnClose = true,
                    Modal = true,
                    ReloadOnShow = true,
                };
                RadWindowItemValue.Windows.Add(newWindow);
            }
            catch (Exception exception)
            {
                CustomMessageErrorControl.ShowErrorMessage(exception.Message);
            }
        }

        protected void BtnSaveFromModal_Click(object sender, EventArgs e)
        {
            try
            {
                if (HdItemValueModal.Value.Trim() != string.Empty)
                {
                    var activeTextBox = FindActiveTextBox();
                    if (activeTextBox == null && PlchFormulaMain.Controls.Count > 0) throw new Exception("یک گزینه از فرمول را انتخاب نمایید");
                    int? currentBoxId = (activeTextBox == null) ? (int?)null : activeTextBox.ProcFormulaAndConditionDetailId;

                    int procFormulaAndConditionDetailId;
                    var procFormulaAndConditionDetailEntity = new ProcFormulaAndConditionDetailEntity
                    {
                        PreLink = (activeTextBox == null) ? null : (int?)activeTextBox.ProcFormulaAndConditionDetailId,
                        PostLink = (activeTextBox == null) ? null : activeTextBox.PostLink,
                        FormulaAndConditionId = _formulaAndConditionId,
                        UserAlterLink = null
                    };

                    procFormulaAndConditionDetailEntity.TextBoxTypeId = 3;
                    procFormulaAndConditionDetailEntity.Value = HdItemValueModal.Value.Trim();
                    procFormulaAndConditionDetailEntity.ValueUserAlter = txtItemValue.Text;
                    procFormulaAndConditionDetailEntity.ItemId = null;
                    procFormulaAndConditionDetailEntity.OperatorId = null;

                    var newBoxEntity = procFormulaAndConditionDetailEntity;
                    if (activeTextBox != null && activeTextBox.TextBoxTypeId == 3 && newBoxEntity.TextBoxTypeId == 3) // در صورتیکه آیتم جدید  و آیتم جاری هر دو آیتم مقدار باشند  باکس قبلی پاک شده  و باکس جدید جایگزین آن میشود
                    {
                        currentBoxId = activeTextBox.PreLink;
                        _procFormulaAndConditionDetailBL.DeleteAndUpdateLinkList(activeTextBox.ProcFormulaAndConditionDetailId);
                        newBoxEntity.PreLink = activeTextBox.PreLink;
                        newBoxEntity.PostLink = activeTextBox.PostLink;
                    }
                    _procFormulaAndConditionDetailBL.AddAndUpdateLinkList(currentBoxId, newBoxEntity, out procFormulaAndConditionDetailId);
                    if (procFormulaAndConditionDetailId > 0)
                    {
                        FillPanelFormulaItem(procFormulaAndConditionDetailId);
                        _procFormulaAndConditionDetailBL.UpdateFinalExpression(_formulaAndConditionId, TxtFinalFormula.Text, FillFinalFormulaForUser());
                        SetClearToolBox();
                        CustomMessageErrorControl.ShowSuccesMessage("ثبت موفق");
                    }
                }
            }
            catch (Exception exception)
            {
                CustomMessageErrorControl.ShowErrorMessage(exception.Message);
            }
        }        

        protected void BtnTestSqlExpression_Click(object sender, EventArgs e)
        {
           var res =  _sqlObjectBL.SqlQueryValidation(TxtFinalFormula.Text.Replace("[", "(").Replace("]",")"));
            if (res==string.Empty)
            {
                LblTestSqlExpression.Text = "عبارت صحیح میباشد";
                LblTestSqlExpression.ForeColor = Color.Green;
            }
            else
            {
                LblTestSqlExpression.Text = res.Replace("Incorrect syntax near", "خطا در نزدیکی");
                LblTestSqlExpression.ForeColor = Color.Red;
            }
           
        }
         
    }
}