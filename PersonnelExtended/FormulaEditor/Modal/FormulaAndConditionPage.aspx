﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FormulaAndConditionPage.aspx.cs"
    Inherits="Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.FormulaEditor.Modal.FormulaAndConditionPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="ITC.Library" Namespace="ITC.Library.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<html xmlns="http://www.w3.org/1999/xhtml">

<link href="../../Style/TextBoxTypeCSS.css" rel="stylesheet" type="text/css" />
<link href="../../Style/VirtualKeyBoard.css" rel="stylesheet" type="text/css" />
<script src="../../Script/VirtualKeyboard.js" type="text/javascript"></script>
<head runat="server">
    <style type="text/css">
        .TextLtr {
            direction: ltr;
            text-align: left;
        }
    </style>
    <title></title>
    <telerik:RadScriptBlock runat="server">
        <script type="text/javascript">
            function SetActiveBox(objId, objClientId) {
                document.getElementById("<%=HiddenField1.ClientID%>").value = objId;
                var boxes = document.getElementsByTagName("INPUT");
                var alterId = objId + 'alter';
                for (var i = 0; i < boxes.length; i++) {
                    {
                        if (boxes[i].className == "Operator" || boxes[i].className == "Item" || boxes[i].className == "ItemValue" || boxes[i].className == "Value") {
                            if (boxes[i].id == objClientId || boxes[i].id == alterId) {
                                boxes[i].style.borderColor = 'red';
                                boxes[i].style.borderWidth = "3px";
                            } else {
                                boxes[i].style.borderColor = '';
                                boxes[i].style.borderWidth = "1px";
                            }
                        }
                    }
                }
            }

            function SetActiveAlterBox(objId, objClientId) {
                document.getElementById("<%=HiddenField1.ClientID%>").value = objId;
                var boxes = document.getElementsByTagName("INPUT");

                for (var i = 0; i < boxes.length; i++) {
                    {
                        if (boxes[i].className == "Operator" || boxes[i].className == "Item" || boxes[i].className == "ItemValue" || boxes[i].className == "Value") {
                            if (boxes[i].id == objClientId || boxes[i].id == objId) {
                                boxes[i].style.borderColor = 'red';
                                boxes[i].style.borderStyle = "solid";
                                boxes[i].style.borderWidth = "3px";
                                document.getElementById(objId).focus();
                            } else {
                                boxes[i].style.borderColor = '';
                                boxes[i].style.borderWidth = "1px";
                            }
                        }
                    }
                }
            }

            function OnClientClicked(button, args) {
                if (window.confirm("آیا آیتم انتخابی حذف گردد?")) {
                    button.set_autoPostBack(true);
                } else {
                    button.set_autoPostBack(false);
                }
            }

            function OnClientclose(sender, eventArgs) {
                var objId = document.getElementById("<%=HiddenField1.ClientID%>").value;
                var txtItemValues = document.getElementById("<%=txtItemValue.ClientID%>");
                var hdItemValueModal = document.getElementById("<%=HdItemValueModal.ClientID%>");
                var arg = eventArgs.get_argument();
                if (arg) {
                    txtItemValues.title = arg.KeyId;
                    txtItemValues.value = arg.Title;
                    hdItemValueModal.value = arg.KeyId;
                    SetActiveBox(objId, objId);
                    var btnModalValue = document.getElementById('<%=BtnSaveFromModal.ClientID %>');
                    btnModalValue.click();
                }
                else {

                }
            }

        </script>
    </telerik:RadScriptBlock>
</head>
<body style="font-weight: 700">
    <form id="form1" runat="server">
        <asp:Panel runat="server" ID="pnlMain" BorderColor="#005B88" BorderWidth="3">
            <table width="100%">
                <tr>
                    <td style="background-image: url('../../Images/BackGround.jpg'); background-repeat: repeat-x">
                        <table width="100%">
                            <tr>
                                <td style="text-align: center">
                                    <asp:Label runat="server" ID="LblPageTitle" Style="font-family: 'Tahoma'; font-size: medium"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center">
                                    <asp:Label ID="LblItemTitle" runat="server" Style="font-family: 'Tahoma'; font-size: medium"></asp:Label>
                                    &nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center">
                                    <asp:Label ID="LblConditionOrFormulaTitleForUser" CssClass="TextLtr" runat="server" Style="font-family: 'Tahoma';"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table width="100%" dir="rtl" cellpadding="0" cellspacing="0" align="center">
                <tr bgcolor="#95C5FF">
                    <td width="30%">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="left">گروه آیتم:</td>
                                <td>
                                    <cc1:CustomRadComboBox ID="CmbItemGroup" runat="server" AppendDataBoundItems="True" Filter="Contains" KeyId="" MarkFirstMatch="True" Skin="Sunset" Width="200px" AutoPostBack="True" OnSelectedIndexChanged="CmbItemGroup_SelectedIndexChanged"></cc1:CustomRadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="left">آیتم:
                                </td>
                                <td>
                                    <cc1:CustomRadComboBox ID="CmbItem" runat="server" AppendDataBoundItems="True" Filter="Contains" KeyId="" MarkFirstMatch="True" Skin="Sunset" Width="200px" AutoPostBack="True" OnSelectedIndexChanged="CmbItem_SelectedIndexChanged"></cc1:CustomRadComboBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td align="center" width="40%">
                        <table width="100%"  cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center">
                                    <asp:ImageButton ID="BtnItemValueModal" runat="server" ImageUrl="~/DeskTopModules/PersonnelExtendedProject/PersonnelExtended/Images/Modal.png" OnClick="BtnItemValueModal_Click" ToolTip="انتخاب مقدار از لیست" Height="91px" Width="94px" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center"><span>
                                    <br />
                                    مقدار:</span>
                                    <asp:TextBox ID="TxtValue" runat="server" class="keyboardInput" Width="100px"></asp:TextBox>
                                    &nbsp;<asp:Button ID="BtnAdd" runat="server" Font-Names="Tahoma" 
                                        OnClick="BtnAdd_Click" Text="افزودن" Width="70px">
                                    </asp:Button>
                                    <br />
                                    &nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    <td width="30%" >
                        <table width="100%" cellpadding="0" cellspacing="0" >
                            <tr>
                                <td align="left">&nbsp;<asp:PlaceHolder ID="plcOperator" runat="server"></asp:PlaceHolder>
                                </td>
                                <td align="right">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr bgcolor="#95C5FF">
                    <td align="center" colspan="3">&nbsp;&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" colspan="3" height="40" style="vertical-align: middle">&nbsp;
                    <asp:Button ID="BtnDelete" runat="server" Font-Names="Tahoma"
                        OnClick="BtnDelete_Click" OnClientClicked="OnClientClicked" Text="حذف" 
                            Width="70px" UseSubmitBehavior="False">
                    </asp:Button>
                        <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table width="100%">
                            <tr>
                                <td>
                                    <asp:Panel runat="server" ID="PnlFormulaForUser" BackColor="#E0EBEB" Direction="LeftToRight">
                                        <asp:PlaceHolder runat="server" ID="PlchFormulaForUser"></asp:PlaceHolder>
                                    </asp:Panel>

                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td dir="ltr">
                                    <asp:Panel runat="server" ID="PnlFormulaMain" BackColor="#E0EBEB" Direction="LeftToRight">
                                        <asp:PlaceHolder runat="server" ID="PlchFormulaMain"></asp:PlaceHolder>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td dir="ltr">
                                    <telerik:RadTextBox ID="TxtFinalFormula" runat="server" BackColor="#E0EBEB" Height="60px" ReadOnly="True" TextMode="MultiLine" Width="100%">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="center">
                        <asp:Button ID="BtnTestSqlExpression" runat="server"
                            OnClick="BtnTestSqlExpression_Click" Text="تست عبارت" Font-Names="Tahoma">
                        </asp:Button>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="3">
                        <asp:Label ID="LblTestSqlExpression" runat="server"
                            Style="font-family: 'Tahoma'; font-size: medium"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="center">
                        <asp:TextBox runat="server" ID="txtItemValue" BackColor="#FFFFEC" Height="40px" TextMode="MultiLine" Width="98%" Font-Names="Tahoma" Font-Bold="True" Font-Size="8pt"></asp:TextBox>
                    </td>
                </tr>
            </table>

            <style type="text/css">
                .windowcss {
                    direction: ltr;
                }
            </style>
            <telerik:RadWindowManager ID="RadWindowItemValue" runat="server" DestroyOnClose="True" CssClass="windowcss" OnClientClose="OnClientclose"
                EnableViewState="False" InitialBehaviors="Maximize" Modal="True"
                ReloadOnShow="True" Skin="Office2007" VisibleTitlebar="False">
                <Localization Cancel="انصراف" Close="بستن" Maximize="بیشینه" Minimize="کمینه" No="خیر"
                    OK="باشه" Reload="بازنشانی" Yes="بلی" />
            </telerik:RadWindowManager>
            <asp:HiddenField ID="HiddenField1" runat="server" />
            <asp:Button ID="BtnSaveFromModal" runat="server" OnClick="BtnSaveFromModal_Click" Width="1px" />
            <asp:HiddenField ID="HdItemValueModal" runat="server" />
        </asp:Panel>
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
            <Scripts>
                <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js"></asp:ScriptReference>
                <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js"></asp:ScriptReference>
                <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js"></asp:ScriptReference>
            </Scripts>
        </telerik:RadScriptManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default">
        </telerik:RadAjaxLoadingPanel>
    </form>
</body>
</html>
