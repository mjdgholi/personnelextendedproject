﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ITC.Library.Controls;
using PersonnelExtended.BL;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.FormulaEditor.Modal
{
    public partial class FrmItemValueGridMultiSelect : System.Web.UI.Page
    {
        private ProcItemBL _procItemBL = new ProcItemBL();
        private int itemId;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                itemId = Int32.Parse(Request.QueryString["ItemId"]);
                SetGrvItemData();
                hiddenfieldKeyId.Value = (Session["SellectedItemValue"] == null) ? "" : Session["SellectedItemValue"].ToString();
                hiddenfieldTitle.Value = (Session["SellectedItemAlterUserValue"] == null) ? "" : Session["SellectedItemAlterUserValue"].ToString();
                SetGridLocalization();
            }
            catch (Exception exception)
            {
                CustomMessageErrorControl1.ShowErrorMessage(exception.Message);
            }

        }

        protected void SetGridLocalization()
        {
            GridFilterMenu menu = grdItem.FilterMenu;

            foreach (RadMenuItem item in menu.Items)
            {
                switch (item.Text.ToLower())
                {
                    case "startswith":
                    {
                        item.Text = "شروع شود با";
                        break;
                    }
                    case "nofilter":
                    {
                        item.Text = "بدون فیلترا";
                        break;
                    }
                    case "contains":
                    {
                        item.Text = "شامل";
                        break;
                    }
                    case "doesnotcontain":
                    {
                        item.Text = "عدم شمول";
                        break;
                    }
                    case "endswith":
                    {
                        item.Text = "خاتمه یابد با";
                        break;
                    }
                    case "equalto":
                    {
                        item.Text = "برابر";
                        break;
                    }
                    case "notequalto":
                    {
                        item.Text = "برابر نباشد با";
                        break;
                    }
                    case "greaterthan":
                    {
                        item.Text = "بزرگتر از";
                        break;
                    }
                    case "lessthan":
                    {
                        item.Text = "کوچکتر از";
                        break;
                    }
                    case "greaterthanorequalto":
                    {
                        item.Text = "بزرگتر مساوی";
                        break;
                    }
                    case "lessthanorequalto":
                    {
                        item.Text = "کوچکتر مساوی";
                        break;
                    }
                    case "between":
                    {
                        item.Text = "وسط";
                        break;
                    }
                    case "notbetween":
                    {
                        item.Text = "وسط نباشد";
                        break;
                    }
                    case "isempty":
                    {
                        item.Text = "تهی";
                        break;
                    }
                    case "notisempty":
                    {
                        item.Text = "تهی نباشد";
                        break;
                    }
                    case "isnull":
                    {
                        item.Visible = false;
                        break;
                    }
                    case "notisnull":
                    {
                        item.Visible = false;
                        break;
                    }
                }
            }
        }

        private void SetGrvItemData()
        {

            grdItem.DataSource = GetDataTable();

        }

        private DataTable GetDataTable()
        {
           
            return _procItemBL.ProcItemGetAllData(itemId);
        }

        protected void grdItem_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            try
            {
                grdItem.DataSource = _procItemBL.ProcItemGetAllData(itemId);
            }
            catch (Exception exception)
            {

                CustomMessageErrorControl1.ShowErrorMessage(exception.Message);
            }

        }

        protected void grdItem_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            SetGrvItemData();
        }

        protected void grdItem_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            SetGrvItemData();
        }

        protected void grdItem_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            SetGrvItemData();
        }

        protected void btnSelectAll_Click(object sender, EventArgs e)
        {
            string PersonId = "";
            string FirstAndLastName = "";
            DataTable dt = GetDataTable();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                PersonId = PersonId + dt.Rows[i]["ObjectId"] + ",";
                FirstAndLastName = FirstAndLastName + dt.Rows[i]["ObjectTitle"] + ",";
            }
            if (PersonId != "")
            {
                PersonId = PersonId.Substring(0, PersonId.Length - 1);
                FirstAndLastName = FirstAndLastName.Substring(0, FirstAndLastName.Length - 1);
            }

            hiddenfieldKeyId.Value = PersonId;
            hiddenfieldTitle.Value = FirstAndLastName;
        }

        protected void grdItem_ColumnCreated(object sender, Telerik.Web.UI.GridColumnCreatedEventArgs e)
        {
            GridColumn gridColumn = e.Column;

            if ((gridColumn.ColumnType == "GridBoundColumn" || gridColumn.ColumnType == "GridNumericColumn") && !IsPersinaText(e.Column.UniqueName))
            {
                e.Column.Visible = false;
            }
        }

        private bool IsPersinaText(string text)
        {
            text = text.ToLower();
            foreach (char chr in text)
            {
                if (chr <= 'z' && chr >= 'a')
                {
                    return false;
                }
            }
            return true;
        }
    }

}