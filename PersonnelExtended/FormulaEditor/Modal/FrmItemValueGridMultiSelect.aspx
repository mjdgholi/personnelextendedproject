﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmItemValueGridMultiSelect.aspx.cs"
    Inherits="Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.FormulaEditor.Modal.FrmItemValueGridMultiSelect" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <telerik:RadScriptBlock ID="RadScriptBlock" runat="server">
        <script type="text/javascript">
            var myArrayKey = new Array();
            var myArrayTitle = new Array();
            var hiddenfieldvalue = '';
            var hiddenfieldtitle = '';
            var str = '';
            function RowSelected(sender, eventArgs) {

                KeyNamevalue = eventArgs._tableView.get_clientDataKeyNames()[0];
                KeyNameTitle = eventArgs._tableView.get_clientDataKeyNames()[1];

                hiddenfieldvalue = document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value;
                hiddenfieldtitle = document.getElementById('<%=hiddenfieldTitle.ClientID %>').value;
                var textKeyvalue = eventArgs.getDataKeyValue(KeyNamevalue);
                var txtTitlevalue = (eventArgs.getDataKeyValue(eventArgs._tableView.get_clientDataKeyNames()[1])).trim();
                if (include(myArrayKey, textKeyvalue) != true) {
                    if (myArrayKey.length > 0)
                        str = ',';
                    hiddenfieldvalue = hiddenfieldvalue + str + textKeyvalue;
                    hiddenfieldtitle = hiddenfieldtitle + str + (eventArgs.getDataKeyValue(eventArgs._tableView.get_clientDataKeyNames()[1])).trim();
                    myArrayKey.push(textKeyvalue);
                    myArrayTitle.push(txtTitlevalue);
                }
                document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value = hiddenfieldvalue;
                document.getElementById('<%=hiddenfieldTitle.ClientID %>').value = hiddenfieldtitle;
            }

            function RowDeselected(sender, eventArgs) {

                str = '';
                hiddenfieldvalue = '';
                hiddenfieldtitle = '';
                KeyName = eventArgs._tableView.get_clientDataKeyNames()[0];
                KeyNameTitle = eventArgs._tableView.get_clientDataKeyNames()[1];
                for (var i = 0; i < myArrayKey.length; i++) {
                    if (myArrayKey[i] == eventArgs.getDataKeyValue(KeyName))
                        myArrayKey.splice(i, 1);
                }
                for (var j = 0; j < myArrayTitle.length; j++) {
                    if (myArrayTitle[j] == (eventArgs.getDataKeyValue(eventArgs._tableView.get_clientDataKeyNames()[1])).trim())
                        myArrayTitle.splice(j, 1);
                }

                for (var i = 0; i < myArrayKey.length; i++) {
                    if (i != 0)
                        str = ',';
                    hiddenfieldvalue = hiddenfieldvalue + str + myArrayKey[i];
                }
                str = '';
                for (var j = 0; j < myArrayTitle.length; j++) {
                    if (j != 0)
                        str = ',';
                    hiddenfieldtitle = hiddenfieldtitle + str + myArrayTitle[j];
                }
                document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value = hiddenfieldvalue;
                document.getElementById('<%=hiddenfieldTitle.ClientID %>').value = hiddenfieldtitle;
            }

            function MasterTableViewCreated(sender, eventArgs) {
                if (document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value != '')
                    myArrayKey = document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value.split(',');
                if (document.getElementById('<%=hiddenfieldTitle.ClientID %>').value != '')
                    myArrayTitle = document.getElementById('<%=hiddenfieldTitle.ClientID %>').value.split(',');
                KeyName = sender.MasterTableView.get_clientDataKeyNames()[0];
                for (var j = 0; j < sender.get_masterTableView().get_dataItems().length; j++)
                    for (var i = 0; i < myArrayKey.length; i++) {
                        if (myArrayKey[i] == sender.get_masterTableView().get_dataItems()[j].getDataKeyValue(KeyName))
                            sender.get_masterTableView().get_dataItems()[j].set_selected(true);
                    }
            }


            function include(arr, obj) {
                if (arr.length == 0)
                    return false;
                for (var i = 0; i < arr.length; i++) {
                    if (arr[i] == obj)
                        return true;
                }
            }

            function CloseRadWindow() {
                var oArg = new Object();
                oArg.Title = document.getElementById('<%=hiddenfieldTitle.ClientID %>').value;
                oArg.KeyId = document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value;
                var oWnd = GetRadWindow();
                oWnd.close(oArg);
            }

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
        </script>
    </telerik:RadScriptBlock>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager runat="server">
    </telerik:RadScriptManager>
    <asp:HiddenField ID="hiddenfieldKeyId" runat="server" />
    <asp:HiddenField ID="hiddenfieldTitle" runat="server" />
    <table width="100%" dir="rtl">
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <cc1:customradbutton id="btnSelectAll" runat="server" customebuttontype="SelectAll"
                                onclick="btnSelectAll_Click">
                            </cc1:customradbutton>
                        </td>
                        <td>
                            <cc1:customradbutton id="btnSelectPersonnelAndBack" runat="server" onclientclicked="CloseRadWindow"
                                customebuttontype="Back" width="110px" skin="Windows7">
                            </cc1:customradbutton>
                        </td>
                        <td>
                            <cc1:custommessageerrorcontrol runat="server" ID="CustomMessageErrorControl1">
                            </cc1:custommessageerrorcontrol>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadGrid ID="grdItem" runat="server" AllowMultiRowSelection="True" AllowPaging="True"
                    AllowFilteringByColumn="True" AllowSorting="True" CellSpacing="0" datakeynames="ObjectId,ObjectTitle"
                    GridLines="None" HorizontalAlign="Center" ShowFooter="True" Skin="Outlook" OnNeedDataSource="grdItem_NeedDataSource"
                    OnColumnCreated="grdItem_ColumnCreated">
                    <MasterTableView ClientDataKeyNames="ObjectId,ObjectTitle" DataKeyNames="ObjectId,ObjectTitle"
                        Dir="RTL" GroupsDefaultExpanded="False" NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد"
                        AllowFilteringByColumn="True">
                        <Columns>
                            <telerik:GridClientSelectColumn UniqueName="Select">
                            </telerik:GridClientSelectColumn>
                        </Columns>
                        <PagerStyle AlwaysVisible="True" FirstPageToolTip="صفحه اول" HorizontalAlign="Center"
                            LastPageToolTip="صفحه آخر" NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی"
                            PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; به &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                            PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                            VerticalAlign="Middle" />
                        <PagerStyle AlwaysVisible="True"></PagerStyle>
                    </MasterTableView>
                    <ClientSettings AllowColumnsReorder="True" EnableRowHoverStyle="true" ReorderColumnsOnClient="True">
                        <ClientEvents OnMasterTableViewCreated="MasterTableViewCreated" OnRowDeselected="RowDeselected"
                            OnRowSelected="RowSelected"></ClientEvents>
                        <Selecting AllowRowSelect="True"></Selecting>
                    </ClientSettings>
                    <FilterMenu EnableImageSprites="False">
                    </FilterMenu>
                    <StatusBarSettings LoadingText="بارگذاری..." ReadyText="آماده" />
                </telerik:RadGrid>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
