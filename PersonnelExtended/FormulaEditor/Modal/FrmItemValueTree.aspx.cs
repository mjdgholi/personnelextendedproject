﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.FormulaEditor.Modal
{
    public partial class FrmItemValueTree : System.Web.UI.Page
    {
        private string selectedObjectId;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SetRadTreeView();
                Session["ObjectId"] = "";
                SetRadTreeView();
            }
        }

        #region Procedure:

        private void SetRadTreeView()
        {
            radPubObject.DataFieldID = "ObjectId";
            radPubObject.DataTextField = "ObjectTitle";
            radPubObject.DataValueField = "ObjectId";
            radPubObject.DataFieldParentID = "ObjectOwnerId";
            radPubObject.DataSource = null;
            radPubObject.DataBind();
        }

        #endregion

        protected void cmbBack_Click(object sender, EventArgs e)
        {
            var Value = "";
            var Title = "";
            var Str = "";
            foreach (var radTreeNode in radPubObject.CheckedNodes)
            {
                if (Value != "")
                    Str = ",";
                Value = Value + Str + radTreeNode.Value;
                Title = Title + Str + radTreeNode.Text;
            }
            hiddenfieldTitle.Value = Title;
            hiddenfieldKeyId.Value = Value;

            ClientScriptManager cs = Page.ClientScript;
            string js = "<script type='text/javascript'>CloseWindow()</script>";
            cs.RegisterStartupScript(this.GetType(), "CloseWindow", js);
        }
    }
}