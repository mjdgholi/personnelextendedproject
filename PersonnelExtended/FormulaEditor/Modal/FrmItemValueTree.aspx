﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmItemValueTree.aspx.cs" Inherits="Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.FormulaEditor.Modal.FrmItemValueTree" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI, Version=2012.3.1016.40, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">




        <table width="100%" dir="rtl" bgcolor="White" style="height: 480px">
            <tr>
                <td height="10" colspan="6" dir="rtl" align="right">
                    <asp:Label ID="LabTitle" runat="server" Text=" آیتم با ساختار درختی" Font-Bold="True"
                        Font-Names="Arial" Font-Size="12pt" ForeColor="#000099"></asp:Label>
                </td>
            </tr>
            <tr style="height: 15px">
                <td>
                    <cc1:CustomRadButton ID="cmbBack" runat="server" CustomeButtonType="Back"
                        OnClick="cmbBack_Click">
                        <Icon PrimaryIconCssClass="rbPrevious"></Icon>
                    </cc1:CustomRadButton>
                </td>
            </tr>
            <tr>
                <td align="right" dir="rtl" colspan="6" valign="top">
                    <telerik:RadTreeView ID="radPubObject" runat="server" Height="400px" 
                        Width="100%" Skin="Outlook"
                        CheckBoxes="True"
                        CheckChildNodes="True">
                    </telerik:RadTreeView>
                </td>
            </tr>
        </table>

        <asp:HiddenField ID="hiddenfieldKeyId" runat="server" />
        <asp:HiddenField ID="hiddenfieldTitle" runat="server" />
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                function onNodeChecking(sender, args) {
                    alert(args.get_node().get_text());
                }

                function CloseWindow() {
                    var hiddenfieldvalue = document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value;
                    var hiddenfieldTitle = document.getElementById('<%=hiddenfieldTitle.ClientID %>').value;
                    var oArg = new Object();
                    oArg.Title = hiddenfieldTitle;
                    oArg.KeyId = hiddenfieldvalue;
                    var oWnd = GetRadWindow();
                    oWnd.close(oArg);
                }


                function GetRadWindow() {
                    var oWindow = null;
                    if (window.radWindow) oWindow = window.radWindow;
                    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                    return oWindow;
                }

                fnObjectSearch:
                    {
                        var myValue = new Array(<%=Session["ObjectId"].ToString() %>);
                   var index = -1;

                   function NextNode() {
                       if (index < myValue.length - 1) {
                           index = index + 1;
                           expandNode(myValue[index]);
                           return true;
                       }
                       else {
                           return false;
                       }
                   }

                   function PreviousNode() {
                       if (index > 0) {
                           index = index - 1;
                           expandNode(myValue[index]);
                           return true;
                       }
                       else {
                           //               alert ('end Previous');
                           return false;
                       }
                   }


                   function expandNode(nodeid) {
                       var treeView = $find("<%= radPubObject.ClientID %>");
                       var node = treeView.findNodeByValue(nodeid);
                       if (node) {

                           node.expand();
                           node.select();
                           scrollToNode(treeView, node);
                           return true;
                       }
                       return false;
                   }


                   function scrollToNode(treeview, node) {
                       var nodeElement = node.get_contentElement();
                       var treeViewElement = treeview.get_element();

                       var nodeOffsetTop = treeview._getTotalOffsetTop(nodeElement);
                       var treeOffsetTop = treeview._getTotalOffsetTop(treeViewElement);
                       var relativeOffsetTop = nodeOffsetTop - treeOffsetTop;

                       if (relativeOffsetTop < treeViewElement.scrollTop) {
                           treeViewElement.scrollTop = relativeOffsetTop;
                       }

                       var height = nodeElement.offsetHeight;

                       if (relativeOffsetTop + height > (treeViewElement.clientHeight + treeViewElement.scrollTop)) {
                           treeViewElement.scrollTop += ((relativeOffsetTop + height) - (treeViewElement.clientHeight + treeViewElement.scrollTop));
                       }
                   }
                   }
            </script>

        </telerik:RadScriptBlock>
    </form>
</body>
</html>
