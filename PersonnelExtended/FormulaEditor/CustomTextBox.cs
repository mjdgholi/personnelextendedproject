﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.FormulaEditor
{
    public class CustomTextBox : TextBox
    {
        public int ProcFormulaAndConditionDetailId { get; set; }
        public int? PreLink { get; set; }

        public int? PostLink { get; set; }

        public int? UserAlterLink { get; set; }

        public int TextBoxTypeId { get; set; }       

        public string SqlValue { get; set; }

        public string AlterUserValue { get; set; } 
              
    }
}