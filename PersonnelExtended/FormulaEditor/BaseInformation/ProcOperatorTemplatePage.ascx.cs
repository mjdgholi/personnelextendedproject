﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ITC.Library.Classes;
using PersonnelExtended.BL;
using PersonnelExtended.Entity;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.FormulaEditor.BaseInformation
{
    public partial class ProcOperatorTemplatePage : ItcBaseControl
    {
        #region PublicParam:

        private readonly ProcOperatorTemplateBL _procOperatorTemplateBL = new ProcOperatorTemplateBL();
        private readonly ProcOperatorBL _procOperatorBL = new ProcOperatorBL();
        private const string TableName = "[Personnel].[v_ProcOperatorTemplate]";
        private const string PrimaryKey = "OperatorTemplateId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
            grdOperator.MasterTableView.GetColumnSafe("ShowAll").Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
            grdOperator.MasterTableView.GetColumnSafe("ShowAll").Visible = false;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            //CmbOperator.ClearSelection();
            CmbOperatorTemplate.ClearSelection();
            txtPriority.Text = string.Empty;
            CmbIsActive.SelectedIndex = 1;
        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var procOperatorTemplateEntity = new ProcOperatorTemplateEntity()
            {
                OperatorTemplateId = Int32.Parse(ViewState["OperatorTemplateId"].ToString())
            };
            var myOperatorTemplate = _procOperatorTemplateBL.GetSingleById(procOperatorTemplateEntity);
            CmbOperator.SelectedValue = myOperatorTemplate.OperatorId.ToString();
            CmbOperatorTemplate.SelectedValue = myOperatorTemplate.TemplateOperatorId.ToString();
            txtPriority.Text = myOperatorTemplate.Priority.ToString();
            CmbIsActive.SelectedValue = myOperatorTemplate.IsActive.ToString();
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }

        protected void SetCmbOperator()
        {
            var operatorData = _procOperatorBL.GetAllIsActive();
            CmbOperator.Items.Clear();
            CmbOperator.Items.Add(new RadComboBoxItem(""));
            CmbOperator.DataTextField = "OperatorTitle";
            CmbOperator.DataValueField = "OperatorId";
            CmbOperator.DataSource = operatorData;
            CmbOperator.DataBind();

            CmbOperatorTemplate.Items.Clear();
            CmbOperatorTemplate.Items.Add(new RadComboBoxItem(""));
            CmbOperatorTemplate.DataTextField = "OperatorTitle";
            CmbOperatorTemplate.DataValueField = "OperatorId";
            CmbOperatorTemplate.DataSource = operatorData;
            CmbOperatorTemplate.DataBind();
        }

        private GridParamEntity SetGridParam(int pageSize, int currentpPage, int rowSelectId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdOperator,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectId = rowSelectId
            };
            return gridParamEntity;
        }

        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = "  ";
                ViewState["SortExpression"] = "OperatorTitle,[Priority] ";
                ViewState["SortType"] = "Asc";
                SetCmbOperator();
                var gridParamEntity = SetGridParam(grdOperator.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }


        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int operatorTemplateId;
                ViewState["WhereClause"] = "";

                var procOperatorTemplateEntity = new ProcOperatorTemplateEntity()
                {
                    OperatorId = Int32.Parse(CmbOperator.SelectedValue),
                    TemplateOperatorId= Int32.Parse(CmbOperatorTemplate.SelectedValue),
                    Priority = Int32.Parse(txtPriority.Text),
                    IsActive = bool.Parse(CmbIsActive.SelectedValue),
                };

                _procOperatorTemplateBL.Add(procOperatorTemplateEntity, out operatorTemplateId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdOperator.MasterTableView.PageSize, 0, operatorTemplateId);
                DatabaseManager.ItcGetPage(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "1 = 1";
                if (CmbOperator.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and OperatorId=" + CmbOperator.SelectedValue;
                if (CmbOperatorTemplate.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and TemplateOperatorId=" + CmbOperatorTemplate.SelectedValue;
                grdOperator.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdOperator.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
                if (grdOperator.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "";
                var gridParamEntity = SetGridParam(grdOperator.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var procOperatorTemplateEntity = new ProcOperatorTemplateEntity()
                {
                    OperatorTemplateId = Int32.Parse(ViewState["OperatorTemplateId"].ToString()),
                    OperatorId = Int32.Parse(CmbOperator.SelectedValue),
                    TemplateOperatorId = Int32.Parse(CmbOperatorTemplate.SelectedValue),
                    Priority = Int32.Parse(txtPriority.Text),
                    IsActive = bool.Parse(CmbIsActive.SelectedValue),
                };
                _procOperatorTemplateBL.Update(procOperatorTemplateEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdOperator.MasterTableView.PageSize, 0, Int32.Parse(ViewState["OperatorTemplateId"].ToString()));
                DatabaseManager.ItcGetPage(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdOperator_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["OperatorTemplateId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = "";

                    var procOperatorTemplateEntity = new ProcOperatorTemplateEntity()
                    {
                        OperatorTemplateId = Int32.Parse(e.CommandArgument.ToString()),
                    };
                    _procOperatorTemplateBL.Delete(procOperatorTemplateEntity);
                    var gridParamEntity = SetGridParam(grdOperator.MasterTableView.PageSize, 0, -1);
                    DatabaseManager.ItcGetPage(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>
        protected void grdOperator_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdOperator.MasterTableView.PageSize, e.NewPageIndex, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>
        protected void grdOperator_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdOperator.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>
        protected void grdOperator_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdOperator.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #endregion
    }
}