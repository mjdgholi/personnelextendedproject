﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ITC.Library.Classes;
using PersonnelExtended.BL;
using PersonnelExtended.Entity;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.FormulaEditor.BaseInformation
{
    public partial class ProcObjectPage : ItcBaseControl
    {  
        #region PublicParam:

        private readonly ProcObjectBL _procObjectBL = new ProcObjectBL();
       private readonly ProcObjectTypeBL _procObjectTypeBL=new ProcObjectTypeBL();
        private const string TableName = "[Personnel].[v_ProcObject]";
        private const string PrimaryKey = "ObjectId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
            grdObject.MasterTableView.GetColumnSafe("ShowAll").Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
            grdObject.MasterTableView.GetColumnSafe("ShowAll").Visible = false;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            CmbObjectTitle.Text = string.Empty;
            TxtObjectTitlePersian.Text = string.Empty;
            CmbObjectTypeId.Text = string.Empty;
            CmbIsActive.SelectedIndex = 1;
            CmbIsHierarchy.SelectedIndex = 2;
        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var procObjectEntity = new ProcObjectEntity()
            {
                ObjectId = Int32.Parse(ViewState["ObjectId"].ToString())
            };
            var myObject = _procObjectBL.GetSingleById(procObjectEntity);
            CmbObjectTitle.SelectedValue = myObject.ObjectTitle;
            TxtObjectTitlePersian.Text = myObject.ObjectTitlePersian;
            CmbObjectTypeId.SelectedValue = myObject.ObjectTypeId.ToString();
            CmbIsHierarchy.SelectedValue = myObject.IsHierarchy.ToString();
            CmbIsActive.SelectedValue = myObject.IsActive.ToString();
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }

        protected void SetCmbObjectTitle()
        {
            CmbObjectTitle.Items.Clear();
            CmbObjectTitle.Items.Add(new RadComboBoxItem("", "-1"));
            CmbObjectTitle.DataTextField = "ObjectTitleDesc";
            CmbObjectTitle.DataValueField = "ObjectTitle";
            CmbObjectTitle.DataSource = _procObjectBL.GetAllSqlObject("","PersonnelFormula");
            CmbObjectTitle.DataBind();
        }

        protected void SetCmbObjectType()
        {
            CmbObjectTypeId.Items.Clear();
            CmbObjectTypeId.Items.Add(new RadComboBoxItem(""));
            CmbObjectTypeId.DataTextField = "ObjectTypeTitle";
            CmbObjectTypeId.DataValueField = "ObjectTypeId";
            CmbObjectTypeId.DataSource = _procObjectTypeBL.GetAllIsActive();
            CmbObjectTypeId.DataBind();
        }

        private GridParamEntity SetGridParam(int pageSize, int currentpPage, int rowSelectId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdObject,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectId = rowSelectId
            };
            return gridParamEntity;
        }

        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = "  ";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdObject.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetCmbObjectTitle();
                SetCmbObjectType();
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }


        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int ObjectId;
                ViewState["WhereClause"] = "";

                var procObjectEntity = new ProcObjectEntity()
                {
                    ObjectTitle = CmbObjectTitle.SelectedItem.Text,
                    ObjectTitlePersian = TxtObjectTitlePersian.Text,
                    ObjectTypeId =  Int32.Parse(CmbObjectTypeId.SelectedValue),
                    IsHierarchy = bool.Parse(CmbIsHierarchy.SelectedValue),
                    IsActive = bool.Parse(CmbIsActive.SelectedValue),
                };

                _procObjectBL.Add(procObjectEntity, out ObjectId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdObject.MasterTableView.PageSize, 0, ObjectId);
                DatabaseManager.ItcGetPage(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "1 = 1";
                if (CmbObjectTitle.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ObjectTitle Like N'%" +
                                               FarsiToArabic.ToArabic(CmbObjectTitle.Text.Trim()) + "%'";
                grdObject.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdObject.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
                if (grdObject.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "";
                var gridParamEntity = SetGridParam(grdObject.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var procObjectEntity = new ProcObjectEntity()
                {
                    ObjectId = Int32.Parse(ViewState["ObjectId"].ToString()),
                    ObjectTitle = CmbObjectTitle.SelectedItem.Text,
                    ObjectTitlePersian = TxtObjectTitlePersian.Text,
                    ObjectTypeId = Int32.Parse(CmbObjectTypeId.SelectedValue),
                    IsHierarchy = bool.Parse(CmbIsHierarchy.SelectedValue),
                    IsActive = bool.Parse(CmbIsActive.SelectedValue),
                };
                _procObjectBL.Update(procObjectEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdObject.MasterTableView.PageSize, 0, Int32.Parse(ViewState["ObjectId"].ToString()));
                DatabaseManager.ItcGetPage(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdObject_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["ObjectId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = "";

                    var procObjectEntity = new ProcObjectEntity()
                    {
                        ObjectId = Int32.Parse(e.CommandArgument.ToString()),
                    };
                    _procObjectBL.Delete(procObjectEntity);
                    var gridParamEntity = SetGridParam(grdObject.MasterTableView.PageSize, 0, -1);
                    DatabaseManager.ItcGetPage(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>
        protected void grdObject_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdObject.MasterTableView.PageSize, e.NewPageIndex, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>
        protected void grdObject_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdObject.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>
        protected void grdObject_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdObject.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #endregion
    }
}