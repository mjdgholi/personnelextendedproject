﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ITC.Library.Classes;
using PersonnelExtended.BL;
using PersonnelExtended.Entity;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.FormulaEditor.BaseInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/04/21>
    /// Description: < آیتم>
    /// </summary>
    public partial class ProcItemPage : ItcBaseControl
    {
        #region PublicParam:

        private readonly ProcItemBL _procItemBL = new ProcItemBL();
        private readonly ProcObjectBL _procObjectBL = new ProcObjectBL();     
        private readonly ProcItemValueTypeBL _itemValueTypeBL = new ProcItemValueTypeBL();
        private const string TableName = "[Personnel].[v_ProcItem]";
        private const string PrimaryKey = "ItemId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
            PnlOtherInformation.Visible = false;
            grdItem.MasterTableView.GetColumnSafe("ShowAll").Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
            PnlOtherInformation.Visible = false;
            grdItem.MasterTableView.GetColumnSafe("ShowAll").Visible = false;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtItemTitle.Text = string.Empty;
            TxtShowOrder.Text = string.Empty;
            CmbObject.ClearSelection();
            CmbObject.ClearSelection();
            CmbItemValueType.ClearSelection();

        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var procItemEntity = new ProcItemEntity()
            {
                ItemId = Int32.Parse(ViewState["ItemId"].ToString())
            };
            var myItem = _procItemBL.GetSingleById(procItemEntity);
            txtItemTitle.Text = myItem.ItemTitle;
            TxtShowOrder.Text = myItem.ShowOrder.ToString();
            var ObjectId = myItem.ObjectId;
            if (ObjectId !=null)
            {
                var ObjectEntity = new ProcObjectEntity();
                ObjectEntity.ObjectId = (int) ObjectId;
                var myObject = _procObjectBL.GetSingleById(ObjectEntity);
                var objectId = myObject.ObjectId;
                CmbObject.SelectedValue = objectId.ToString();
                SetCmbObject(objectId);
                CmbObject.SelectedValue = ObjectId.ToString();
            }
            CmbItemValueType.SelectedValue = myItem.ItemValueTypeId.ToString();
            CmbHasTitle.SelectedValue = myItem.HasTitle.ToString();
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }

        /// <summary>
        ///  پر کردن لیست کشویی نوع مقدار آیتم
        /// </summary>
        private void SetCmbItemValueType()
        {
            CmbItemValueType.Items.Clear();
            CmbItemValueType.Items.Add(new RadComboBoxItem(""));
            CmbItemValueType.DataTextField = "ItemValueTypeTitle";
            CmbItemValueType.DataValueField = "ItemValueTypeId";
            CmbItemValueType.DataSource = _itemValueTypeBL.GetAllIsActive();
            CmbItemValueType.DataBind();
        }

        /// <summary>
        ///  پر کردن لیست کشویی آبجکتها
        /// </summary>
        private void SetCmbObject()
        {
            CmbObject.Items.Clear();
            CmbObject.Items.Add(new RadComboBoxItem(""));
            CmbObject.DataTextField = "ObjectTitlePersian";
            CmbObject.DataValueField = "ObjectId";
            CmbObject.DataSource = _procObjectBL.GetAllIsActive();
            CmbObject.DataBind();
        }

        /// <summary>
        /// پر کردن لیست کشویی فیلدهای آبجکت
        /// </summary>
        private void SetCmbObject(int objectId)
        {
            CmbObject.Items.Clear();
            CmbObject.Items.Add(new RadComboBoxItem(""));
            CmbObject.DataTextField = "ObjectTitlePersian";
            CmbObject.DataValueField = "ObjectId";
            CmbObject.DataSource = _procObjectBL.GetAllIsActive();
            CmbObject.DataBind();
        }

        private GridParamEntity SetGridParam(int pageSize, int currentpPage, int rowSelectId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdItem,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectId = rowSelectId
            };
            return gridParamEntity;
        }

        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = "  ";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                SetCmbItemValueType();
                SetCmbObject();
                var gridParamEntity = SetGridParam(grdItem.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void CmbObject_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (CmbObject.SelectedIndex > 0)
            {
                var objectId = e.Value;
                SetCmbObject(Int32.Parse(objectId));
            }
            else
            {
                CmbObject.Items.Clear();
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int itemId;
                ViewState["WhereClause"] = "";

                var procItemEntity = new ProcItemEntity()
                {
                    ItemTitle = txtItemTitle.Text,
                    HasTitle = (CmbHasTitle.SelectedIndex > 0) ? bool.Parse(CmbHasTitle.SelectedValue) : (bool?) null,
                    ItemValueTypeId = (CmbItemValueType.SelectedIndex > 0) ? Int32.Parse(CmbItemValueType.SelectedValue) : (int?) null,
                    ObjectId = (CmbObject.SelectedIndex > 0) ? Int32.Parse(CmbObject.SelectedValue) : (int?) null,
                    ShowOrder = (TxtShowOrder.Text.Trim() != string.Empty) ? Int32.Parse(TxtShowOrder.Text) : (int?) null,
                };

                _procItemBL.Add(procItemEntity, out itemId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdItem.MasterTableView.PageSize, 0, itemId);
                DatabaseManager.ItcGetPage(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "1 = 1";
                if (txtItemTitle.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ItemTitle Like N'%" +
                                               FarsiToArabic.ToArabic(txtItemTitle.Text.Trim()) + "%'";
                if (CmbItemValueType.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And ItemValueTypeId=" + CmbItemValueType.SelectedValue;
                if (CmbObject.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And ObjectId=" + CmbObject.SelectedValue;
                if (CmbObject.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And ObjectId=" + CmbObject.SelectedValue;
                if (CmbHasTitle.SelectedIndex != 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And HasTitle='" +
                                               (CmbHasTitle.SelectedItem.Value.Trim()) + "'";
                grdItem.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdItem.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
                if (grdItem.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "";
                var gridParamEntity = SetGridParam(grdItem.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var procItemEntity = new ProcItemEntity()
                {
                    ItemId = Int32.Parse(ViewState["ItemId"].ToString()),
                    ItemTitle = txtItemTitle.Text,
                    HasTitle = (CmbHasTitle.SelectedIndex > 0) ? bool.Parse(CmbHasTitle.SelectedValue) : (bool?) null,
                    ItemValueTypeId = (CmbItemValueType.SelectedIndex > 0) ? Int32.Parse(CmbItemValueType.SelectedValue) : (int?) null,
                    ObjectId = (CmbObject.SelectedIndex > 0) ? Int32.Parse(CmbObject.SelectedValue) : (int?) null,
                    ShowOrder = (TxtShowOrder.Text.Trim() != string.Empty) ? Int32.Parse(TxtShowOrder.Text) : (int?) null,
                };
                _procItemBL.Update(procItemEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdItem.MasterTableView.PageSize, 0, Int32.Parse(ViewState["ItemId"].ToString()));
                DatabaseManager.ItcGetPage(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdItem_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["ItemId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = "";

                    var procItemEntity = new ProcItemEntity()
                    {
                        ItemId = Int32.Parse(e.CommandArgument.ToString()),
                    };
                    _procItemBL.Delete(procItemEntity);
                    var gridParamEntity = SetGridParam(grdItem.MasterTableView.PageSize, 0, -1);
                    DatabaseManager.ItcGetPage(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
                if (e.CommandName == "OtherInformation")
                {
                   // PnlMaster.Visible = false;
                    radmultipageOtherInformation.PageViews.Clear();
                    PnlOtherInformation.Visible = true;
                    var itemId = Int32.Parse(e.CommandArgument.ToString());
                    Session["ItemId"] = itemId;

                    ViewState["WhereClause"] = "ItemId='" + itemId + "'";
                    var gridParamEntity = SetGridParam(grdItem.MasterTableView.PageSize, 0, itemId);
                    DatabaseManager.ItcGetPage(gridParamEntity);
                    e.Item.Selected = true;
                    grdItem.MasterTableView.GetColumnSafe("ShowAll").Visible = true;
                    RadTabOtherInformation_TabClick(new object(), new RadTabStripEventArgs(RadTabOtherInformation.FindTabByValue("ProcItemDetailProcessPage.ascx")));
                }                        
                if (e.CommandName == "_ShowAll")
                {
                    try
                    {
                        ViewState["WhereClause"] = "";
                        var gridParamEntity = SetGridParam(grdItem.MasterTableView.PageSize, 0, -1);
                        DatabaseManager.ItcGetPage(gridParamEntity);
                        SetPanelFirst();
                    }
                    catch (Exception ex)
                    {
                        CustomMessageErrorControl.ShowErrorMessage(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>
        protected void grdItem_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdItem.MasterTableView.PageSize, e.NewPageIndex, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>
        protected void grdItem_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdItem.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>
        protected void grdItem_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdItem.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #endregion

        protected void RadTabOtherInformation_TabClick  (object sender, RadTabStripEventArgs e)
        {
            radmultipageOtherInformation.PageViews.Clear();
            const string baseUrl = @"DesktopModules\PersonnelExtendedProject\PersonnelExtended\FormulaEditor\Operational\";
            var pageView = new RadPageView
            {
                ID = baseUrl + e.Tab.Value,
            };
            radmultipageOtherInformation.PageViews.Add(pageView);
            e.Tab.Selected = true;
        }

        protected void radmultipageOtherInformation_PageViewCreated(object sender, RadMultiPageEventArgs e)
        {
            if (Session["ItemId"] == null)
            {
                Session["MessageError"] = "کاربر گرامی، به علت بدون استفاده ماندن بیش از حد صفحه برای امنیت اطلاعات، عملیات را از ابتدا آغاز نمایید. ";               
                Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri);
            }

            if (!string.IsNullOrEmpty(e.PageView.ID))
            {
                string userControlName = e.PageView.ID;
                var userControl = (Page.LoadControl(userControlName));
                if (userControl != null)
                {
                    userControl.ID = ViewState["MenuPageName"] + "_userControl";
                }
                if (userControl != null) e.PageView.Controls.Add(userControl);
                var tabedControl = (radmultipageOtherInformation.PageViews[0]).Controls[0] as ITC.Library.Classes.ITabedControl;
                if (tabedControl != null) tabedControl.InitControl();
                e.PageView.Selected = true;
            }
        }
    }
}