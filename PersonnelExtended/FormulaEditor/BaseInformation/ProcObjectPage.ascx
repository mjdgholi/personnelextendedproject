﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProcObjectPage.ascx.cs" Inherits="Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.FormulaEditor.BaseInformation.ProcObjectPage" %>
<%@ Register Assembly="ITC.Library" Namespace="ITC.Library.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .auto-style1 {
        width: 308px;
    }
</style>
<asp:Panel ID="Panel1" runat="server">
    <table style="width: 100%; background-color: #F0F8FF;" dir="rtl">
        <tr>
            <td dir="rtl" style="width: 100%;" valign="top">
                <asp:Panel ID="pnlButton" runat="server">
                    <table>
                        <tr>
                            <td>
                                <cc1:CustomRadButton ID="btnSave" runat="server" CustomeButtonType="Add" OnClick="btnSave_Click">
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnSearch" runat="server" CustomeButtonType="Search" CausesValidation="False"
                                    OnClick="btnSearch_Click">
                                    <Icon PrimaryIconCssClass="rbSearch" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnShowAll" runat="server" CustomeButtonType="ShowAll" CausesValidation="False"
                                    OnClick="btnShowAll_Click">
                                    <Icon PrimaryIconCssClass="rbRefresh" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnEdit" runat="server" CustomeButtonType="Edit" OnClick="btnEdit_Click">
                                    <Icon PrimaryIconCssClass="rbEdit" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnBack" runat="server" CustomeButtonType="Back" OnClick="btnBack_Click">
                                    <Icon PrimaryIconCssClass="rbPrevious" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <asp:Panel ID="pnlDetail" runat="server" Width="100%">
                    <table>
                        <tr>
                            <td nowrap="nowrap">
                                <asp:Label runat="server" ID="lblOObjectTitle">عنوان آبجکت <font color="red">*</font>:</asp:Label>
                            </td>
                            <td >
                                <telerik:RadComboBox ID="CmbObjectTitle" runat="server" Width="300px" AppendDataBoundItems="True" Filter="Contains" MarkFirstMatch="True">
                                </telerik:RadComboBox>
                                <asp:RequiredFieldValidator ID="rfvObjectTitle" runat="server" ControlToValidate="CmbObjectTitle"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">
                                <asp:Label ID="lblOObjectTitlePersian" runat="server">عنوان فارسی <font color="red">*</font>:</asp:Label>
                            </td>
                            <td >
                                <telerik:RadTextBox ID="TxtObjectTitlePersian" runat="server" Width="300px">
                                </telerik:RadTextBox>
                                <asp:RequiredFieldValidator ID="rfvObjectTitlePersian" runat="server" ControlToValidate="TxtObjectTitlePersian" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">
                                <asp:Label ID="lblObjectTypeId" runat="server"> نوع آبجکت</asp:Label>
                            </td>
                            <td >
                                <telerik:RadComboBox ID="CmbObjectTypeId" runat="server" Width="300px" AppendDataBoundItems="True">
                                </telerik:RadComboBox>
                                <asp:RequiredFieldValidator ID="rfvObjectType" runat="server" ControlToValidate="CmbObjectTypeId" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">
                                <asp:Label ID="LblIsHierarchy" runat="server">آیا ساختار سلسله مراتبی دارد؟</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="CmbIsHierarchy" runat="server" AppendDataBoundItems="True" Filter="Contains" MarkFirstMatch="True">
                                    <Items>
                                        <telerik:RadComboBoxItem Text="" Value="" />
                                        <telerik:RadComboBoxItem Text="بلی" Value="True" />
                                        <telerik:RadComboBoxItem Text="خیر" Value="False" />
                                    </Items>
                                </cc1:CustomRadComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="LblIsActive" runat="server">آیا این رکورد فعال است؟</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="CmbIsActive" runat="server" AppendDataBoundItems="True" Filter="Contains" MarkFirstMatch="True">
                                    <Items>
                                        <telerik:RadComboBoxItem Text="" Value="" />
                                        <telerik:RadComboBoxItem Text="بلی" Value="True" />
                                        <telerik:RadComboBoxItem Text="خیر" Value="False" />
                                    </Items>
                                </cc1:CustomRadComboBox>
                                <asp:RequiredFieldValidator ID="rfvIsActive" runat="server" ControlToValidate="CmbIsActive" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top" bgcolor="White">
                <asp:Panel ID="pnlGrid" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <telerik:RadGrid ID="grdObject" runat="server" AllowCustomPaging="True" AllowPaging="True"
                                    AllowSorting="True" CellSpacing="0" GridLines="None" HorizontalAlign="Center"
                                    AutoGenerateColumns="False" OnItemCommand="grdObject_ItemCommand" OnPageIndexChanged="grdObject_PageIndexChanged"
                                    OnPageSizeChanged="grdObject_PageSizeChanged" OnSortCommand="grdObject_SortCommand">
                                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Office2007">
                                    </HeaderContextMenu>
                                    <MasterTableView DataKeyNames="ObjectId" Dir="RTL" GroupsDefaultExpanded="False" NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="ObjectTitle" HeaderText="عنوان آبجکت" SortExpression="ObjectTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ObjectTitlePersian" HeaderText="عنوان فارسی"
                                                SortExpression="ObjectTitlePersian">
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>                                            
                                             <telerik:GridBoundColumn DataField="ObjectTypeTitle" HeaderText="نوع آبجکت"
                                                SortExpression="ObjectTypeTitle">
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>    
                                            <telerik:GridCheckBoxColumn DataField="IsActive" HeaderText="وضیعت رکورد" SortExpression="IsActive">
                                            </telerik:GridCheckBoxColumn>
                                            <telerik:GridTemplateColumn HeaderText="ویرایش" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="TemplateColumn1">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton" runat="server" CausesValidation="false" CommandArgument='<%#Eval("ObjectId").ToString() %>'
                                                        CommandName="_MyِEdit" ForeColor="#000066" ImageUrl="../../Images/Edit.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="حذف" FilterControlAltText="Filter TemplateColumn column"
                                                UniqueName="TemplateColumn">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton1" runat="server" CausesValidation="false" OnClientClick="return confirm('آیا اطلاعات رکورد حذف شود؟');"
                                                        CommandArgument='<%#Eval("ObjectId").ToString() %>' CommandName="_MyِDelete" ForeColor="#000066"
                                                        ImageUrl="../../Images/Delete.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="نمایش همه"
                                                UniqueName="ShowAll">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgShoaAll" runat="server" CausesValidation="false" CommandArgument='<%#Eval("ObjectId").ToString() %>'
                                                        CommandName="_ShowAll" ForeColor="#000066" ImageUrl="../../Images/ShowAllInGrid.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                        </ExpandCollapseColumn>
                                        <EditFormSettings>
                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                            </EditColumn>
                                        </EditFormSettings>
                                        <PagerStyle FirstPageToolTip="صفحه اول" HorizontalAlign="Center" LastPageToolTip="صفحه آخر"
                                            NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آبجکت &lt;strong&gt;{2}&lt;/strong&gt; به &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                                            PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                                            VerticalAlign="Middle" />
                                    </MasterTableView><FilterMenu EnableImageSprites="False">
                                    </FilterMenu>
                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnSave">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnShowAll">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnEdit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnBack">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="grdObject">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
</telerik:RadAjaxLoadingPanel>
