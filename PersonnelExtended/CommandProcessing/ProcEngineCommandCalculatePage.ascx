﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProcEngineCommandCalculatePage.ascx.cs" Inherits="Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.CommandProcessing.ProcEngineCommandCalculatePage" %>
<%@ Register Assembly="ITC.Library" Namespace="ITC.Library.Controls" TagPrefix="cc1" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<table align="right" dir="rtl" width="100%">
    <tr>
        <td>
            <table>
                <tr>
                    <td>
                  
                        <cc1:CustomRadButton ID="btnCalculate" runat="server" 
                            CustomeButtonType="Calculate" onclick="btnCalculate_Click" >
<Icon PrimaryIconUrl="mvwres://ITC.Library, Version=1.0.0.1, Culture=neutral, PublicKeyToken=null/ITC.Library.Images.RadButtonImage.Calculate.png"></Icon>
                        </cc1:CustomRadButton>
                  
                    </td>
                    <td>
                        <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />   
                    </td>
                </tr>
            </table>
        </td>
        </tr>
        <tr>
        <td>
            
            <telerik:RadGrid ID="grdProcEngineCommandCalculate" runat="server" AllowCustomPaging="True"
                                                    AllowPaging="True" AllowSorting="True" 
                CellSpacing="0" GridLines="None" HorizontalAlign="Center" Width="100%"
                                                    AutoGenerateColumns="False" 
                AllowMultiRowSelection="True" 
                onitemdatabound="grdProcEngineCommandCalculate_ItemDataBound">
                                                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Office2007">
                                                    </HeaderContextMenu>
                                                    <MasterTableView DataKeyNames="TemporaryCommandmentId,ToolTip" Dir="RTL" GroupsDefaultExpanded="False"
                                                        ClientDataKeyNames="TemporaryCommandmentId" NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد">
                                                        <Columns>
                                                            <telerik:GridBoundColumn DataField="PersonnelNo" HeaderText="شماره پرسنلی" SortExpression="PersonnelNo">
                                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="NationalNo" HeaderText="کد ملی" SortExpression="NationalNo">
                                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="FirstName" HeaderText="نام" SortExpression="FirstName">
                                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="LastName" HeaderText="نام خانوادگی" SortExpression="LastName">
                                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="FatherName" HeaderText="نام پدر" SortExpression="FatherName">
                                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="ToolTip" HeaderText="نوع حکم" SortExpression="ToolTip">
                                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="ExecutionDate" HeaderText="تاریخ اجرا" SortExpression="ExecutionDate">
                                                          </telerik:GridBoundColumn>
                                                            <telerik:GridClientSelectColumn UniqueName="Select" HeaderText="انتخاب">
                                                            </telerik:GridClientSelectColumn>
                                                        </Columns>
                                                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                                                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                                        </RowIndicatorColumn>
                                                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                                        </ExpandCollapseColumn>
                                                        <EditFormSettings>
                                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                                            </EditColumn>
                                                        </EditFormSettings>
                                                        <PagerStyle FirstPageToolTip="صفحه اول" HorizontalAlign="Center" LastPageToolTip="صفحه آخر"
                                                            NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; به &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                                                            PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                                                            VerticalAlign="Middle" />
                                                    </MasterTableView>
                                                    <ClientSettings ReorderColumnsOnClient="True" AllowColumnsReorder="True" EnableRowHoverStyle="true">
                                                        <ClientEvents OnRowSelected="CalculateRowSelected" OnRowDeselected="CalculateRowDeselected" OnMasterTableViewCreated="CalculateMasterTableViewCreated">
                                                        </ClientEvents>
                                                        <Selecting AllowRowSelect="True"></Selecting>
                                                    </ClientSettings>
                                                    <FilterMenu EnableImageSprites="False">
                                                    </FilterMenu>
                                                </telerik:RadGrid> 
        </td>
    </tr>
</table>


<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnCalculate">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="CustomMessageErrorControl" />
                <telerik:AjaxUpdatedControl ControlID="grdProcEngineCommandCalculate" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="hiddenfieldKeyId" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default">
</telerik:RadAjaxLoadingPanel>

<asp:HiddenField ID="hiddenfieldKeyId" runat="server" />
<telerik:RadScriptBlock ID="RadScriptBlock" runat="server">
    <script type="text/javascript">
        var myArrayKey = new Array();
        var hiddenfieldvalue = '';
        var str = '';
        function CalculateRowSelected(sender, eventArgs) {            
            KeyNamevalue = eventArgs._tableView.get_clientDataKeyNames()[0];            
            hiddenfieldvalue = document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value;
            var textKeyvalue = eventArgs.getDataKeyValue(KeyNamevalue);
            if (include(myArrayKey, textKeyvalue) != true) {
                if (myArrayKey.length > 0)
                    str = ',';
                hiddenfieldvalue = hiddenfieldvalue + str + textKeyvalue;
                myArrayKey.push(textKeyvalue);
            }
            document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value = hiddenfieldvalue;            
        }

        function CalculateRowDeselected(sender, eventArgs) {

            str = '';
            hiddenfieldvalue = '';
            KeyName = eventArgs._tableView.get_clientDataKeyNames()[0];
            for (var i = 0; i < myArrayKey.length; i++) {
                if (myArrayKey[i] == eventArgs.getDataKeyValue(KeyName))
                    myArrayKey.splice(i, 1);
            }
            for (var i = 0; i < myArrayKey.length; i++) {
                if (i != 0)
                    str = ',';
                hiddenfieldvalue = hiddenfieldvalue + str + myArrayKey[i];
            }
            document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value = hiddenfieldvalue;
        }

        function CalculateMasterTableViewCreated(sender, eventArgs) {
            if (document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value != '')
                myArrayKey = document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value.split(',');
            KeyName = sender.MasterTableView.get_clientDataKeyNames()[0];
            for (var j = 0; j < sender.get_masterTableView().get_dataItems().length; j++)
                for (var i = 0; i < myArrayKey.length; i++) {
                    if (myArrayKey[i] == sender.get_masterTableView().get_dataItems()[j].getDataKeyValue(KeyName))
                        sender.get_masterTableView().get_dataItems()[j].set_selected(true);
                }
        }


        function include(arr, obj) {
            if (arr.length == 0)
                return false;
            for (var i = 0; i < arr.length; i++) {
                if (arr[i] == obj)
                    return true;
            }
        }

    </script>
</telerik:RadScriptBlock>

