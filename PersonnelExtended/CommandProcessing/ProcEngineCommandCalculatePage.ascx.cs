﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.Personnel.Classes;
using ITC.Library.Classes;
using ITC.Library.Controls;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.CommandProcessing
{
    public partial class ProcEngineCommandCalculatePage : ItcBaseControl
    {

        #region PublicParam:
        private const string TableName = "Personnel.v_ProcEnginCommandCalculate";
        private const string PrimaryKey = "TemporaryCommandmentId";

        #endregion
        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>




        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, int rowSelectId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdProcEngineCommandCalculate,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectId = -1
            };
            return gridParamEntity;
        }

        #endregion

        #region ControlEvent:
        protected void Page_Load(object sender, EventArgs e)
        {

        }




        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = "PersonId in (" + Session["PersonId"]+")";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdProcEngineCommandCalculate.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
         
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        protected void grdProcEngineCommandCalculate_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {

            if (e.Item is GridDataItem)
            {
                GridDataItem item = e.Item as GridDataItem;                
                e.Item.ToolTip = item.GetDataKeyValue("ToolTip").ToString();
            }
        }
        #endregion

        protected void btnCalculate_Click(object sender, EventArgs e)
        {
            DataRow dr = ProcEnginWarrant.ProcItemCalculateCount(hiddenfieldKeyId.Value);
            
        }

        

        
    }
}