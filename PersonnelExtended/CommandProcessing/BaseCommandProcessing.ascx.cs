﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Common.ItcException;
using ITC.Library.Classes;
using ITC.Library.Classes.ItcException;
using ITC.Library.Controls;
using Telerik.Web.UI;
using ItcApplicationErrorManagerException = ITC.Library.Classes.ItcException.ItcApplicationErrorManagerException;

namespace Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.CommandProcessing
{
    public partial class BaseCommandProcessing : ItcBaseControl
    {        
        #region PublicParam:
        private TextBox FirstName;
        private const string TableName = "Personnel.v_PrsPersonnelCommandProcessingSearch";
        private const string PrimaryKey = "PersonId";
        private RadGrid grdCommandProcessingSearch;
        private TextBox txtPersonnelNo;
        private TextBox txtNationalNo;
        private TextBox txtFirstName;
        private TextBox txtLastName;
        private SelectControl SelectPersonnelSearch;
        private RadMultiPage radmultipageCommandProccessing ;


        #endregion


        #region Procedure:


        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {

        }



        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, int rowSelectId)
        {
            grdCommandProcessingSearch = (RadGrid)radpanelbarSearch.FindItemByValue("PersonalInformation").FindControl("grdCommandProcessingSearch");
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdCommandProcessingSearch,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectId = rowSelectId,
                UserId = int.Parse(HttpContext.Current.User.Identity.Name),
                ObjectTitlesForRowAcess = "Personnel_RowAccess.v_PrsOrganizationPhysicalChart"
                
            };
            return gridParamEntity;
        }

        private void AddPageView(RadTab tab)
        {
            var pageView = new RadPageView();
            pageView.ID = tab.PageViewID;
            radmultipageCommandProccessing = (RadMultiPage)radpanelbarSearch.FindItemByValue("CommandmentProcessInformation").FindControl("radmultipageCommandProccessing");
            radmultipageCommandProccessing.PageViews.Add(pageView);
        }
        #endregion


        #region ITabedControl Members

        public override void InitControl()
        {
            ViewState["WhereClause"] = "1 = 1";
            ViewState["SortExpression"] = " ";
            ViewState["SortType"] = "Asc";
            //FirstName=(TextBox)radpanelbarSearch.FindItemByValue("PersonalInformation").FindControl("txtFirstName");
            
        }


        #endregion

        #region ControlEvent:
        /// <summary>
        /// جستجو کردن
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                txtFirstName = (TextBox)radpanelbarSearch.FindItemByValue("PersonalInformation").FindControl("txtFirstName");
                txtLastName = (TextBox)radpanelbarSearch.FindItemByValue("PersonalInformation").FindControl("txtLastName");
                txtNationalNo = (TextBox)radpanelbarSearch.FindItemByValue("PersonalInformation").FindControl("txtNationalNo");
                txtPersonnelNo = (TextBox)radpanelbarSearch.FindItemByValue("PersonalInformation").FindControl("txtPersonnelNo");
                SelectPersonnelSearch=(SelectControl)radpanelbarSearch.FindItemByValue("PersonalInformation").FindControl("SelectPersonnelSearch");
                if(txtFirstName.Text!="")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and FirstName Like N'%" +
                           FarsiToArabic.ToArabic(txtFirstName.Text.Trim()) + "%'";
                if (txtLastName.Text != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and LastName Like N'%" +
                           FarsiToArabic.ToArabic(txtLastName.Text.Trim()) + "%'";
                if (txtNationalNo.Text != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and NationalNo ='" +
                           txtNationalNo.Text+ "'";
                if (txtPersonnelNo.Text != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and PersonnelNo Like N'%" +
                          FarsiToArabic.ToArabic(txtPersonnelNo.Text.Trim()) + "%'";
                if(SelectPersonnelSearch.KeyId!="")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and PersonId in (" +
                          SelectPersonnelSearch.KeyId+ ")";
              
                grdCommandProcessingSearch = (RadGrid)radpanelbarSearch.FindItemByValue("PersonalInformation").FindControl("grdCommandProcessingSearch");
              
                var gridParamEntity = SetGridParam(grdCommandProcessingSearch.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);
                if (grdCommandProcessingSearch.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// اضافه کردن صفحه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void radmultipageCommandProccessing_PageViewCreated(object sender, RadMultiPageEventArgs e)
        {
               try
            {
            //if (Session["PersonId"] == null)
            //{
            //    Session["MessageError"] = "کاربر گرامی، به علت بدون استفاده ماندن بیش از حد صفحه برای امنیت اطلاعات، عملیات را از ابتدا آغاز نمایید. ";
            //    //Response.Redirect(Intranet.Common.IntranetUI.BuildUrl("/Page.aspx?mID=4253" + "&page=IsargaranProject/Isargaran/SacrificeInformation/DossierSacrificePage"));
            //    Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri);

            //}
               
            string userControlName = @"DeskTopModules\PersonnelExtendedProject\PersonnelExtended\" + e.PageView.ID + ".ascx";

            var userControl = (Page.LoadControl(userControlName));

            if (userControl != null)
            {

                userControl.ID = e.PageView.ID + "_userControl";
            }

            e.PageView.Controls.Add(userControl);
            }
               catch (Exception ex)
               {
                   CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
               }
        }
        /// <summary>
        ///  انتخاب کردن هریک از تب ها
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void radtabCommandProccessing_TabClick(object sender, RadTabStripEventArgs e)
        {
            try
            {
                
                if (hiddenfieldKeyId.Value == "")                    
                    throw new ItcApplicationErrorManagerException("برای پردازش اطلاعات حکمی فردی انتخاب نشده است");
                Session["PersonId"] = hiddenfieldKeyId.Value; 
                
                if (e.Tab.PageView == null)
                {
                    AddPageView(e.Tab);
                    e.Tab.PageView.Selected = true;
                }
                (e.Tab.PageView.Controls[0] as ITabedControl).InitControl();

               
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));                
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {    
            
                SetClearToolBox();
                ViewState["WhereClause"] = "";
                grdCommandProcessingSearch = (RadGrid)radpanelbarSearch.FindItemByValue("PersonalInformation").FindControl("grdCommandProcessingSearch");
                var gridParamEntity = SetGridParam(grdCommandProcessingSearch.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);
                
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        #endregion
    }
}