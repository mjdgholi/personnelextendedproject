﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BaseCommandProcessing.ascx.cs"
    Inherits="Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.CommandProcessing.BaseCommandProcessing" %>
<%@ Register Assembly="ITC.Library" Namespace="ITC.Library.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<table align="right" dir="rtl" width="100%">
    <tr>
               <td>
                   
         <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                                                        </td>
    </tr>
    <tr>
        <td >
            <telerik:RadPanelBar runat="server" ID="radpanelbarSearch" Width="100%" dir="rtl"
                Skin="Outlook">
                <Items>
                    <telerik:RadPanelItem Text="جستجو" Expanded="True" Font-Bold="True" Font-Size="11"
                        Font-Names="Arial" ForeColor="White" ImagePosition="Right" ImageUrl="../Images/Search.png">
                        <Items>
                            <telerik:RadPanelItem Value="PersonalInformation" Expanded="True">
                                <ItemTemplate>
                                    <table width="100%">
                                        <tr>
                                            <td colspan="=4">
                                                <table>
                                                    <tr>
                                                        <td width="7%">
                                                            <cc1:CustomRadButton ID="btnSearch" runat="server" CustomeButtonType="Search" OnClick="btnSearch_Click">
                                                            </cc1:CustomRadButton>
                                                        </td>
                                                        <td width="7%">
                                                            <cc1:CustomRadButton ID="btnShowAll" runat="server" CustomeButtonType="ShowAll" OnClick="btnShowAll_Click">
                                                            </cc1:CustomRadButton>
                                                        </td>
                                                     
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label runat="server" ID="lblSelectControlPersonnel">انتخاب پرسنل:</asp:Label>
                                            </td>
                                            <td>
                                                <cc1:SelectControl ID="SelectPersonnelSearch" runat="server" PortalPathUrl="Personnel/PrsPersonnelSearch.aspx"
                                                    ToolTip="جستجو پرسنل" MultiSelect="True" RadWindowWidth="950" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="10%">
                                                <asp:Label runat="server" ID="lblPersonnelNo">شماره پرسنلی:</asp:Label>
                                            </td>
                                            <td width="20%">
                                                <cc1:NumericTextBox ID="txtPersonnelNo" runat="server" Text=""></cc1:NumericTextBox>
                                            </td>
                                            <td width="10%">
                                                <asp:Label runat="server" ID="lblNationalNo">کد ملی:</asp:Label>
                                            </td>
                                            <td width="60%">
                                                <cc1:NumericTextBox ID="txtNationalNo" runat="server" Text=""></cc1:NumericTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label runat="server" ID="lblFirstName">نام:</asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblLastName">نام و نام خانوادگی:</asp:Label>
                                            </td>
                                            <td>
                                                
                                                <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <telerik:RadGrid ID="grdCommandProcessingSearch" runat="server" AllowCustomPaging="True"
                                                    AllowPaging="True" AllowSorting="True" CellSpacing="0" GridLines="None" HorizontalAlign="Center"
                                                    AutoGenerateColumns="False" AllowMultiRowSelection="True">
                                                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Office2007">
                                                    </HeaderContextMenu>
                                                    <MasterTableView DataKeyNames="PersonId" Dir="RTL" GroupsDefaultExpanded="False"
                                                        ClientDataKeyNames="PersonId" NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد">
                                                        <Columns>
                                                            <telerik:GridBoundColumn DataField="PersonnelNo" HeaderText="شماره پرسنلی" SortExpression="PersonnelNo">
                                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="NationalNo" HeaderText="کد ملی" SortExpression="NationalNo">
                                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="FirstName" HeaderText="نام" SortExpression="FirstName">
                                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="LastName" HeaderText="نام خانوادگی" SortExpression="LastName">
                                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="FatherName" HeaderText="نام پدر" SortExpression="FatherName">
                                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridClientSelectColumn UniqueName="Select" HeaderText="انتخاب">
                                                            </telerik:GridClientSelectColumn>
                                                        </Columns>
                                                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                                                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                                        </RowIndicatorColumn>
                                                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                                        </ExpandCollapseColumn>
                                                        <EditFormSettings>
                                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                                            </EditColumn>
                                                        </EditFormSettings>
                                                        <PagerStyle FirstPageToolTip="صفحه اول" HorizontalAlign="Center" LastPageToolTip="صفحه آخر"
                                                            NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; به &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                                                            PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                                                            VerticalAlign="Middle" />
                                                    </MasterTableView>
                                                    <ClientSettings ReorderColumnsOnClient="True" AllowColumnsReorder="True" EnableRowHoverStyle="true">
                                                        <ClientEvents OnRowSelected="RowSelected" OnRowDeselected="RowDeselected" OnMasterTableViewCreated="MasterTableViewCreated">
                                                        </ClientEvents>
                                                        <Selecting AllowRowSelect="True"></Selecting>
                                                    </ClientSettings>
                                                    <FilterMenu EnableImageSprites="False">
                                                    </FilterMenu>
                                                </telerik:RadGrid>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelItem>
                    <telerik:RadPanelItem Text="پردازش اطلاعات حکمی" Font-Bold="True" Font-Size="11"
                        Height="35" Font-Names="Arial" ForeColor="White" ImagePosition="Right" ImageUrl="../Images/Process-Info.png">
                        <Items>
                            <telerik:RadPanelItem Value="CommandmentProcessInformation">
                                <ItemTemplate>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <telerik:RadTabStrip ID="radtabCommandProccessing" runat="server" Align="Right" Skin="Office2007"
                                                    AutoPostBack="True" CausesValidation="False" MultiPageID="radmultipageCommandProccessing"
                                                    OnTabClick="radtabCommandProccessing_TabClick" Orientation="HorizontalBottom">
                                                    <Tabs>
                                                        <telerik:RadTab runat="server" ImageUrl="../Images/calculater.png" PageViewID="CommandProcessing/ProcEngineCommandCalculatePage"
                                                            Text="محاسبه" ToolTip="محاسبه" Value="1" Width="150">
                                                        </telerik:RadTab>
                                                        <telerik:RadTab runat="server" ImageUrl="../Images/TabPrintTemprory.png" PageViewID="PrsPersonnelEmploymentPage"
                                                            Text="حکم پیش نویس و کمیته" ToolTip="حکم پیش نویس و کمیته" Value="2">
                                                        </telerik:RadTab>
                                                    </Tabs>
                                                </telerik:RadTabStrip>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <telerik:RadMultiPage ID="radmultipageCommandProccessing" runat="server" OnPageViewCreated="radmultipageCommandProccessing_PageViewCreated" Width="100%"
                                                    RenderSelectedPageOnly="True">
                                                </telerik:RadMultiPage>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelItem>
                </Items>
                <ExpandAnimation Type="None"></ExpandAnimation>
            </telerik:RadPanelBar>
        </td>
    </tr>
</table>
<asp:HiddenField ID="hiddenfieldKeyId" runat="server" />
<telerik:RadScriptBlock ID="RadScriptBlock" runat="server">
    <script type="text/javascript">
        var myArrayKey = new Array();
        var hiddenfieldvalue = '';
        var str = '';
        function RowSelected(sender, eventArgs) {

            KeyNamevalue = eventArgs._tableView.get_clientDataKeyNames()[0];

            hiddenfieldvalue = document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value;
            var textKeyvalue = eventArgs.getDataKeyValue(KeyNamevalue);
            if (include(myArrayKey, textKeyvalue) != true) {
                if (myArrayKey.length > 0)
                    str = ',';
                hiddenfieldvalue = hiddenfieldvalue + str + textKeyvalue;
                myArrayKey.push(textKeyvalue);
            }
            document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value = hiddenfieldvalue;
        }

        function RowDeselected(sender, eventArgs) {

            str = '';
            hiddenfieldvalue = '';
            KeyName = eventArgs._tableView.get_clientDataKeyNames()[0];
            for (var i = 0; i < myArrayKey.length; i++) {
                if (myArrayKey[i] == eventArgs.getDataKeyValue(KeyName))
                    myArrayKey.splice(i, 1);
            }
            for (var i = 0; i < myArrayKey.length; i++) {
                if (i != 0)
                    str = ',';
                hiddenfieldvalue = hiddenfieldvalue + str + myArrayKey[i];
            }
            document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value = hiddenfieldvalue;
        }

        function MasterTableViewCreated(sender, eventArgs) {
            if (document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value != '')
                myArrayKey = document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value.split(',');
            KeyName = sender.MasterTableView.get_clientDataKeyNames()[0];
            for (var j = 0; j < sender.get_masterTableView().get_dataItems().length; j++)
                for (var i = 0; i < myArrayKey.length; i++) {
                    if (myArrayKey[i] == sender.get_masterTableView().get_dataItems()[j].getDataKeyValue(KeyName))
                        sender.get_masterTableView().get_dataItems()[j].set_selected(true);
                }
        }


        function include(arr, obj) {
            if (arr.length == 0)
                return false;
            for (var i = 0; i < arr.length; i++) {
                if (arr[i] == obj)
                    return true;
            }
        }

    </script>
</telerik:RadScriptBlock>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="CustomMessageErrorControl" />
                <telerik:AjaxUpdatedControl ControlID="radpanelbarSearch" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnShowAll">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="CustomMessageErrorControl" />
                <telerik:AjaxUpdatedControl ControlID="radpanelbarSearch" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="radtabCommandProccessing">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="CustomMessageErrorControl" />
                <telerik:AjaxUpdatedControl ControlID="radtabCommandProccessing" />
                <telerik:AjaxUpdatedControl ControlID="radmultipageCommandProccessing" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="hiddenfieldKeyId" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default">
</telerik:RadAjaxLoadingPanel>
