﻿using System.Collections.Generic;
using System.Data;
using Intranet.Configuration;
using Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.DAL;
using PersonnelExtended.Entity;

namespace PersonnelExtended.BL
{
//-------------------------------------------------------


    /// <summary>
    /// 
    /// </summary>
    public class ProcItemBL : DeskTopObj
    {
        private readonly ProcItemDB _ProcItemDB;

        public ProcItemBL()
        {
            _ProcItemDB = new ProcItemDB();
        }

        public void Add(ProcItemEntity ProcItemEntityParam, out int ItemId)
        {
            _ProcItemDB.AddProcItemDB(ProcItemEntityParam, out ItemId);
        }

        public void Update(ProcItemEntity ProcItemEntityParam)
        {
            _ProcItemDB.UpdateProcItemDB(ProcItemEntityParam);
        }

        public void Delete(ProcItemEntity ProcItemEntityParam)
        {
            _ProcItemDB.DeleteProcItemDB(ProcItemEntityParam);
        }

        public ProcItemEntity GetSingleById(ProcItemEntity ProcItemEntityParam)
        {
            ProcItemEntity o = GetProcItemFromProcItemDB(
                _ProcItemDB.GetSingleProcItemDB(ProcItemEntityParam));

            return o;
        }

        public List<ProcItemEntity> GetAll()
        {
            List<ProcItemEntity> lst = new List<ProcItemEntity>();
            //string key = "ProcItem_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ProcItemEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetProcItemCollectionFromProcItemDBList(
                _ProcItemDB.GetAllProcItemDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<ProcItemEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "ProcItem_List_Page_" + currentPage ;
            //string countKey = "ProcItem_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<ProcItemEntity> lst = new List<ProcItemEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ProcItemEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetProcItemCollectionFromProcItemDBList(
                _ProcItemDB.GetPageProcItemDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private ProcItemEntity GetProcItemFromProcItemDB(ProcItemEntity o)
        {
            if (o == null)
                return null;
            ProcItemEntity ret = new ProcItemEntity(o.ItemId, o.ItemTitle, o.ItemValueTypeId, o.ShowOrder, o.ObjectId, o.SourceTableId, o.SourceFieldName, o.Icon, o.CreationDate, o.ModificationDate, o.HasTitle);
            return ret;
        }

        private List<ProcItemEntity> GetProcItemCollectionFromProcItemDBList(List<ProcItemEntity> lst)
        {
            List<ProcItemEntity> RetLst = new List<ProcItemEntity>();
            foreach (ProcItemEntity o in lst)
            {
                RetLst.Add(GetProcItemFromProcItemDB(o));
            }
            return RetLst;

        }        

        // own Function
        public bool CheckItemObjectIsTree(ProcItemEntity procItemEntity)
        {            
            return   _ProcItemDB.CheckItemObjectIsTree(procItemEntity);            
        }

        // own Function
        public DataTable ProcItemGetAllData(int itemId)
        {
            return _ProcItemDB.ProcItemGetAllData(itemId);
        }

        // own Function
        public DataTable ProcItemGroupGetAll()
        {
            return _ProcItemDB.ProcItemGroupGetAll();
        }

        public DataTable GetAllByItemGroup(int itemGroupId)
        {
            return _ProcItemDB.GetAllByItemGroup(itemGroupId);
        }
    }
}