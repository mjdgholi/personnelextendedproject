﻿using System.Collections.Generic;
using Intranet.Configuration;
using Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.DAL;
using PersonnelExtended.Entity;

namespace PersonnelExtended.BL
{

//-------------------------------------------------------


    /// <summary>
    /// 
    /// </summary>
    public class ProcTextBoxTypeBL : DeskTopObj
    {
        private readonly ProcTextBoxTypeDB _ProcTextBoxTypeDB;

        public ProcTextBoxTypeBL()
        {
            _ProcTextBoxTypeDB = new ProcTextBoxTypeDB();
        }

        public void Add(ProcTextBoxTypeEntity ProcTextBoxTypeEntityParam, out int TextBoxTypeId)
        {
            _ProcTextBoxTypeDB.AddProcTextBoxTypeDB(ProcTextBoxTypeEntityParam, out TextBoxTypeId);
        }

        public void Update(ProcTextBoxTypeEntity ProcTextBoxTypeEntityParam)
        {
            _ProcTextBoxTypeDB.UpdateProcTextBoxTypeDB(ProcTextBoxTypeEntityParam);
        }

        public void Delete(ProcTextBoxTypeEntity ProcTextBoxTypeEntityParam)
        {
            _ProcTextBoxTypeDB.DeleteProcTextBoxTypeDB(ProcTextBoxTypeEntityParam);
        }

        public ProcTextBoxTypeEntity GetSingleById(ProcTextBoxTypeEntity ProcTextBoxTypeEntityParam)
        {
            ProcTextBoxTypeEntity o = GetProcTextBoxTypeFromProcTextBoxTypeDB(
                _ProcTextBoxTypeDB.GetSingleProcTextBoxTypeDB(ProcTextBoxTypeEntityParam));

            return o;
        }

        public List<ProcTextBoxTypeEntity> GetAll()
        {
            List<ProcTextBoxTypeEntity> lst = new List<ProcTextBoxTypeEntity>();
            //string key = "ProcTextBoxType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ProcTextBoxTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetProcTextBoxTypeCollectionFromProcTextBoxTypeDBList(
                _ProcTextBoxTypeDB.GetAllProcTextBoxTypeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<ProcTextBoxTypeEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "ProcTextBoxType_List_Page_" + currentPage ;
            //string countKey = "ProcTextBoxType_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<ProcTextBoxTypeEntity> lst = new List<ProcTextBoxTypeEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ProcTextBoxTypeEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetProcTextBoxTypeCollectionFromProcTextBoxTypeDBList(
                _ProcTextBoxTypeDB.GetPageProcTextBoxTypeDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private ProcTextBoxTypeEntity GetProcTextBoxTypeFromProcTextBoxTypeDB(ProcTextBoxTypeEntity o)
        {
            if (o == null)
                return null;
            ProcTextBoxTypeEntity ret = new ProcTextBoxTypeEntity(o.TextBoxTypeId, o.TextBoxTypeTitle, o.CSSName, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private List<ProcTextBoxTypeEntity> GetProcTextBoxTypeCollectionFromProcTextBoxTypeDBList(List<ProcTextBoxTypeEntity> lst)
        {
            List<ProcTextBoxTypeEntity> RetLst = new List<ProcTextBoxTypeEntity>();
            foreach (ProcTextBoxTypeEntity o in lst)
            {
                RetLst.Add(GetProcTextBoxTypeFromProcTextBoxTypeDB(o));
            }
            return RetLst;

        }


    }

}