﻿using System.Collections.Generic;
using Intranet.Configuration;
using Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.DAL;
using PersonnelExtended.Entity;

namespace PersonnelExtended.BL
{
    public class ProcFormulaAndConditionBL : DeskTopObj
    {
        private readonly ProcFormulaAndConditionDB _ProcFormulaAndConditionDB;

        public ProcFormulaAndConditionBL()
        {
            _ProcFormulaAndConditionDB = new ProcFormulaAndConditionDB();
        }

        public void Add(ProcFormulaAndConditionEntity ProcFormulaAndConditionEntityParam, out int FormulaAndConditionId)
        {
            _ProcFormulaAndConditionDB.AddProcFormulaAndConditionDB(ProcFormulaAndConditionEntityParam, out FormulaAndConditionId);
        }

        public void Update(ProcFormulaAndConditionEntity ProcFormulaAndConditionEntityParam)
        {
            _ProcFormulaAndConditionDB.UpdateProcFormulaAndConditionDB(ProcFormulaAndConditionEntityParam);
        }

        public void Delete(ProcFormulaAndConditionEntity ProcFormulaAndConditionEntityParam)
        {
            _ProcFormulaAndConditionDB.DeleteProcFormulaAndConditionDB(ProcFormulaAndConditionEntityParam);
        }

        public ProcFormulaAndConditionEntity GetSingleById(ProcFormulaAndConditionEntity ProcFormulaAndConditionEntityParam)
        {
            ProcFormulaAndConditionEntity o = GetProcFormulaAndConditionFromProcFormulaAndConditionDB(
                _ProcFormulaAndConditionDB.GetSingleProcFormulaAndConditionDB(ProcFormulaAndConditionEntityParam));

            return o;
        }

        public List<ProcFormulaAndConditionEntity> GetAll()
        {
            List<ProcFormulaAndConditionEntity> lst = new List<ProcFormulaAndConditionEntity>();
            //string key = "ProcFormulaAndCondition_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ProcFormulaAndConditionEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetProcFormulaAndConditionCollectionFromProcFormulaAndConditionDBList(
                _ProcFormulaAndConditionDB.GetAllProcFormulaAndConditionDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<ProcFormulaAndConditionEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "ProcFormulaAndCondition_List_Page_" + currentPage ;
            //string countKey = "ProcFormulaAndCondition_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<ProcFormulaAndConditionEntity> lst = new List<ProcFormulaAndConditionEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ProcFormulaAndConditionEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetProcFormulaAndConditionCollectionFromProcFormulaAndConditionDBList(
                _ProcFormulaAndConditionDB.GetPageProcFormulaAndConditionDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private ProcFormulaAndConditionEntity GetProcFormulaAndConditionFromProcFormulaAndConditionDB(ProcFormulaAndConditionEntity o)
        {
            if (o == null)
                return null;
            ProcFormulaAndConditionEntity ret = new ProcFormulaAndConditionEntity(o.FormulaAndConditionId, o.ItemDetailProcessId, o.TypeId, o.ConditionalExpressionForUser, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private List<ProcFormulaAndConditionEntity> GetProcFormulaAndConditionCollectionFromProcFormulaAndConditionDBList(List<ProcFormulaAndConditionEntity> lst)
        {
            List<ProcFormulaAndConditionEntity> RetLst = new List<ProcFormulaAndConditionEntity>();
            foreach (ProcFormulaAndConditionEntity o in lst)
            {
                RetLst.Add(GetProcFormulaAndConditionFromProcFormulaAndConditionDB(o));
            }
            return RetLst;

        }


        public List<ProcFormulaAndConditionEntity> GetProcFormulaAndConditionCollectionByProcItemDetailProcess(int ItemDetailProcessId)
        {
            return GetProcFormulaAndConditionCollectionFromProcFormulaAndConditionDBList(_ProcFormulaAndConditionDB.GetProcFormulaAndConditionDBCollectionByProcItemDetailProcessDB(ItemDetailProcessId));
        }

        public ProcFormulaAndConditionEntity GetSingleByDetailProcessByType(int itemDetailProcessId, int type)
        {
          return  GetProcFormulaAndConditionFromProcFormulaAndConditionDB(_ProcFormulaAndConditionDB.GetSingleByDetailProcessByType(itemDetailProcessId, type));
        }
    }
}