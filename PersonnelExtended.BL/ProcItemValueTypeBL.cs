﻿using System.Collections.Generic;
using Intranet.Configuration;
using Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.DAL;
using PersonnelExtended.Entity;

namespace PersonnelExtended.BL
{

//-------------------------------------------------------


    /// <summary>
    /// 
    /// </summary>
    public class ProcItemValueTypeBL : DeskTopObj
    {
        private readonly ProcItemValueTypeDB _ProcItemValueTypeDB;

        public ProcItemValueTypeBL()
        {
            _ProcItemValueTypeDB = new ProcItemValueTypeDB();
        }

        public void Add(ProcItemValueTypeEntity ProcItemValueTypeEntityParam, out int ItemValueTypeId)
        {
            _ProcItemValueTypeDB.AddProcItemValueTypeDB(ProcItemValueTypeEntityParam, out ItemValueTypeId);
        }

        public void Update(ProcItemValueTypeEntity ProcItemValueTypeEntityParam)
        {
            _ProcItemValueTypeDB.UpdateProcItemValueTypeDB(ProcItemValueTypeEntityParam);
        }

        public void Delete(ProcItemValueTypeEntity ProcItemValueTypeEntityParam)
        {
            _ProcItemValueTypeDB.DeleteProcItemValueTypeDB(ProcItemValueTypeEntityParam);
        }

        public ProcItemValueTypeEntity GetSingleById(ProcItemValueTypeEntity ProcItemValueTypeEntityParam)
        {
            ProcItemValueTypeEntity o = GetProcItemValueTypeFromProcItemValueTypeDB(
                _ProcItemValueTypeDB.GetSingleProcItemValueTypeDB(ProcItemValueTypeEntityParam));

            return o;
        }

        public List<ProcItemValueTypeEntity> GetAll()
        {
            List<ProcItemValueTypeEntity> lst = new List<ProcItemValueTypeEntity>();
            //string key = "ProcItemValueType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ProcItemValueTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetProcItemValueTypeCollectionFromProcItemValueTypeDBList(
                _ProcItemValueTypeDB.GetAllProcItemValueTypeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<ProcItemValueTypeEntity> GetAllIsActive()
        {
            List<ProcItemValueTypeEntity> lst = new List<ProcItemValueTypeEntity>();
            //string key = "ProcItemValueType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ProcItemValueTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetProcItemValueTypeCollectionFromProcItemValueTypeDBList(
                _ProcItemValueTypeDB.GetAllIsActiveProcItemValueTypeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<ProcItemValueTypeEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "ProcItemValueType_List_Page_" + currentPage ;
            //string countKey = "ProcItemValueType_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<ProcItemValueTypeEntity> lst = new List<ProcItemValueTypeEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ProcItemValueTypeEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetProcItemValueTypeCollectionFromProcItemValueTypeDBList(
                _ProcItemValueTypeDB.GetPageProcItemValueTypeDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private ProcItemValueTypeEntity GetProcItemValueTypeFromProcItemValueTypeDB(ProcItemValueTypeEntity o)
        {
            if (o == null)
                return null;
            ProcItemValueTypeEntity ret = new ProcItemValueTypeEntity(o.ItemValueTypeId, o.ItemValueTypeTitle, o.CreationDate, o.ModificationDate, o.IsViewed);
            return ret;
        }

        private List<ProcItemValueTypeEntity> GetProcItemValueTypeCollectionFromProcItemValueTypeDBList(List<ProcItemValueTypeEntity> lst)
        {
            List<ProcItemValueTypeEntity> RetLst = new List<ProcItemValueTypeEntity>();
            foreach (ProcItemValueTypeEntity o in lst)
            {
                RetLst.Add(GetProcItemValueTypeFromProcItemValueTypeDB(o));
            }
            return RetLst;

        }
    }
}