﻿using System.Collections.Generic;
using Intranet.Configuration;
using Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.DAL;
using PersonnelExtended.Entity;

namespace PersonnelExtended.BL
{
    public class ProcItemDetailProcessBL : DeskTopObj
    {
        private readonly ProcItemDetailProcessDB _ProcItemDetailProcessDB;

        public ProcItemDetailProcessBL()
        {
            _ProcItemDetailProcessDB = new ProcItemDetailProcessDB();
        }

        public void Add(ProcItemDetailProcessEntity ProcItemDetailProcessEntityParam, out int ItemDetailProcessId)
        {
            _ProcItemDetailProcessDB.AddProcItemDetailProcessDB(ProcItemDetailProcessEntityParam, out ItemDetailProcessId);
        }

        public void Update(ProcItemDetailProcessEntity ProcItemDetailProcessEntityParam)
        {
            _ProcItemDetailProcessDB.UpdateProcItemDetailProcessDB(ProcItemDetailProcessEntityParam);
        }

        public void Delete(ProcItemDetailProcessEntity ProcItemDetailProcessEntityParam)
        {
            _ProcItemDetailProcessDB.DeleteProcItemDetailProcessDB(ProcItemDetailProcessEntityParam);
        }

        public ProcItemDetailProcessEntity GetSingleById(ProcItemDetailProcessEntity ProcItemDetailProcessEntityParam)
        {
            ProcItemDetailProcessEntity o = GetProcItemDetailProcessFromProcItemDetailProcessDB(
                _ProcItemDetailProcessDB.GetSingleProcItemDetailProcessDB(ProcItemDetailProcessEntityParam));

            return o;
        }

        public List<ProcItemDetailProcessEntity> GetAll()
        {
            List<ProcItemDetailProcessEntity> lst = new List<ProcItemDetailProcessEntity>();
            //string key = "ProcItemDetailProcess_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ProcItemDetailProcessEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetProcItemDetailProcessCollectionFromProcItemDetailProcessDBList(
                _ProcItemDetailProcessDB.GetAllProcItemDetailProcessDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<ProcItemDetailProcessEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "ProcItemDetailProcess_List_Page_" + currentPage ;
            //string countKey = "ProcItemDetailProcess_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<ProcItemDetailProcessEntity> lst = new List<ProcItemDetailProcessEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ProcItemDetailProcessEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetProcItemDetailProcessCollectionFromProcItemDetailProcessDBList(
                _ProcItemDetailProcessDB.GetPageProcItemDetailProcessDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private ProcItemDetailProcessEntity GetProcItemDetailProcessFromProcItemDetailProcessDB(ProcItemDetailProcessEntity o)
        {
            if (o == null)
                return null;
            ProcItemDetailProcessEntity ret = new ProcItemDetailProcessEntity(o.ItemDetailProcessId, o.ItemId, o.Condition, o.IsBuiltInFunction, o.Formula, o.FunctionId, o.Priority, o.FromDate, o.ToDate, o.IsValid, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private List<ProcItemDetailProcessEntity> GetProcItemDetailProcessCollectionFromProcItemDetailProcessDBList(List<ProcItemDetailProcessEntity> lst)
        {
            List<ProcItemDetailProcessEntity> RetLst = new List<ProcItemDetailProcessEntity>();
            foreach (ProcItemDetailProcessEntity o in lst)
            {
                RetLst.Add(GetProcItemDetailProcessFromProcItemDetailProcessDB(o));
            }
            return RetLst;

        }

        public List<ProcItemDetailProcessEntity> GetProcItemDetailProcessCollectionByProcItem(int ItemId)
        {
            return GetProcItemDetailProcessCollectionFromProcItemDetailProcessDBList(_ProcItemDetailProcessDB.GetProcItemDetailProcessDBCollectionByProcItemDB(ItemId));
        }

        public void CloseDateProcItemDetailProcessDB(ProcItemDetailProcessEntity procItemDetailProcessEntity)
        {
            _ProcItemDetailProcessDB.CloseDateProcItemDetailProcessDB(procItemDetailProcessEntity);
        }
    }
}