﻿using System.Collections.Generic;
using System.Data;
using Intranet.Configuration;
using Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.DAL;
using PersonnelExtended.Entity;

namespace PersonnelExtended.BL
{
    public class ProcObjectBL : DeskTopObj
    {
        private readonly ProcObjectDB _ProcObjectDB;

        public ProcObjectBL()
        {
            _ProcObjectDB = new ProcObjectDB();
        }

        public void Add(ProcObjectEntity ProcObjectEntityParam, out int ObjectId)
        {
            _ProcObjectDB.AddProcObjectDB(ProcObjectEntityParam, out ObjectId);
        }

        public void Update(ProcObjectEntity ProcObjectEntityParam)
        {
            _ProcObjectDB.UpdateProcObjectDB(ProcObjectEntityParam);
        }

        public void Delete(ProcObjectEntity ProcObjectEntityParam)
        {
            _ProcObjectDB.DeleteProcObjectDB(ProcObjectEntityParam);
        }

        public ProcObjectEntity GetSingleById(ProcObjectEntity ProcObjectEntityParam)
        {
            ProcObjectEntity o = GetProcObjectFromProcObjectDB(
                _ProcObjectDB.GetSingleProcObjectDB(ProcObjectEntityParam));

            return o;
        }

        public List<ProcObjectEntity> GetAll()
        {
            List<ProcObjectEntity> lst = new List<ProcObjectEntity>();
            //string key = "ProcObject_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ProcObjectEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetProcObjectCollectionFromProcObjectDBList(
                _ProcObjectDB.GetAllProcObjectDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<ProcObjectEntity> GetAllIsActive()
        {
            List<ProcObjectEntity> lst = new List<ProcObjectEntity>();
            //string key = "ProcObject_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ProcObjectEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetProcObjectCollectionFromProcObjectDBList(
                _ProcObjectDB.GetAllIsActiveProcObjectDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<ProcObjectEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "ProcObject_List_Page_" + currentPage ;
            //string countKey = "ProcObject_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<ProcObjectEntity> lst = new List<ProcObjectEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ProcObjectEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetProcObjectCollectionFromProcObjectDBList(
                _ProcObjectDB.GetPageProcObjectDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private ProcObjectEntity GetProcObjectFromProcObjectDB(ProcObjectEntity o)
        {
            if (o == null)
                return null;
            ProcObjectEntity ret = new ProcObjectEntity(o.ObjectId, o.ObjectTitle, o.ObjectTitlePersian, o.CreationDate, o.ModificationDate, o.ObjectTypeId, o.SystemId, o.IsHierarchy, o.IsActive);
            return ret;
        }

        private List<ProcObjectEntity> GetProcObjectCollectionFromProcObjectDBList(List<ProcObjectEntity> lst)
        {
            List<ProcObjectEntity> RetLst = new List<ProcObjectEntity>();
            foreach (ProcObjectEntity o in lst)
            {
                RetLst.Add(GetProcObjectFromProcObjectDB(o));
            }
            return RetLst;

        }

        public List<ProcObjectEntity> GetProcObjectCollectionByProcObjectType(int ObjectTypeId)
        {
            return GetProcObjectCollectionFromProcObjectDBList(_ProcObjectDB.GetProcObjectDBCollectionByProcObjectTypeDB(ObjectTypeId));
        }

        public DataSet GetAllSqlObject(string objectTitle, string schemaName)
        {
            return _ProcObjectDB.GetAllSqlObject(objectTitle, schemaName);
        }
    }
}