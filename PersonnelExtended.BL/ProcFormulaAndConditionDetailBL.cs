﻿using System.Collections.Generic;
using System.Data;
using Intranet.Configuration;
using Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.DAL;
using PersonnelExtended.Entity;

namespace PersonnelExtended.BL
{
    /// <summary>
    /// 
    /// </summary>
    public class ProcFormulaAndConditionDetailBL : DeskTopObj
    {
        private readonly ProcFormulaAndConditionDetailDB _ProcFormulaAndConditionDetailDB;

        public ProcFormulaAndConditionDetailBL()
        {
            _ProcFormulaAndConditionDetailDB = new ProcFormulaAndConditionDetailDB();
        }

        public void Add(ProcFormulaAndConditionDetailEntity ProcFormulaAndConditionDetailEntityParam, out int ProcFormulaAndConditionDetailId)
        {
            _ProcFormulaAndConditionDetailDB.AddProcFormulaAndConditionDetailDB(ProcFormulaAndConditionDetailEntityParam, out ProcFormulaAndConditionDetailId);
        }

        public void Update(ProcFormulaAndConditionDetailEntity ProcFormulaAndConditionDetailEntityParam)
        {
            _ProcFormulaAndConditionDetailDB.UpdateProcFormulaAndConditionDetailDB(ProcFormulaAndConditionDetailEntityParam);
        }

        public void Delete(ProcFormulaAndConditionDetailEntity ProcFormulaAndConditionDetailEntityParam)
        {
            _ProcFormulaAndConditionDetailDB.DeleteProcFormulaAndConditionDetailDB(ProcFormulaAndConditionDetailEntityParam);
        }

        public ProcFormulaAndConditionDetailEntity GetSingleById(ProcFormulaAndConditionDetailEntity ProcFormulaAndConditionDetailEntityParam)
        {
            ProcFormulaAndConditionDetailEntity o = GetProcFormulaAndConditionDetailFromProcFormulaAndConditionDetailDB(
                _ProcFormulaAndConditionDetailDB.GetSingleProcFormulaAndConditionDetailDB(ProcFormulaAndConditionDetailEntityParam));

            return o;
        }

        public List<ProcFormulaAndConditionDetailEntity> GetAll()
        {
            List<ProcFormulaAndConditionDetailEntity> lst = new List<ProcFormulaAndConditionDetailEntity>();
            //string key = "ProcFormulaAndConditionDetail_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ProcFormulaAndConditionDetailEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetProcFormulaAndConditionDetailCollectionFromProcFormulaAndConditionDetailDBList(
                _ProcFormulaAndConditionDetailDB.GetAllProcFormulaAndConditionDetailDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<ProcFormulaAndConditionDetailEntity> GetAllPaging(int currentPage, int pageSize, string sortExpression, out int count, string whereClause)
        {
            //string key = "ProcFormulaAndConditionDetail_List_Page_" + currentPage ;
            //string countKey = "ProcFormulaAndConditionDetail_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<ProcFormulaAndConditionDetailEntity> lst = new List<ProcFormulaAndConditionDetailEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ProcFormulaAndConditionDetailEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetProcFormulaAndConditionDetailCollectionFromProcFormulaAndConditionDetailDBList(
                _ProcFormulaAndConditionDetailDB.GetPageProcFormulaAndConditionDetailDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private ProcFormulaAndConditionDetailEntity GetProcFormulaAndConditionDetailFromProcFormulaAndConditionDetailDB(ProcFormulaAndConditionDetailEntity o)
        {
            if (o == null)
                return null;
            ProcFormulaAndConditionDetailEntity ret = new ProcFormulaAndConditionDetailEntity(o.ProcFormulaAndConditionDetailId, o.FormulaAndConditionId, o.PreLink, o.PostLink, o.UserAlterLink, o.TextBoxTypeId, o.OperatorId, o.ItemId, o.Value, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private List<ProcFormulaAndConditionDetailEntity> GetProcFormulaAndConditionDetailCollectionFromProcFormulaAndConditionDetailDBList(List<ProcFormulaAndConditionDetailEntity> lst)
        {
            List<ProcFormulaAndConditionDetailEntity> RetLst = new List<ProcFormulaAndConditionDetailEntity>();
            foreach (ProcFormulaAndConditionDetailEntity o in lst)
            {
                RetLst.Add(GetProcFormulaAndConditionDetailFromProcFormulaAndConditionDetailDB(o));
            }
            return RetLst;

        }

        public List<ProcFormulaAndConditionDetailEntity> GetProcFormulaAndConditionDetailCollectionByProcFormulaAndCondition(int FormulaAndConditionId)
        {
            return GetProcFormulaAndConditionDetailCollectionFromProcFormulaAndConditionDetailDBList(_ProcFormulaAndConditionDetailDB.GetProcFormulaAndConditionDetailDBCollectionByProcFormulaAndConditionDB(FormulaAndConditionId));
        }

        public List<ProcFormulaAndConditionDetailEntity> GetProcFormulaAndConditionDetailCollectionByProcItem(int ItemId)
        {
            return GetProcFormulaAndConditionDetailCollectionFromProcFormulaAndConditionDetailDBList(_ProcFormulaAndConditionDetailDB.GetProcFormulaAndConditionDetailDBCollectionByProcItemDB(ItemId));
        }

        public List<ProcFormulaAndConditionDetailEntity> GetProcFormulaAndConditionDetailCollectionByProcOperator(int OperatorId)
        {
            return GetProcFormulaAndConditionDetailCollectionFromProcFormulaAndConditionDetailDBList(_ProcFormulaAndConditionDetailDB.GetProcFormulaAndConditionDetailDBCollectionByProcOperatorDB(OperatorId));
        }

        public List<ProcFormulaAndConditionDetailEntity> GetProcFormulaAndConditionDetailCollectionByProcTextBoxType(int TextBoxTypeId)
        {
            return GetProcFormulaAndConditionDetailCollectionFromProcFormulaAndConditionDetailDBList(_ProcFormulaAndConditionDetailDB.GetProcFormulaAndConditionDetailDBCollectionByProcTextBoxTypeDB(TextBoxTypeId));
        }

        public DataTable GetProcFormulaAndConditionDetailCollectionByProcFormulaAndConditionAndType(int itemDetailProcessId, int typeId)
        {
            return _ProcFormulaAndConditionDetailDB.GetProcFormulaAndConditionDetailCollectionByProcFormulaAndConditionAndType(itemDetailProcessId, typeId);
        }

        // own Function
        public void AddAndUpdateLinkList(int? currentBoxId, ProcFormulaAndConditionDetailEntity newBoxEntity, out int procFormulaAndConditionDetailId)
        {
            _ProcFormulaAndConditionDetailDB.AddAndUpdateLinkListDB(currentBoxId, newBoxEntity, out procFormulaAndConditionDetailId);
        }

        // own Function
        public void DeleteAndUpdateLinkList(int procFormulaAndConditionDetailId)
        {
            _ProcFormulaAndConditionDetailDB.DeleteAndUpdateLinkList(procFormulaAndConditionDetailId);
        }

        public void UpdateFinalExpression(int formulaAndConditionId, string finalFormula,string finalFormulaForUser)
        {
            _ProcFormulaAndConditionDetailDB.UpdateFinalExpression(formulaAndConditionId, finalFormula, finalFormulaForUser);
        }
    }
}