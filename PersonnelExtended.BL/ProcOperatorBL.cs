﻿using System.Collections.Generic;
using Intranet.Configuration;
using Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.DAL;
using PersonnelExtended.Entity;

namespace PersonnelExtended.BL
{
    public class ProcOperatorBL : DeskTopObj
    {
        private readonly ProcOperatorDB _ProcOperatorDB;

        public ProcOperatorBL()
        {
            _ProcOperatorDB = new ProcOperatorDB();
        }

        public void Add(ProcOperatorEntity ProcOperatorEntityParam, out int OperatorId)
        {
            _ProcOperatorDB.AddProcOperatorDB(ProcOperatorEntityParam, out OperatorId);
        }

        public void Update(ProcOperatorEntity ProcOperatorEntityParam)
        {
            _ProcOperatorDB.UpdateProcOperatorDB(ProcOperatorEntityParam);
        }

        public void Delete(ProcOperatorEntity ProcOperatorEntityParam)
        {
            _ProcOperatorDB.DeleteProcOperatorDB(ProcOperatorEntityParam);
        }

        public ProcOperatorEntity GetSingleById(ProcOperatorEntity ProcOperatorEntityParam)
        {
            ProcOperatorEntity o = GetProcOperatorFromProcOperatorDB(
                _ProcOperatorDB.GetSingleProcOperatorDB(ProcOperatorEntityParam));

            return o;
        }

        public List<ProcOperatorEntity> GetAll()
        {
            List<ProcOperatorEntity> lst = new List<ProcOperatorEntity>();
            //string key = "ProcOperator_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ProcOperatorEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetProcOperatorCollectionFromProcOperatorDBList(
                _ProcOperatorDB.GetAllProcOperatorDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<ProcOperatorEntity> GetAllIsActive()
        {
            List<ProcOperatorEntity> lst = new List<ProcOperatorEntity>();
            //string key = "ProcOperator_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ProcOperatorEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetProcOperatorCollectionFromProcOperatorDBList(
                _ProcOperatorDB.GetAllIsActiveProcOperatorDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<ProcOperatorEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "ProcOperator_List_Page_" + currentPage ;
            //string countKey = "ProcOperator_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<ProcOperatorEntity> lst = new List<ProcOperatorEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ProcOperatorEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetProcOperatorCollectionFromProcOperatorDBList(
                _ProcOperatorDB.GetPageProcOperatorDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private ProcOperatorEntity GetProcOperatorFromProcOperatorDB(ProcOperatorEntity o)
        {
            if (o == null)
                return null;
            ProcOperatorEntity ret = new ProcOperatorEntity(o.OperatorId, o.OperatorTitle, o.SqlOperator, o.SampleTextForToolTip, o.IsActive, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private List<ProcOperatorEntity> GetProcOperatorCollectionFromProcOperatorDBList(List<ProcOperatorEntity> lst)
        {
            List<ProcOperatorEntity> RetLst = new List<ProcOperatorEntity>();
            foreach (ProcOperatorEntity o in lst)
            {
                RetLst.Add(GetProcOperatorFromProcOperatorDB(o));
            }
            return RetLst;

        }


    }
}