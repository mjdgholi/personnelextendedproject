﻿using System.Collections.Generic;
using Intranet.Configuration;
using Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.DAL;
using PersonnelExtended.Entity;

namespace PersonnelExtended.BL
{
    
//-------------------------------------------------------


    /// <summary>
    /// 
    /// </summary>
    public class ProcOperatorTemplateBL : DeskTopObj
    {
        private readonly ProcOperatorTemplateDB _ProcOperatorTemplateDB;

        public ProcOperatorTemplateBL()
        {
            _ProcOperatorTemplateDB = new ProcOperatorTemplateDB();
        }

        public void Add(ProcOperatorTemplateEntity ProcOperatorTemplateEntityParam, out int OperatorTemplateId)
        {
            _ProcOperatorTemplateDB.AddProcOperatorTemplateDB(ProcOperatorTemplateEntityParam, out OperatorTemplateId);
        }

        public void Update(ProcOperatorTemplateEntity ProcOperatorTemplateEntityParam)
        {
            _ProcOperatorTemplateDB.UpdateProcOperatorTemplateDB(ProcOperatorTemplateEntityParam);
        }

        public void Delete(ProcOperatorTemplateEntity ProcOperatorTemplateEntityParam)
        {
            _ProcOperatorTemplateDB.DeleteProcOperatorTemplateDB(ProcOperatorTemplateEntityParam);
        }

        public ProcOperatorTemplateEntity GetSingleById(ProcOperatorTemplateEntity ProcOperatorTemplateEntityParam)
        {
            ProcOperatorTemplateEntity o = GetProcOperatorTemplateFromProcOperatorTemplateDB(
                _ProcOperatorTemplateDB.GetSingleProcOperatorTemplateDB(ProcOperatorTemplateEntityParam));

            return o;
        }

        public List<ProcOperatorTemplateEntity> GetAll()
        {
            List<ProcOperatorTemplateEntity> lst = new List<ProcOperatorTemplateEntity>();
            //string key = "ProcOperatorTemplate_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ProcOperatorTemplateEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetProcOperatorTemplateCollectionFromProcOperatorTemplateDBList(
                _ProcOperatorTemplateDB.GetAllProcOperatorTemplateDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<ProcOperatorTemplateEntity> GetAllIsActive()
        {
            List<ProcOperatorTemplateEntity> lst = new List<ProcOperatorTemplateEntity>();
            //string key = "ProcOperatorTemplate_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ProcOperatorTemplateEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetProcOperatorTemplateCollectionFromProcOperatorTemplateDBList(
                _ProcOperatorTemplateDB.GetAllIsActiveProcOperatorTemplateDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<ProcOperatorTemplateEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "ProcOperatorTemplate_List_Page_" + currentPage ;
            //string countKey = "ProcOperatorTemplate_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<ProcOperatorTemplateEntity> lst = new List<ProcOperatorTemplateEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ProcOperatorTemplateEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetProcOperatorTemplateCollectionFromProcOperatorTemplateDBList(
                _ProcOperatorTemplateDB.GetPageProcOperatorTemplateDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private ProcOperatorTemplateEntity GetProcOperatorTemplateFromProcOperatorTemplateDB(ProcOperatorTemplateEntity o)
        {
            if (o == null)
                return null;
            ProcOperatorTemplateEntity ret = new ProcOperatorTemplateEntity(o.OperatorTemplateId, o.OperatorId, o.TemplateOperatorId, o.Priority, o.IsActive, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private List<ProcOperatorTemplateEntity> GetProcOperatorTemplateCollectionFromProcOperatorTemplateDBList(List<ProcOperatorTemplateEntity> lst)
        {
            List<ProcOperatorTemplateEntity> RetLst = new List<ProcOperatorTemplateEntity>();
            foreach (ProcOperatorTemplateEntity o in lst)
            {
                RetLst.Add(GetProcOperatorTemplateFromProcOperatorTemplateDB(o));
            }
            return RetLst;

        }

        public List<ProcOperatorTemplateEntity> GetProcOperatorTemplateCollectionByProcOperator(int OperatorId)
        {
            return GetProcOperatorTemplateCollectionFromProcOperatorTemplateDBList(_ProcOperatorTemplateDB.GetProcOperatorTemplateDBCollectionByProcOperatorDB(OperatorId));
        }
    }
}