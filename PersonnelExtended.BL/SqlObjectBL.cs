﻿using System.Data;
using Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.DAL;

namespace PersonnelExtended.BL
{
    public class SqlObjectBL
    {
        private SqlObjectDB _sqlObjectDB = new SqlObjectDB();

        public DataTable GetAllFunction(string schemaName)
        {
            return _sqlObjectDB.GetAllFunction(schemaName);
        }

        public string SqlQueryValidation(string sqlQuery)
        {
            return _sqlObjectDB.SqlQueryValidation(sqlQuery);
        }
    }
}