﻿using System.Collections.Generic;
using Intranet.Configuration;
using Intranet.DesktopModules.PersonnelExtendedProject.PersonnelExtended.DAL;
using PersonnelExtended.Entity;

namespace PersonnelExtended.BL
{
    /// <summary>
    /// 
    /// </summary>
    public class ProcObjectTypeBL : DeskTopObj
    {
        private readonly ProcObjectTypeDB _ProcObjectTypeDB;

        public ProcObjectTypeBL()
        {
            _ProcObjectTypeDB = new ProcObjectTypeDB();
        }

        public void Add(ProcObjectTypeEntity ProcObjectTypeEntityParam, out int ObjectTypeId)
        {
            _ProcObjectTypeDB.AddProcObjectTypeDB(ProcObjectTypeEntityParam, out ObjectTypeId);
        }

        public void Update(ProcObjectTypeEntity ProcObjectTypeEntityParam)
        {
            _ProcObjectTypeDB.UpdateProcObjectTypeDB(ProcObjectTypeEntityParam);
        }

        public void Delete(ProcObjectTypeEntity ProcObjectTypeEntityParam)
        {
            _ProcObjectTypeDB.DeleteProcObjectTypeDB(ProcObjectTypeEntityParam);
        }

        public ProcObjectTypeEntity GetSingleById(ProcObjectTypeEntity ProcObjectTypeEntityParam)
        {
            ProcObjectTypeEntity o = GetProcObjectTypeFromProcObjectTypeDB(
                _ProcObjectTypeDB.GetSingleProcObjectTypeDB(ProcObjectTypeEntityParam));

            return o;
        }

        public List<ProcObjectTypeEntity> GetAll()
        {
            List<ProcObjectTypeEntity> lst = new List<ProcObjectTypeEntity>();
            //string key = "ProcObjectType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ProcObjectTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetProcObjectTypeCollectionFromProcObjectTypeDBList(
                _ProcObjectTypeDB.GetAllProcObjectTypeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<ProcObjectTypeEntity> GetAllIsActive()
        {
            List<ProcObjectTypeEntity> lst = new List<ProcObjectTypeEntity>();
            //string key = "ProcObjectType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ProcObjectTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetProcObjectTypeCollectionFromProcObjectTypeDBList(
                _ProcObjectTypeDB.GetAllIsActiveProcObjectTypeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<ProcObjectTypeEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "ProcObjectType_List_Page_" + currentPage ;
            //string countKey = "ProcObjectType_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<ProcObjectTypeEntity> lst = new List<ProcObjectTypeEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ProcObjectTypeEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetProcObjectTypeCollectionFromProcObjectTypeDBList(
                _ProcObjectTypeDB.GetPageProcObjectTypeDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private ProcObjectTypeEntity GetProcObjectTypeFromProcObjectTypeDB(ProcObjectTypeEntity o)
        {
            if (o == null)
                return null;
            ProcObjectTypeEntity ret = new ProcObjectTypeEntity(o.ObjectTypeId, o.ObjectTypeTitle, o.ObjectTypeEnglieshTitle, o.IsActive, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private List<ProcObjectTypeEntity> GetProcObjectTypeCollectionFromProcObjectTypeDBList(List<ProcObjectTypeEntity> lst)
        {
            List<ProcObjectTypeEntity> RetLst = new List<ProcObjectTypeEntity>();
            foreach (ProcObjectTypeEntity o in lst)
            {
                RetLst.Add(GetProcObjectTypeFromProcObjectTypeDB(o));
            }
            return RetLst;

        }


    }
}